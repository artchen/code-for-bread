public class SudokuSolver {
    public static boolean[][] rowh = new boolean[9][9];
    public static boolean[][] colh = new boolean[9][9];
    public static boolean[][] sqh = new boolean[9][9];

    public static void main(String[] args) {

    }

    public static void print(char[][] board) {
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                System.out.print(board[i][j]);
            }
            System.out.println();
        }
    }

    public static void solveSudoku(char[][] board) {
        buildHash(board);
        solve(board, 0, 0);
    }
    
    public static void buildHash(char[][] board) {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                if ( board[i][j] != '.') {
                    rowh[i][board[i][j]-48-1] = true;
                    colh[j][board[i][j]-48-1] = true;
                    sqh[countSq(i,j)][board[i][j]-48-1] = true;
                }
            }
        }
    }
    
    public static int countSq(int row, int col) {
        int r = row / 3;
        int c = col / 3;
        return r*3 + c;
    }

    public static void mark(char[][] board, int row, int col, boolean marked) {
        //System.out.println(board[row][col]);
        rowh[row][board[row][col]-48-1] = marked;
        colh[col][board[row][col]-48-1] = marked;
        sqh[countSq(row,col)][board[row][col]-48-1] = marked;
    }
    
    public static boolean solve(char[][] board, int row, int col) {

        int next_row = col == 8 ? row + 1 : row;
        int next_col = col == 8 ? 0 : col + 1;

        if (board[row][col] == '.') {
            for (int j = 1; j < 10; j ++) {
                board[row][col] = (char)(j + '0');
                //mark(board,row,col,true);
                if (valid(board, row, col)) {
                    mark(board,row,col,true);
                    if (row == 8 && col == 8) {
                        return true;
                    }
                    if (solve(board, next_row, next_col)) {
                        return true;
                    }
                    else {
                        mark(board,row,col,false);
                        board[row][col] = '.';
                    }
                }
                board[row][col] = '.';
            }
            return false;
        }
        else if (row == 8 && col == 8) {
            return true;
        }
        else if (solve(board, next_row, next_col)) {
            return true;
        }
        
        return false;
    }
    
    public static boolean valid(char[][] board, int row, int col) {
        char value = board[row][col];
        //System.out.println("value=" + value + ", value-48-1=" + (value-48-1));
        if (rowh[row][value-48-1] || colh[col][value-48-1] || sqh[countSq(row,col)][value-48-1])
            return false;
        return true;
    }
    

}