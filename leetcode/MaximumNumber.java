import java.util.*;
public class MaximumNumber {

	public static void main(String[] args) {
		int[] nums1 = {6,7};
		int[] nums2 = {6,0,4};
		int k = 9;

		int[] nums3 = {7,6,1,9,3,2,3,1,1};
		int[] nums4 = {4,0,9,9,0,5,5,4,7};

		int[] nums5 = {3,4,6,5};
		int[] nums6 = {9,1,2,5,8,3};
		
		System.out.println("k: " + k);
		System.out.println("array 1: " + Arrays.toString(nums3));
		System.out.println("array 2: " + Arrays.toString(nums4));
		System.out.println("result: " + Arrays.toString(maxNumber(nums3, nums4, k)));
	}


  public static int[] maxNumber(int[] nums1, int[] nums2, int k) {
  	int m = nums1.length;
  	int n = nums2.length;
  	if (m > n) return maxNumber(nums2, nums1, k); // make sure nums2.length >= nums1.length
  	int[] result = new int[k];
  	for (int i = Math.max(k-n,0); i <= Math.min(k,n); i++) {
  		int[] sub1 = maxSubArray(nums1, Math.min(i,m));
  		int[] sub2 = maxSubArray(nums2, k-Math.min(i,m));
  		int[] current = merge(sub1, sub2);
  		result = max(current, result);
  	}
    return result;
  }

  /**
   * find a sub array that will evaluate to the largest number of x digits
   * x is guaranteed to be less than arr.length
   */
  public static int[] maxSubArray(int[] arr, int x) {
  	if (arr.length == 0 || x == 0) return new int[0];
  	int[] result = new int[x];
  	int discard = arr.length - x;
  	for (int i = 0, j = 0; i < arr.length; i++) {
  		while (discard > i-j && j > 0 && result[j-1] < arr[i]) {
  			j--;
  		}
  		if (j < x) {
				result[j] = arr[i];
				j++;
  		}
  	}
  	return result;
  }

  public static int[] merge(int[] arr1, int[] arr2) {
  	int len = arr1.length + arr2.length;
  	int[] result = new int[len];
  	int j = 0, k = 0;
  	for (int i = 0; k < len; k++) {
  		if (larger(arr1,i,arr2,j)) {
  			result[k] = arr1[i];
  			i++;
  		}
  		else {
  			result[k] = arr2[j];
  			j++;
  		}
  	}

  	return result;
  }

  // return true if a > b
  public static boolean larger(int[] a, int x, int[] b, int y) {
  	// find the next position they have different digit
  	while (x < a.length && y < b.length && a[x] == b[y]) {
  		x++;
  		y++;
  	}
  	return ((y == b.length) && (x < a.length)) || ((x < a.length) && a[x] > b[y]);
  }

  public static int[] max(int[] arr1, int[] arr2) {
  	for (int i = 0; i < arr1.length; i++) {
  		if (arr1[i] > arr2[i]) return arr1;
  		else if (arr2[i] > arr1[i]) return arr2;
  	}
  	return arr1;
  }

}