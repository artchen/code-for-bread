public class MaxProfit {

    public static void main(String[] args){
        int[] prices = {5, 4, 7, 9, 10, 16, 2, 4, 1, 6, 14, 11};
        
        System.out.println( maxProfit(prices) );
    }

    public static int maxProfit(int[] prices) {

        if (prices.length <= 1) { 
            return 0;
        }
        int total = 0;
        for (int i = 1; i < prices.length; i++) {
            if (prices[i] > prices[i-1]) {
                total += prices[i] - prices[i-1];
            }
        }
        return total;
    }

}