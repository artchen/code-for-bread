/**
 * Definition for binary tree
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class BalancedBinaryTree {
    public boolean isBalanced(TreeNode root) {
        if (root == null) {
           return true; 
        }
        return getDepth(root) == -1 ? false:true;
    }
    public int getDepth(TreeNode root) {
        if (root == null) 
            return 0;
        int ld = getDepth(root.left);
        int rd = getDepth(root.right);
        
        if (ld == -1 || rd == -1) return -1;
        if ( (ld-rd>1) || (rd-ld>1) ) return -1;
        
        return (ld > rd)?ld+1:rd+1;

    }
}