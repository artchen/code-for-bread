public class AddBinary {

    public static void main(String[] args) {
        String a = "1010";
        String b = "1011";
        System.out.println(addBinary("1010","1011"));
    }

    public static String addBinary(String a, String b) {
        StringBuilder sb = new StringBuilder();
        int i = a.length()-1, j = b.length()-1, carry = 0;
        int ad,bd;
        while (i >= 0 && j >= 0) {
            ad = a.charAt(i) - '0';
            bd = b.charAt(j) - '0';
            sb.insert(0, ad^bd^carry);
            carry = carry&(ad|bd)|(ad&bd);
            i--;
            j--;
        }
        while (i >= 0) {
            ad = a.charAt(i) - '0';
            sb.insert(0, ad^carry);
            carry = carry&ad;
            i--;
        }
        while (j >= 0) {
            bd = b.charAt(j) - '0';
            sb.insert(0, bd^carry);
            carry = carry&bd;
            j--;
        }
        if (carry > 0)
            sb.insert(0, carry);
        return sb.toString();
    }
}