import java.util.*;
public class BestTimeToBuyAndSellStockIV {
	
	public static void main(String[] args) {
		int k = 3;
		int[] prices = {4,2,3,2,5,2,4,1,3,1,6,2};
		int[] prices2 = {6,1,3,2,4,7};
		System.out.println(maxProfit(k,prices));
	}

	public static int maxProfit(int k, int[] prices) {
		if (prices.length <= 1) return 0;
		if (k >= prices.length) return maxProfitUnlimited(prices);
		int[][] current = new int[k+1][prices.length];
		int prev = 0;
		for (int i = 1; i < k+1; i++) {
			int tmp = 0;
			for (int j = 1; j < prices.length; j++) {
				int diff = prices[j] - prices[j-1];
				current[i][j] = Math.max(current[i][j-1], tmp+diff);
				tmp = Math.max(tmp+diff, current[i-1][j-1]);
			}
		}
		return current[k][prices.length-1];
	}


	public static int maxProfit2(int k, int[] prices) {
		if (prices.length <= 1) return 0;
		// if # of trasactions allowed > # of dates, it is equavalent to unlimited
		if (k >= prices.length) return maxProfitUnlimited(prices);

		for (int i = 0; i < prices.length-1; i++) {
			int diff = prices[i+1] - prices[i];
			for (int j = k-1; j >= 0; j--) {

			}
		}
		return 0;
	}

	public static int maxProfitUnlimited(int[] prices) {
    int total = 0;
    for (int i = 1; i < prices.length; i++) {
      if (prices[i] > prices[i-1]) total += prices[i] - prices[i-1];
    }
    return total;
	}

}