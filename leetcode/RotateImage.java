public class RotateImage {
	public static void main(String[] args){
		int[][] test_matrix = {
			{1,2,3,4},
			{5,6,7,8},
			{9,10,11,12},
			{13,14,15,16}
		};
		rotate(test_matrix);
		for (int i = 0; i < test_matrix.length; i++) {
			for (int j = 0; j < test_matrix[0].length; j++) {
				System.out.print(test_matrix[i][j] + " ");
			}
			System.out.println();
		}
	}
    public static void rotate(int[][] matrix) {
    	int len = matrix.length;
        int num_of_loop = (int)Math.floor((double)len/2);
        int temp = 0;
        if (matrix.length > 1) {
	        for (int i = 0; i < num_of_loop; i++) {
	        	for (int j = 0; j < len - 1; j++) {
	        		/* save temp */
	        		temp = matrix[i][j+i];
	        		/* rotate */
        			matrix[i][j+i] = matrix[len-1-j+i][i];
	        		matrix[len-1-j+i][i] = matrix[len-1+i][len-1-j+i];
	        		matrix[len-1+i][len-1-j+i] = matrix[j+i][len-1+i];
	        		matrix[j+i][len-1+i] = temp;
	        	}
	        	len = len - 2;
	        }
	    }
    }
}