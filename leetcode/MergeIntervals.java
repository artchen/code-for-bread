/**
 * Definition for an interval.
 * public class Interval {
 *     int start;
 *     int end;
 *     Interval() { start = 0; end = 0; }
 *     Interval(int s, int e) { start = s; end = e; }
 * }
 */
import java.util.*;
public class MergeIntervals {

	public static class Interval {
		public int start;
		public int end;
		public Interval() { start = 0; end = 0; }
		public Interval(int s, int e) { start = s; end = e; }

	}

	public static class IntervalComparator implements Comparator<Interval> {
    public int compare(Interval interval1, Interval interval2){
       return interval1.start - interval2.start;
    }
	}

	public static void main(String[] args) {

	}

	public static List<Interval> merge(List<Interval> intervals) {
		if (intervals.size() < 2) return intervals;
		IntervalComparator comparator = new IntervalComparator();
		Collections.sort(intervals, comparator);
		List<Interval> result = new ArrayList<Interval>();
		Interval current;
		int start = intervals.get(0).start, end = intervals.get(0).end;
		for (int i = 1; i < intervals.size(); i++) {
			current = intervals.get(i);
			if (end < current.start) {
				result.add(new Interval(start,end));
				start = current.start;
				end = current.end;
			}
			else {
				end = current.end > end ? current.end : end;
			}
			if (i == intervals.size() - 1) {
				result.add(new Interval(start,end));
			}
		}
		return result;
	}

}