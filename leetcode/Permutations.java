import java.util.*;
public class Permutations {

    public static boolean[] used;
    public static List<List<Integer>> result;
    public static List<Integer> arr;

    public static void main(String[] args) {
        int[] test = {1,2,3};
        permute(test);
        for (int i = 0; i < result.size(); i++)
            System.out.println(result.get(i).toString());
    }

    public static List<List<Integer>> permute(int[] num) {
        result = new ArrayList<List<Integer>>();
        arr = new ArrayList<Integer>();
        used = new boolean[num.length];
        permute(0, num);
        return result;
    }

    private static void permute(int index, int[] num) {
        if (index == num.length) {
            result.add( new ArrayList<Integer>(arr) );
            return;
        }
    	for (int i = 0; i < num.length; i++) {
    		if (!used[i]) {
    			arr.add(num[i]);
    			used[i] = true;
    			permute(index + 1, num);
    			used[i] = false;
    			arr.remove(arr.size()-1);
    		}
    	}  
    }

}