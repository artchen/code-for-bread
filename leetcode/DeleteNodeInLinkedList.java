/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
public class DeleteNodeInLinkedList {
  public void deleteNode(ListNode node) {
  	// the problem description said except the tail
  	// which means node.next != null
  	if (node == null) return;
    ListNode next = node.next;
    node.val = next.val;
    node.next = next.next;
  }
}