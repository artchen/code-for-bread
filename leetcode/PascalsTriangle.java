import java.util.*;

public class PascalsTriangle {
    public static void main(String[] args){
        ArrayList<ArrayList<Integer>> result = generate(3);

        for (int i = 0; i < result.size(); i++) {
            for (int j = 0; j < result.get(i).size(); j++) {
                System.out.print(result.get(i).get(j) + " ");
            }
            System.out.println();
        }
    }
    public static ArrayList<ArrayList<Integer>> generate(int numRows) {
        ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
        ArrayList<Integer> current_arr = new ArrayList<Integer>();
        ArrayList<Integer> prev_arr = current_arr;

        for (int i = 1; i <= numRows; i++) {
            current_arr = new ArrayList<Integer>();

            for (int j = 0; j < i; j++) {
                if (j == 0 || j == i - 1) {
                    current_arr.add(1);
                }
                else {
                    current_arr.add( prev_arr.get(j-1) + prev_arr.get(j) );  
                }
            }
            result.add(current_arr);
            prev_arr = current_arr;
        }
        return result;
    }
}