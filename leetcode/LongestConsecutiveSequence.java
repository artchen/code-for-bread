import java.util.*;
public class LongestConsecutiveSequence {

	public static void main(String[] args) {
		int[] test = {100,4,200,1,3,2};
		System.out.println(longestConsecutive(test));
	}

  public static int longestConsecutive(int[] nums) {
  	if (nums.length == 0) return 0;
  	Set<Integer> hs = new HashSet<Integer>();
  	for (int i : nums) {
  		hs.add(i);
  	}
  	int max = 0;
  	for (int curr : nums) {
  		int sm = curr-1;
  		int lg = curr+1;
  		int count = 1;
  		while (hs.contains(sm)) {
  			hs.remove(sm);
  			sm--;
  			count++;
  		}
  		while (hs.contains(lg)) {
  			hs.remove(lg);
  			lg++;
  			count++;
  		}
  		hs.remove(curr);
  		max = max > count ? max : count;
  	}
  	return max;
  }

}