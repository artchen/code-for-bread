public class SingleNumber {

	public static void main(String[] args){
		int[] X = {132, 657, 384, 132, 384, 875, 657, 984, 984};
		System.out.println(singleNumber(X));
	}

    public static int singleNumber(int[] A) {
        int r = 0;
        for (int i=0; i<A.length; i++ ) {
            r = r ^ A[i];
            System.out.println(r);
        }
        return r;
    }
}