import java.util.*;
public class InterleavingString {
	
	public static void main(String[] args) {
		String s1 = "ab";
		String s2 = "bc";
		String s3 = "babc";
		System.out.println(isInterleave(s1,s2,s3));
	}

	// good dp solution
	public static boolean isInterleave(String s1, String s2, String s3) {
		int len1 = s1.length(), len2 = s2.length(), len3 = s3.length();
		if (len1+len2 != len3) return false;
		char[] ss1 = s1.toCharArray();
		char[] ss2 = s2.toCharArray();
		char[] ss3 = s3.toCharArray();
		boolean[][] match = new boolean[len1+1][len2+1];
		for (int i = 0; i <= len1; i++) {
			for (int j = 0; j <= len2; j++) {
				if (i==0&&j==0) match[i][j] = true;
				else if (i == 0) {
					if (ss2[j-1]==ss3[i+j-1]) match[i][j] = match[i][j-1];
				}
				else if (j == 0) {
					if (ss1[i-1]==ss3[i+j-1]) match[i][j] = match[i-1][j];
				}
				else {
					if (ss1[i-1]==ss3[i+j-1]) match[i][j] = match[i-1][j] || match[i][j];
					if (ss2[j-1]==ss3[i+j-1]) match[i][j] = match[i][j-1] || match[i][j];
				}
			}
		}
		return match[len1][len2];
	}

}