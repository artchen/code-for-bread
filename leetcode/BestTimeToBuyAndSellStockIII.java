import java.util.*;
public class BestTimeToBuyAndSellStockIII {
	
	public static void main(String[] args) {
		int[] prices = {1,2,4};
		System.out.println(maxProfit(prices));
	}

	public static int maxProfit(int[] prices) {
		if (prices.length < 2) return 0;
		if (prices.length == 2) return Math.max(prices[1]-prices[0],0);
		int[] left = new int[prices.length];
		int[] right = new int[prices.length];
		findLocalMax(prices, left, right);
		System.out.println(Arrays.toString(left));
		System.out.println(Arrays.toString(right));
		int max = Integer.MIN_VALUE;
		for (int i = 0; i < prices.length-1; i++) {
			max = max < left[i]+right[i] ? left[i]+right[i] : max;
		}
		return max;
	}

	public static void findLocalMax(int[] prices, int[] left, int[] right) {
		int minBuy = 0;
		left[0] = 0;
		for (int i = 1; i < left.length; i++) {
			int diff = prices[i] - prices[minBuy];
			left[i] = left[i-1];
			if (diff > left[i-1])
				left[i] = diff;
			else if (diff < 0)
				minBuy = i;
		}

		int maxSell = right.length-1;
		right[right.length-1] = 0;
		for (int j = right.length-2; j >= 0; j-- ) {
			int diff = prices[maxSell] - prices[j];
			right[j] = right[j+1];
			if (diff > right[j+1])
				right[j] = diff;
			else if (diff < 0)
				maxSell = j;
		} 
	}

}