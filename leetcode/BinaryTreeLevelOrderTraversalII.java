import java.util.*;
/**
 * Definition for binary tree
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class BinaryTreeLevelOrderTraversalII {
    public static List<List<Integer>> result;
    public static ArrayList<Integer> level_al;
    public static int height = 0;
    public static int height_left = 0;
    public static int height_right = 0;

    public static void main(String[] args){
        TreeNode root = new TreeNode(0);
        root.left = new TreeNode(-3);
        root.right = new TreeNode(-2);
        root.left.left = new TreeNode(-9);
        root.left.right = new TreeNode(-8);
        root.left.left.left = new TreeNode(2);
        root.left.right.right = new TreeNode(-5);
        root.left.right.right.left = new TreeNode(5);

        List<List<Integer>> test = levelOrderBottom(root);

        for (int i = 0; i < test.size(); i ++) {
            ArrayList<Integer> test_level = (ArrayList<Integer>)test.get(i);
            for (int j = 0; j < test_level.size(); j++) {
                System.out.print(test_level.get(j));
            }
            System.out.println();
        }
    }
    
    public static List<List<Integer>> levelOrderBottom(TreeNode root) {
        result = new ArrayList<List<Integer>>();
        if (root == null) return result;
        height = getHeightofTree(root);
        for (int i = height; i > 0; i--) {
            level_al = new ArrayList<Integer>();
            levelOrderBottomRecur(root, i);
            result.add(level_al);
        }
        return result;
    }
    public static void levelOrderBottomRecur(TreeNode root, int level) {
        if (root == null) {
            return;
        }
        if (level == 1) {
            level_al.add(root.val);
        }
        else {
            levelOrderBottomRecur(root.left, level - 1);
            levelOrderBottomRecur(root.right, level - 1);
        }
    }
    public static int getHeightofTree(TreeNode root) {
        if (root == null) {
            return 0; 
        }
        if (root.left == null && root.right == null) {
            return 1;
        }
        height_left = getHeightofTree(root.left);
        height_right = getHeightofTree(root.right);
        if (height_left > height_right) {
            return 1 + height_left;
        }
        return 1 + height_right;
    }
}