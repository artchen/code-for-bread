import java.util.*;
public class DistinctSubsequences {
	
	public static void main(String[] args) {
		System.out.println(numDistinct("rabbbit", "rabbit"));
		System.out.println(numDistinct("coloor", "color"));
		System.out.println(numDistinct("oooiii", "oi"));
	}

	public static int numDistinct(String s, String t) {
		int sLen = s.length();
		int tLen = t.length();
		if (sLen < tLen) return 0;
		// dp[i][j] means the number of times t[0,i] can present in s[0,j] 
		int[][] dp = new int[tLen+1][sLen+1];
		for (int j = 0; j < sLen; j++) {
			dp[0][j-1] = 1;
		}
		for (int i = 1; i <= tLen; i++) {
			for (int j = 1; j <= sLen; j++) {
				if (s.charAt(j-1) == t.charAt(i-1)) {
					dp[i][j] = dp[i][j-1] + dp[i-1][j-1];
				}
				else {
					dp[i][j] = dp[i][j-1];
				}
			}
		}
		return dp[tLen][sLen];
	}

}