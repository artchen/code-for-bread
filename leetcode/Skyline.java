import java.util.*;
public class Skyline {
	public List<int[]> getSkyline(int[][] buildings) {
		ArrayList<int[]> res = new ArrayList<int[]>();
		if (buildings.length==0) return res;
		TreeMap<Integer,Integer> endHeights = new TreeMap<Integer,Integer>();
		PriorityQueue<Integer> endPos = new PriorityQueue<Integer>();
		int curHeight = 0;
		for(int i = 0; i< buildings.length; i++){
			endHeights.put(buildings[i][2],buildings[i][1]);
			endPos.add(buildings[i][1]);
			while (i<buildings.length-1 && buildings[i][0]==buildings[i+1][0]){
				endHeights.put(buildings[i+1][2],buildings[i+1][1]);
				endPos.add(buildings[i+1][1]);
				i++;
			}
			if (endHeights.lastKey() > curHeight) {
				curHeight = endHeights.lastKey();
				res.add( new int[]{buildings[i][0], curHeight} );
			} 
			while (!endPos.isEmpty() && (i==buildings.length-1 || buildings[i+1][0]>endPos.peek() ) ){
				int pos = endPos.poll();
				endHeights.values().remove(pos);
				int height = endHeights.size() == 0 ? 0 : endHeights.lastKey();
				if (height < curHeight) 
					res.add( new int[]{ pos, curHeight = height} );
			}
		}
		return res;
	}
}
