public class MaximumSubarray {

	public static void main(String[] args){
		int[] test = {-2, 1, -3, 4, -1, 2, 1, -5, 4};
		System.out.println( maxSubArray(test) );
	}

    public static int maxSubArray(int[] A) {

    	int max = A[0];
    	int max_tail = A[0];
    	
    	for (int i = 1; i < A.length; i++) {
    		if (max_tail < 0) {
    			max_tail = A[i];
    		}
    		else {
    			max_tail += A[i];
    		}
    		if (max <= max_tail) {
    			max = max_tail;
    		}
    	}
        
        return max;
    }

}