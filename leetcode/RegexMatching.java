public class RegexMatching {

	public static void main(String[] args) {
		String s1 = "cabbbacccbbacccbcba";
		String p1 = ".b*.*a*..b*.*aa*a*c";

		String s2 = "aaaaaaaaaaaaab";
		String p2 = "a*a*a*a*a*a*a*a*a*a*c";

		String s3 = "abcd";
		String p3 = "d*";

		String s4 = "aa";
		String p4 = "a*";

		String s5 = "aab";
		String p5 = "c*a*b";

		System.out.println(isMatch(s1,p1));
		System.out.println(isMatch(s2,p2));
		System.out.println(isMatch(s3,p3));
		System.out.println(isMatch(s4,p4));
		System.out.println(isMatch(s5,p5));
	}

	public static boolean isMatch(String s, String p) {
    int pp = 0, sp = 0, sLen = s.length(), pLen = p.length();
    while (sp <= sLen && pp <= pLen) {
    	if (pp >= p.length()) return sp >= s.length();
    	if (pp == p.length()-1) {
    		if (sp >= s.length()) return false;
    		else if (p.charAt(pp) != s.charAt(sp) && p.charAt(pp) != '.') return false;
    		else {
    			sp ++;
    			pp ++;
    		} 
    	}
    	else {
    		char p0 = p.charAt(pp);
    		char p1 = p.charAt(pp+1);
    		if (p1 != '*') {
    			if (sp >= s.length()) return false;
    			else if (p0 != '.' && p0 != s.charAt(sp)) return false;
    			else {
    				sp++;
    				pp++;
    			}
    		}
    		else {
    			// TODO
    			/*
    			if (recursive(s, sp, p, pp+2)) {
		      	return true;
		      }
		    	int i = 0;
		    	// otherwise have to try all substrings
		      while (sp+i < s.length() && (s.charAt(sp+i) == p0 || p0 == '.') ) {
		        if (recursive(s, sp+i+1, p, pp+2)) return true;
		        i++;
		      }
		      */
    		}
    	}
    }
    return false;
	}

	// my original recursive solution
	public static boolean isMatch2(String s, String p) {
		return recursive(s,0,p,0); 
	}

	public static boolean recursive(String s, int sp, String p, int pp) {

		// case 1: empty pattern only matches empty string
    if (pp >= p.length()) return sp >= s.length();
        
    // case 2: only 1 char in p
		if (pp == p.length()-1) {
	    if (sp >= s.length()) return false;
	  	else if (p.charAt(pp)!=s.charAt(sp) && p.charAt(pp)!='.') return false;
	    else return recursive(s, sp+1, p, pp+1);
		}

		// case 3: recursively check
    char p0 = p.charAt(pp);
    char p1 = p.charAt(pp+1);
        
    // if p[1] != '*', continue recursive match when either p[0]==s[0] or p[0]=='.'
    if (p1 != '*') {
      if (sp >= s.length()) return false;
      else if (p0=='.' || p0 == s.charAt(sp)) return recursive(s, sp+1, p, pp+1);
      else return false;
    }
    // if p[1] == '*'
  	else {
      if (recursive(s, sp, p, pp+2)) {
      	return true;
      }
    	int i = 0;
    	// otherwise have to try all substrings
      while (sp+i < s.length() && (s.charAt(sp+i) == p0 || p0 == '.') ) {
        if (recursive(s, sp+i+1, p, pp+2)) return true;
        i++;
      }
    }
        
	  return false;
  }

}