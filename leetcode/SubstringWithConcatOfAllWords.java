import java.util.*;
public class SubstringWithConcatOfAllWords {

	public static void main (String[] args) {
		String s = "wordgoodgoodgoodbestword";
		String[] words = {"word", "good", "best", "good"};
		System.out.println(findSubstring(s,words).toString());
	}

	// TODO: find a better solution, check by words, not by characters
	// if bad word, discard all and start from next word
	// if too many good word, advance by 1 word
	// if passed, advance by 1 character 
	public static List<Integer> findSubstring(String s, String[] words) {
		List<Integer> result = new ArrayList<Integer>();
		int wordLen = words[0].length();
		int wordCnt = words.length;
		int concatLen = wordCnt*wordLen;
		int strLen = s.length();
		if (wordCnt == 0 || concatLen > strLen) return result;
		// hashtable for recording occurance
		HashMap<String,Integer> ht = new HashMap<String,Integer>();
		for (int i = 0; i < wordCnt; i++) {
			if (!ht.containsKey(words[i])) ht.put(words[i], 1);
			else ht.put(words[i], ht.get(words[i]) + 1);
		}
		int k = 0;
		for (int j = 0; j <= strLen-concatLen; j++) {

		}
		return result;
	}

	public static boolean check() {

		return false;
	}

	public static List<Integer> findSubstring2(String s, String[] words) {
		List<Integer> result = new ArrayList<Integer>();
		int wordLen = words[0].length();
		int wordCnt = words.length;
		int concatLen = wordCnt*wordLen;
		int strLen = s.length();
		if (wordCnt == 0 || concatLen > strLen) return result;
		
		HashMap<String,Integer> ht = new HashMap<String,Integer>();
		for (int i = 0; i < wordCnt; i++) {
			if (!ht.containsKey(words[i])) ht.put(words[i], 1);
			else ht.put(words[i], ht.get(words[i]) + 1);
		}
		for (int j = 0; j <= strLen-concatLen; j++) {
			// check substring
			if (check2(s,j,ht,wordLen,wordCnt,concatLen)) {
				result.add(j);
			}
		}
		return result;
	}

	public static boolean check2(String s, int start, HashMap<String,Integer> ht, int wordLen, int wordCnt, int concatLen) {
		HashMap<String,Integer> ht_tmp = new HashMap<String,Integer>();
		for (int i = start; i <= start+concatLen-wordLen; i = i+wordLen) {
			String sub = s.substring(i,i+wordLen);
			if (!ht.containsKey(sub)) return false;
			if ( ht_tmp.containsKey(sub) ) {
				ht_tmp.put(sub,ht_tmp.get(sub)+1);
			}
			else {
				ht_tmp.put(sub,1);
			}
			if (ht_tmp.get(sub) > ht.get(sub)) return false;
		}
		return true;
	}

}