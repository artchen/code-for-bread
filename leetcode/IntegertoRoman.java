public class IntegertoRoman {
	public static void main(String[] args) {
        int test = 787;
        System.out.println( intToRoman(test) );
    }
    public static String intToRoman(int num) {
        int M = num / 1000;
        int C = (num - M*1000) / 100;
        int X = (num - M*1000 - C*100) / 10;
        int I = num - M*1000 - C*100 - X*10;
        String result = "";

        for (int i = 0; i < M; i++) {
        	result += "M";
        }

        if (C == 4) {
        	result += "CD";
        }
        else if (C > 4 && C < 9) {
        	result += "D";
        	for (int i = 5; i < C; i++) {
	        	result += "C";
	        }
        }
        else if (C == 9) {
        	result += "CM";
        }
        else {
        	for (int i = 0; i < C; i++) {
	        	result += "C";
	        }
        }
        

        if (X == 4) {
        	result += "XL";
        }
        else if (X > 4 && X < 9) {
        	result += "L";
        	for (int i = 5; i < X; i++) {
	        	result += "X";
	        }
        }
        else if (X == 9) {
        	result += "XC";
        }
        else {
        	for (int i = 0; i < X; i++) {
	        	result += "X";
	        }
        }
        

        if (I == 4) {
        	result += "IV";
        }
        else if (I > 4 && I < 9) {
        	result += "V";
        	for (int i = 5; i < I; i++) {
	        	result += "I";
	        }
        }
        else if (I == 9) {
        	result += "IX";
        }
        else {
        	for (int i = 0; i < I; i++) {
	        	result += "I";
	        }
        }

        return result;
        

    }

    
}