import java.util.*;
public class TopThreeElementsInArray {

	static class RevCompare implements Comparator<Integer>  {
		public int compare (Integer a, Integer b) {
			return b-a;
		}
	}
	
	public static void main (String[] args) {
		int[][] test = {
			{1,4,5,6,3,3,4,5,6,7,8,5,6},
			{34,7,619,34,75,16,71,861,73},
			{1,1,1,1,1,1,1}
		};
		for (int[] t : test)
			System.out.println( Arrays.toString(find(t)) );
	}

	public static int[] find (int[] list) {
		int[] result = new int[3];
		RevCompare rc = new RevCompare();
		PriorityQueue<Integer> queue = new PriorityQueue<Integer>(10, rc);
		for (int i = 0; i < list.length; i++) {
			queue.add(list[i]);
		}
		for (int i = 0; i < result.length; i++) {
			result[i] = queue.poll();
		}
		return result;
	}
}