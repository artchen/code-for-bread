public class LRUDriver {
	public static void main(String[] args) {
		LRUCache cache = new LRUCache(2);
		cache.set(2,1);
		print("Passed: cache.set(2,1);");

		cache.set(1,1);
		print("Passed: cache.set(1,1);");

		cache.get(2);
		print("Passed: cache.get(2);");
		cache.print();

		cache.set(4,1);
		print("Passed: cache.set(4,1);");
		cache.print();

		cache.get(1);
		print("Passed: cache.get(1);");
		cache.print();

		cache.get(2);
		print("Passed: cache.get(2);");
		cache.print();
	}
	public static void print(String msg) {
		System.out.println(msg);
	}
}