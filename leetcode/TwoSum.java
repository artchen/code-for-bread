import java.util.*;
public class TwoSum {
	
	public static void main (String[] args) {
		int[] nums = {1,2,3,4,3,2,1};
		print( twoSum(nums, 5) );
	}

	public static void print (List<List<Integer>> list) {
		for (List<Integer> elem : list) {
			System.out.println(elem);
		}
	}

	public static List<List<Integer>> twoSum (int[] nums, int tar) {
		HashMap<Integer,Integer> map = new HashMap<Integer,Integer>();
		List<List<Integer>> result = new LinkedList<List<Integer>>();
		for (int i = 0; i < nums.length; i++) {
			int tmp = tar-nums[i];
			if (map.containsKey(tmp)) {
				int count = map.get(tmp);
				if (count > 0) {
					map.put(tmp, map.get(tmp)-1);
					LinkedList<Integer> ans = new LinkedList<Integer>();
					ans.add(tmp);
					ans.add(nums[i]);
					result.add(ans);
				}
				else {
					if (map.containsKey(nums[i])) map.put(nums[i], map.get(nums[i])+1);
					else map.put(nums[i], 1);
				}
			}
			else {
				map.put(nums[i], 1);
			}
		}
		return result;
	}

}