import java.util.*;
public class MedianOfSortedArrays {

    public static void main(String[] args) {
        int[] n1 = {1,1,2,3,5,8};
        int[] n2 = {1,2,3,4,5,6};

        int[] n3 = {1,4,5,6};
        int[] n4 = {2,3,7,8};

        int[] n5 = {3,3,3,3};
        int[] n6 = {3,3,3,3};

        System.out.println(findMedianSortedArrays(n3,n4));
    }

    public static double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int n = nums1.length, m = nums2.length;
        if (n == 0 && m == 0)
            return 0;
        else if (n == 0) {
            if (m % 2 == 0)
                return median2(nums2[(m-1)/2], nums2[(m-1)/2+1]);
            else
                return nums2[(m-1)/2];
        }
        else if (m == 0) {
            if (n % 2 == 0)
                return median2(nums1[(n-1)/2], nums1[(n-1)/2+1]);
            else
                return nums1[(n-1)/2];
        }
        return find(nums1, 0, n-1, nums2, 0, m-1);
    }
    
    /* Find median of 2 items */
    public static double median2(int x1, int x2) {
        return (x1+x2)/2.0;
    }
    
    /* Find median of 3 items */
    public static double median3(int x1, int x2, int x3) {
        return Math.max(Math.min(x1,x2), Math.min(Math.max(x1,x2),x3));
    }
    
    /* Find median of 4 items */
    public static double median4(int x1, int x2, int x3, int x4) {
        int max = Math.max(Math.max(Math.max(x1,x2),x3),x4);
        int min = Math.min(Math.min(Math.min(x1,x2),x3),x4);

        if (max == x1 && min == x2 || min == x1 && max == x2) return (x3+x4)/2.0;
        else if (max == x1 && min == x3 || min == x3 && max == x1) return (x2+x4)/2.0;
        else if (max == x1 && min == x4 || min == x1 && max == x4) return (x2+x3)/2.0;
        else if (max == x2 && min == x3 || min == x2 && max == x3) return (x1+x4)/2.0;
        else if (max == x2 && min == x4 || min == x2 && max == x4) return (x1+x3)/2.0;
        
        return (x1+x2)/2.0;
    }
    
    // assume a.length <= b.length
    public static double find(int[] a, int a0, int an, int[] b, int b0, int bn) {
        int n = an-a0+1, m = bn-b0+1;
        int mid_a = (an+a0)/2;
        int mid_b = (bn+b0)/2;

        // make sure size(a) > size(b)
        if (a.length > b.length || n > m) {
            return find(b, b0, bn, a, a0, an);
        }
        // 1 left for smaller array
        if (n == 1) {
            // case 1: also 1 left for larger array
            if (m == 1) {
                return median2(a[a0],b[b0]);
            }
            // case 2: even left for larger array
            else if (m % 2 == 0) {
                return median3(b[mid_b], b[mid_b+1], a[a0]);
            }
            // case 3: odd left for larger array
            else {
                return median4(b[mid_b], b[mid_b-1], b[mid_b+1], a[a0]);
            }
        }
        // 2 left for smaller array
        if (n == 2) {
            // case 4: also 3 left for larger array
            if (m == 2) {
                return median4(a[a0],a[an],b[b0],b[bn]);
            }
            // case 5: even left for larger array
            else if ( m % 2 == 0 ) {
                int[] tmp = {a[a0], a[an], b[mid_b-1], b[mid_b], b[mid_b+1], b[mid_b+2]};
                Arrays.sort(tmp);
                return median2(tmp[2],tmp[3]);
            }
            // case 6: odd left for larger array
            else {
                int[] tmp = {a[a0], a[an], b[mid_b-1], b[mid_b], b[mid_b+1]};
                Arrays.sort(tmp);
                return tmp[2];
            }
        }
         // 3 left for smaller array
        if (n == 3) {
            // case 7: also 3 left for larger array
            if (m == 3) {
                int[] tmp = {a[a0], a[a0+1], a[an], b[b0], b[b0+1], b[bn]};
                Arrays.sort(tmp);
                return median2(tmp[2], tmp[3]);
            }
            // case 8: even left for larger array
            else if ( m % 2 == 0) {
                int[] tmp = {a[a0], a[a0+1], a[an], b[mid_b], b[mid_b-1], b[mid_b+1], b[mid_b+2]};
                Arrays.sort(tmp);
                return tmp[3];
            }
            // case 9: odd left for larger array
            else {
                int[] tmp = {a[a0], a[a0+1], a[an], b[mid_b-2], b[mid_b-1], b[mid_b], b[mid_b+1], b[mid_b+2]};
                Arrays.sort(tmp);
                return median2(tmp[3], tmp[4]);
            }
        }
        

        if (a[mid_a] > b[mid_b]) {
            return find(a, a0, mid_a+1, b, b0+an-mid_a-1, bn);
        }
        
        return find(a, mid_a, an, b, b0, bn-mid_a+a0);
    }
}