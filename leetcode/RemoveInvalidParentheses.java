import java.util.*;

public class RemoveInvalidParentheses {

	public static void main(String[] args) {
		//System.out.println(removeInvalidParentheses("()())()"));
		System.out.println(removeInvalidParentheses("((z(((fp))()((((((g("));
	}

	// Better dfs solution
	public static List<String> removeInvalidParentheses(String s) {
		Set<String> result = new HashSet<>();
    int rml = 0, rmr = 0;
    for (int i = 0; i < s.length(); i++) {
      if (s.charAt(i) == '(') rml++;
      if (s.charAt(i) == ')') {
        if (rml != 0) rml--;
        else rmr++;
      }
    }
    dfs(s, result, 0, rml, rmr, 0, new StringBuilder());
    return new ArrayList<String>(result);
	}

	public static void dfs(String s, Set<String> result, int i, int rml, int rmr, int open, StringBuilder sb) {
		int n = s.length();
		// scan complete: if string is parentheses valid, push to result
		if (i == n && rml == 0 && rmr == 0 && open == 0) {
      result.add(sb.toString());
      return;
    }
    // scan complete: if not valid, do not push
    if (i == n || rml < 0 || rmr < 0 || open < 0) return;

		char c = s.charAt(i);
		int len = sb.length();
		if (c == '(') {
			dfs(s,result,i+1,rml-1,rmr,open,sb);
			dfs(s,result,i+1,rml,rmr,open+1,sb.append(c));
		}
		else if (c == ')') {
			dfs(s,result,i+1,rml,rmr-1,open,sb);
			dfs(s,result,i+1,rml,rmr,open-1,sb.append(c));
		}
		else {
			dfs(s,result,i+1,rml,rmr,open,sb.append(c));
		}
		sb.setLength(len);
	}

	// my original solution using hashset, beats 50%
  public static List<String> removeInvalidParentheses2(String s) {
  	List<String> result = new ArrayList<String>();
  	HashSet<String> hs = new HashSet<String>();
    String tmp;
    String curr;
    char c;
    ArrayDeque<String> queue = new ArrayDeque<String>();
    boolean valid = false;
    boolean found = false;
    queue.add(s);
    // bfs
    while(!queue.isEmpty()) {
    	curr = queue.poll();
    	//System.out.println(curr);
    	if (validate(curr)) {
    		result.add(curr);
    		found = true;
    	}
    	if (found) continue;
    	for (int i = 0; i < curr.length(); i ++) {
    		c = curr.charAt(i);
    		if (c == '(' || c == ')') {
    			tmp = curr.substring(0,i) + curr.substring(i+1);
    			if (!hs.contains(tmp)) {
    				queue.add(tmp);
    				hs.add(tmp);
    			}
    		}
    	}
    	//System.out.println(queue);
    }
    return result;
  }

  public static boolean validate(String s) {
    int count = 0;
    for (int i = 0; i < s.length(); i++) {
      char c = s.charAt(i);
      if (c == '(') count++;
      else if (c == ')') count--;
      if (count < 0) return false;
    }
    return count == 0;
  }

}