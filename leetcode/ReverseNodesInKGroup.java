/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
public class ReverseNodesInKGroup {

    public static void main(String[] args) {
        ListNode head = new ListNode(1);
        head.next = new ListNode(2);
        head.next.next = new ListNode(3);
        head.next.next.next = new ListNode(4);
        head.next.next.next.next = new ListNode(5);
        head.next.next.next.next.next = new ListNode(6);
        print(reverseKGroup(head,3));
    }

    public static void print(ListNode head) {
        ListNode itr = head;
        while (itr != null) {
            System.out.print(itr.val);
            itr = itr.next;
        }
        System.out.println();
    }

    public static ListNode reverseKGroup(ListNode head, int k) {
        if (head == null || head.next == null) return head;
        if (k <= 1) return head;
        ListNode start = head, end = head, prevEnd = null;
        int i = 0;
        while (end != null) {
            //System.out.println(end.val + ", " + i);
            if (i == k-1) {
                ListNode curr = start.next;
                ListNode next = null;
                ListNode prev = start;
                start.next = end == null ? null : end.next;
                for (i = i-1; i >= 0; i--) {
                    next = curr.next;
                    curr.next = prev;
                    prev = curr;
                    curr = next;
                }
                if (start == head) {
                    head = end;
                }
                else {
                    prevEnd.next = prev;
                }
                prevEnd = start;
                end = start;
                start = start.next;
                i = 0;
            }
            else i++;
            end = end.next;
        }
        return head;
    }
}