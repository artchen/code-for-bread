public class ExcelSheetColumnNumber {
	
	public static void main(String[] args) {
		System.out.println(titleToNumber("ABC"));
	}

  public static int titleToNumber(String s) {
  	int sLen = s.length();
  	int number = 0;
  	for (int i = sLen-1; i >= 0; i--) {
  		int c = s.charAt(sLen-i-1) - 'A' + 1;
  		number += c*Math.pow(26,i);
  	}
  	return number;
  }

}