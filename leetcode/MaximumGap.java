public class MaximumGap {

	public static void main(String[] args) {

	}

	// space O(2N) time O(4N)
  public static int maximumGap(int[] nums) {
  	if (nums.length < 2) return 0;
    

    // find the max and min element in the array of numbers
    int max = nums[0];
    int min = nums[0];
    for (int i = 0; i < nums.length; i++) {
      max = max > nums[i] ? max : nums[i];
      min = min < nums[i] ? min : nums[i];
    }
    

    // populate minInHole with Integer.MAX_VALUE
    int[] maxInHole = new int[nums.length];
    int[] minInHole = new int[nums.length];
    for (int i = 0; i < minInHole.length; i++) {
      minInHole[i] = Integer.MAX_VALUE;
    }
    

    // assign elements to holes
    // find min and max in each hole
    // note that if there are > 2 elements, there must be some gap between 
    // holes (one or more empty holes). max[i]-min[i] never greater than hole size,
    // therefore we only need to save min and max within a hole.
    double holeSize = (double)(max-min)/(double)(nums.length-1);
    for (int i = 0; i < nums.length; i++ ){
      int index = holeSize == 0 ? 0 : (int)((nums[i]-min)/holeSize);
      maxInHole[index] = maxInHole[index] > nums[i] ? maxInHole[index] : nums[i];
      minInHole[index] = minInHole[index] < nums[i] ? minInHole[index] : nums[i];
    }
    

    // find max gap
    // between holes: current.min-prev.max
    // within a hole: current.max-current.min
    int prev = maxInHole[0];
    int maxGap = maxInHole[0]-minInHole[0];
    for (int i = 1; i < nums.length; i++) {
      if (minInHole[i] != Integer.MAX_VALUE) {
        maxGap = maxGap > (maxInHole[i]-minInHole[i]) ? maxGap : (maxInHole[i]-minInHole[i]);
        maxGap = maxGap > (minInHole[i]-prev) ? maxGap : minInHole[i]-prev;
        prev = maxInHole[i];
      }
    }
    return maxGap;
  }

}