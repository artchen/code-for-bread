public class BinarySearch {
	
	public static void main(String[] args) {
		int[] test = {1, 5, 7, 9, 13, 18, 39, 59, 100, 198, 283, 487, 598};
		int result = binarySearch(18, 0, test.length-1, test);
		System.out.println(result);
	}

	public static int binarySearch(int key, int min, int max, int[] arr) {
		while (min <= max) {
			int mid = (min + max) / 2;
			if (key == arr[mid]) {
				return mid;
			}
			else if (key > arr[mid]) {
				min = mid + 1;
			}
			else {
				max = mid - 1;
			}
		}
		return -1;
	}
}