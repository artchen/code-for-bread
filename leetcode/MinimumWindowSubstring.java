import java.util.*;
public class MinimumWindowSubstring {

	public static void main(String[] args) {
		String s = "caae";
		String t = "cae";
		System.out.println(minWindow(s,t));
	}

	// improved solution 72ms
	public static String minWindow(String s, String t) {
		int tLen = t.length();
		int sLen = s.length();
		if (tLen > sLen) return "";

		// build reference hashmap from T
		HashMap<Character,Integer> tMap = new HashMap<Character,Integer>();
		for (int i = 0; i < tLen; i++) {
			char c = t.charAt(i);
			if (tMap.containsKey(c)) tMap.put(c,tMap.get(c)+1);
			else tMap.put(c,1);
		}

		// traverse string S
		HashMap<Character,Integer> sMap = new HashMap<Character,Integer>();
		int left = 0, right = 0; 		// 2 pointers
		int cnt = 0; 								// number of valid char found so far
		int start = 0, end = sLen; 	// saved pointers of the result window
		boolean valid = false;			// true if at least 1 valid window is found
		while (left < sLen && right < sLen) {
			char r = s.charAt(right);
			char l = s.charAt(left);
			if (tMap.containsKey(r)) {
				if (sMap.containsKey(r)) {
					// only increment cnt when we haven't found enough of this char
					if (sMap.get(r) < tMap.get(r)) cnt++;
					sMap.put(r,sMap.get(r)+1);
				}
				else {
					sMap.put(r,1);
					cnt++;
				}
			}
			// if cnt == tLen, we have a valid window
			if (cnt == tLen) {
				valid = true;
				// case 1: we have more leading char than needed (l == r)
				// e.g. need "abc", current window = "abckka"
				// case 2: if the leading char is invalid (not in T)
				// e.g. need "abc", current window = "kbca"
				while (!sMap.containsKey(l) || sMap.get(l) > tMap.get(l)) {
					// if case 1, decrement char count in sMap
					if (sMap.containsKey(l) && sMap.get(l) > tMap.get(l)) {
						sMap.put(l, sMap.get(l)-1);
					}
					// for both case, advance left pointer
					left++;
					l = s.charAt(left);
				}
				// update minimum window 
				if (right - left + 1 < end - start) {
					start = left;
					end = right + 1;
				}
			}
			right ++;
		}

		return valid ? s.substring(start,end) : "";
	}

	// original solution 79ms
	public static String minWindow2(String s, String t) {
		int strLen = s.length();
		HashMap<Character,Integer> ht = new HashMap<Character,Integer>();
		for (int i = 0; i < t.length(); i++) {
			char c = t.charAt(i);
			if (ht.containsKey(c)) ht.put(c,ht.get(c)+1);
			else ht.put(c,1);
		}
		int left = 0, right = 0, start = 0, end = strLen, size = 0;
		boolean valid = false;
		HashMap<Character,Integer> tmp = new HashMap<Character,Integer>();
		while (left < strLen && right < strLen) {
			char l = s.charAt(left);
			char r = s.charAt(right);
			boolean leftGood = ht.containsKey(l);
			boolean rightGood = ht.containsKey(r);
			//System.out.println("l: " + l + ", r: " + r + ", " + s.substring(left,right+1));

			if (rightGood) {
				if (tmp.containsKey(r)) tmp.put(r, tmp.get(r)+1);
				else tmp.put(r,1);
				size++;
			}
			if (leftGood && left != right) {
				tmp.put(l,Math.max(tmp.get(l),1));
				size++;
			}

			if (leftGood && rightGood && r == l && left != right && tmp.get(r) > ht.get(r)) {
				tmp.put(r, tmp.get(r)-1);
				left++;
				while (left <= right) {
					char rt = s.charAt(left);
					if (tmp.containsKey(rt)) {
						if (tmp.get(rt) > ht.get(rt)) {
							tmp.put(rt,tmp.get(rt)-1);
							left++;
						}
						else break;
					}
					else left++;
				}
			}
			//System.out.println(tmp);
			if (valid || (size >= t.length() && leftGood && rightGood && check(ht,tmp)) ) {
			    valid = true;
				if (end-start > right-left+1) {
					start = left;
					end = right+1;
				}
			}
			if (!leftGood) left++;
			right++;
		}

		return valid ? s.substring(start,end) : "";
	}

	public static boolean check(HashMap<Character,Integer> ht, HashMap<Character,Integer> tmp) {
		for (Map.Entry<Character,Integer> entry : ht.entrySet()) {
			Character key = entry.getKey();
			Integer val = entry.getValue();
			if (!tmp.containsKey(key) || val > tmp.get(key)) return false;
		}
		return true;
	}

}