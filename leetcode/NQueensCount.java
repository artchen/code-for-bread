public class NQueensCount {
    public static int count = 0;
    public static int size =  0;
    public static int[][] board;

    public static void main(String[] args) {
    	int board_size = 8;
    	System.out.print(totalNQueens(board_size));
    }
    
    public static int totalNQueens(int n) {
        if (n == 1) return 1;
        if (n <= 3) return 0;
        size = n;
        board = new int[n][n];
        solve(0);
        return count;
    }
    
    /**
     * Check position safety
     */
	public static boolean checkSafe(int row, int col) {
		int i = 0, j = 0, k = 0;
		// check upper rows
		for (i = row; i >= 0; i--) {
			if (board[i][col] == 1)
				return false;
		}
		// check upper left diagonal
		for (i = row, j = col; i >= 0 && j >= 0; i--, j--) {
			if (board[i][j] == 1)
				return false;
		}
		// check upper right diagonal
		for (i = row, j = col; i >= 0 && j < size; i--, j++) {
			if (board[i][j] == 1)
				return false;
		}
		return true;
	}
    
  public static void solve(int row) {
		// if row == n, this is a solution
		if (row == size) {
			count++;
			return;
		}
		// else keep recurring
		for (int j = 0; j < size; j++) {
			if (checkSafe(row, j)) {
				board[row][j] = 1;
				solve(row+1);
				board[row][j] = 0;
			}
			board[row][j] = 0;
		}
	}
	
	
}