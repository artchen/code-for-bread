import java.util.*;
public class LargestRectangleInHistogram {
	
	public static void main(String[] args) {
		int[][] input = {
			/*
			{1},
			{1,1,1},
			{1,2,3,4},*/
			{2,1,2},
			/*
			{1,2,3,4,5,4,3,2,1},*/
			{1,2,3,4,5,0,4,3,2,1},
			{9,0},
			{4,2,0,3,2,5}
		};
		for (int i = 0; i < input.length; i++)
			System.out.println("Area=" + largestRectangleArea(input[i]) + " // " + Arrays.toString(input[i]));
	}

	public static int largestRectangleArea(int[] height) {
		int i = 0, max = 0, maximal = 0, tmp = 0, width = 0;
		Stack<Integer> stack = new Stack<Integer>();
		while(i < height.length) {
			if ( stack.empty() || height[i] >= height[stack.peek()]) {
				stack.push(i);
				i++;
			}
			else {
				maximal = stack.pop();
				if (stack.empty())
					width = i;
				else 
					width = i-stack.peek()-1;
				tmp = height[maximal] * width;
				max = max > tmp ? max : tmp;
			}
		}
		while (!stack.empty()) {
			maximal = stack.pop();
			if (stack.empty()) 
				width = i;
			else 
				width = i-stack.peek()-1;
			tmp = height[maximal] * width;
			max = max > tmp ? max : tmp;
		}
		return max;
	}

}