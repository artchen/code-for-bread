class ListNode {
	public int val;
	public ListNode next;
	public ListNode(int x) {
		next = null;
		val = x;
	}
}