public class SearchInRotatedSortedArrayII {

	public static void main(String[] args) {
		int[] test = {1,1,3,1,1,1,1};
		int[] test2 = {1,1,1,1,1,3,1};
		int[] test3 = {3,1};
		System.out.println(search(test3,1));
	}

	public static boolean search(int[] nums, int target) {
		if (nums.length == 0) return false;
    if (nums.length == 1) return target == nums[0];
    int pivot = findPivot(nums, 0, nums.length-1);
    if (target >= nums[0] && pivot != 0) 
    	return binarySearch(nums,0,pivot-1,target) != -1;
    return binarySearch(nums,pivot,nums.length-1,target) != -1;
	}

	public static int findPivot(int[] nums, int start, int end) {
    while (start < end) {
      int mid = (start+end)/2;
      if (nums[start] > nums[end]) {
        if (nums[mid] < nums[start]) {
          end = mid;
        }
        else if (nums[mid] > nums[start]) {
        	start = mid+1;
        }
        // if equal, have to do linear search
        else {
        	int i = start+1;
        	while(i < end) {
        		if (nums[i] != nums[start]) break;
        		i++;
        	}
        	start = i;
        }
      }
      else if (nums[start] < nums[end]) {
      	return start;
      }
      // if equal, must do linear search
      else {
				int i = start+1;
      	while(i < end) {
      		if (nums[i] != nums[start]) break;
      		i++;
      	}
      	start = i;
      }
    }
    return start;
  }
    
  public static int binarySearch(int[] nums, int start, int end, int target) {
    while (start <= end) {
			int mid = (start + end) / 2;
			if (target == nums[mid]) return mid;
			else if (target > nums[mid]) start = mid + 1;
			else end = mid - 1;
		}
		return -1;
  }

}