import java.util.Arrays;
public class MergeSort {

    public static void main(String[] args){
        int[] test = {5, 4, 7, 9, 10, 16, 2, 4, 1, 6, 14, 11};
        int[] sortResult = mergeSort(test);
        System.out.println( Arrays.toString(sortResult) );
    }

    /*
     * Do merge sort to an array
     * @param int[] arr the array to be sorted
     * return sorted array
     */
    public static int[] mergeSort(int[] arr) {
        /* empty or single-item array need no sorting */
        if (arr.length <= 1) return arr;

        /* split array into halves */
        int size = arr.length;
        int mid = arr.length/2;
        int[] left = new int[mid];
        int[] right = (mid*2 < size) ? new int[mid+1]: new int[mid];
        int[] result = new int[size];
        
        for (int i = 0; i < mid; i++) {
            left[i] = arr[i];
        }
        for (int j = mid; j < size; j++) {
            right[j-mid] = arr[j];
        }

        /* recursively spliting and merging arrays */
        left = mergeSort(left);
        right = mergeSort(right);
        result = merge(left, right);
        return result;
    }

    /*
     * Util function for mergesort
     * @param int[] l the left half of the array to be merged
     * @param int[] r the right half of the array to be merged
     * return merged array
     */
    public static int[] merge(int[] l, int[] r) {
        int[] answer = new int[l.length + r.length];

        int lin = 0, rin = 0, ain = 0;

        while (lin < l.length && rin < r.length) { 
            if (l[lin] < r[rin]) {
                answer[ain++] = l[lin++];
            }
            else {
                answer[ain++] = r[rin++];
            }
        }
        while (lin < l.length) {
            answer[ain++] = l[lin++];
        }
        while (rin < r.length) {
            answer[ain++] = r[rin++];
        }

        return answer;
    }

}