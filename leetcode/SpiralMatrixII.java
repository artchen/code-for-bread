public class SpiralMatrixII {
    public static void main(String[] args){

        int[][] result = generateMatrix(5);
        for (int i = 0; i<result.length; i++) {
            for (int j = 0; j<result[0].length; j++) {
                System.out.print(result[i][j] + " ");
            }
            System.out.println();
        }

    }

    public static int[][] generateMatrix(int n) {
        if (n == 1) {
            int[][] matrix = new int[1][1];
            matrix[0][0] = 1;
            return matrix;
        }
        int[][] matrix = new int[n][n];
        int max_tries = 0;
        if (n%2!=0) {
            matrix[n/2][n/2] = n*n;
            max_tries = 2*(n-1);
        }
        else {
            max_tries = 2*n;
        }
        int num = 1; // number counter
        int a = n - 1; // segment length
        int nth = 1; // on which side of the suqare
        int tries = 0; // num of segment
        int cyc = 0; // segment positioner

        while (tries < max_tries) {
            if (nth == 1) {
                for (int i = 0; i < a; i++) {
                    matrix[cyc][cyc+i] = num;
                    num = num + 1;
                }
            }
            else if (nth == 2) {
                for (int i = 0; i < a; i++) {
                    matrix[cyc+i][n-1-cyc] = num;
                    num = num + 1;
                }
            }
            else if (nth == 3) {
                for (int i = 0; i < a; i++) {
                    matrix[n-1-cyc][n-1-cyc-i] = num;
                    num = num + 1;
                }
            }
            else if (nth == 4) {
                for (int i = 0; i < a; i++) {
                    matrix[n-1-cyc-i][cyc] = num;
                    num = num + 1;
                }
            }
            tries = tries + 1;
            nth = (nth==4)?1:nth + 1;
            if (nth == 1) {
                a = a-2;
                cyc = cyc + 1;
            }
        }
        return matrix;
    }
}