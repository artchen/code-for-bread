import java.util.*;
/**
 * Definition for binary tree
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class MaxDepthOfBinaryTree {

	public static void main(String[] args){
		TreeNode root = new TreeNode(0);
		root.left = new TreeNode(1);
		root.right = new TreeNode(2);
		root.left.left = new TreeNode(3);
		System.out.println( maxDepth(root) );
	}

    public static int maxDepth(TreeNode root) {
    	int nleft = 0;
        int nright = 0;
        
    	if (root == null) {
    		return 0;
    	}

        if (root.left != null) {
        	nleft += maxDepth(root.left);
        }
        if (root.right != null) {
        	nright += maxDepth(root.right);
        } 

        return (nleft >= nright)? nleft+1:nright+1;

    }

}