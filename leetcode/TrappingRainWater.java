import java.util.*;
public class TrappingRainWater {
	
	public static void main(String[] args) {
		int[] heights = {0,1,0,2,1,0,1,3,2,1,2,1};
		System.out.println(trap2(heights));
	}

	// O(N) time, O(1) space solution
	public static int trap(int[] height) {
		if (height == null || height.length < 2) return 0;
    int left = 0, right = height.length-1;
    int maxLeft = 0, maxRight = 0;
    int result = 0;
    while (left <= right) {
      if (height[left] <= height[right]) {
        if (height[left] >= maxLeft) {
          maxLeft = height[left];
        }
        else {
          result += maxLeft - height[left];
        }
        left++;
      }
      else {
        if (height[right] >= maxRight) {
          maxRight = height[right];
        }
        else {
          result += maxRight - height[right];
        }
        right--;
      }
    }
    return result;
	}

	// my original solution using a stack
	// similar to largest rectangle in histogram
	// super slow orz
	public static int trap2(int[] height) {
		if (height == null || height.length < 2) return 0;
		Stack<Integer> stack = new Stack<Integer>();
		int i = 0, result = 0;
		while (i < height.length) {
			if (stack.empty() || height[i] < height[stack.peek()]) {
				stack.push(i);
				i++;
			}
			else {
				int bottom = stack.pop();
				if (!stack.empty()) {
					int shortSlab = height[stack.peek()] > height[i] ? height[i] : height[stack.peek()];
					int layer = (shortSlab-height[bottom])*(i-stack.peek()-1);
					result += layer;
				}
			}
		}
		return result;
	}

}