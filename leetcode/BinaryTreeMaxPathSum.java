/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
import java.util.*;
public class BinaryTreeMaxPathSum {
    
    public static int sum = Integer.MIN_VALUE;

    public static void main(String[] args) {
        TreeNode root = new TreeNode(2);
        root.left = new TreeNode(-1);
        System.out.println(maxPathSum(root));
    }
    
    public static int maxPathSum(TreeNode root) {
        if (root == null) return 0;
        dfs(root);
        return sum;
    }
    
    public static int dfs(TreeNode node) {
        if (node == null) return 0;
        int left_sum = dfs(node.left);
        int right_sum = dfs(node.right);
        // max path sum to return to parent node
        int return_path_sum = Math.max(Math.max(left_sum+node.val, right_sum+node.val), node.val);
        // max path sum at current node
        int current_path_sum = Math.max(return_path_sum, left_sum+right_sum+node.val);
        // update overall max path sum
        sum = Math.max(sum, current_path_sum);

        return return_path_sum;
    }
}