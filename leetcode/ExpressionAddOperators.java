import java.util.*;
public class ExpressionAddOperators {

	public static List<String> result = new ArrayList<String>();

	public static void main(String[] args) {
		System.out.println(addOperators("1234", 10).toString());
	}

  public static List<String> addOperators(String num, int target) {
  	generate(num,target,new StringBuilder(),0,0);
  	return result;
  }

  // a better solution 234ms
  public static void generate(String num, int target, StringBuilder exp, long value, long prev) {
  	String exp_str = exp.toString();
  	int numLen = num.length();
  	if (value == target && numLen == 0) {
  		result.add(exp_str);
  		return;
  	}
  	for (int i = 1; i <= numLen; i++) {
  		String left = num.substring(0,i);
  		// skip 0 leading numbers except 0 itself
  		if (left.charAt(0) == '0' && left.length() != 1) return;
  		long current = Long.parseLong(left);
  		String right = num.substring(i);
  		// first time, only add number, no operator

  		if (exp.length() == 0) {
  			StringBuilder noop = new StringBuilder();
  			noop.append(current);
  			generate(right, target, noop, current, current);
  		}
  		// other cases, add operator
  		else {
  			StringBuilder mult = new StringBuilder();
  			mult.append(exp_str);
  			mult.append("*");
  			mult.append(current);
  			generate(right, target, mult, (value-prev)+current*prev, current*prev);

  			StringBuilder add = new StringBuilder();
  			add.append(exp_str);
  			add.append("+");
  			add.append(current);
  			generate(right, target, add, value+current, current);
  			
  			StringBuilder minus = new StringBuilder();
  			minus.append(exp_str);
  			minus.append("-");
  			minus.append(current);
  			generate(right, target, minus, value-current, -current);
  		}
  	}
  }

  // abandoned attempt
  public static void generate2(String num, int target, StringBuilder exp, int pos) {
  	if (pos >= num.length()) 
  	{
  		int value = evaluate(exp);
  		if (value == target) result.add(exp.toString());
  		return;
  	}
  	if (pos == 0) 
  	{
  		StringBuilder newexp = new StringBuilder();
  		newexp.append(num.charAt(0));
  		generate2(num,target,newexp,pos+1);
  		newexp.deleteCharAt(pos);
  	}
  	else {
  		//System.out.println("pos: " + num.charAt(pos) + ", exp: " + exp.toString());
  		if (exp.charAt(exp.length()-1) == '*' || 
  				exp.charAt(exp.length()-1) == '-' || 
  				exp.charAt(exp.length()-1) == '+') {
  			exp.append(num.charAt(pos));
	  		generate2(num,target,exp,pos+1);
	  		exp.deleteCharAt(exp.length()-1);
  		}
  		else {
  			if (num.charAt(pos-1) != '0') {
		  		exp.append(num.charAt(pos));
		  		generate2(num,target,exp,pos+1);
		  		exp.deleteCharAt(exp.length()-1);
	  		}
	  		exp.append("*");
	  		exp.append(num.charAt(pos));
	  		generate2(num,target,exp,pos+1);

	  		exp.setCharAt(exp.length()-2, '+');
	  		generate2(num,target,exp,pos+1);

	  		exp.setCharAt(exp.length()-2, '-');
	  		generate2(num,target,exp,pos+1);

	  		exp.deleteCharAt(exp.length()-1);
	  		exp.deleteCharAt(exp.length()-1);
  		}
  	}
  }

  public static int evaluate(StringBuilder exp) {
  	int j = 0;
  	List<String> list = new ArrayList<String>();
  	// build list of operands and operators from string builder
  	for (int i = 0; i < exp.length(); i++) {
  		String str = exp.substring(i,i+1);
  		if (i == exp.length()-1) {
  			list.add(exp.substring(j));
  			break;
  		}
  		if (str.equals("*") || str.equals("+") || str.equals("-")) {
  			list.add(exp.substring(j,i));
  			list.add(str);
  			j = i+1;
  		}
  	}
  	//System.out.println(list.toString());

  	return 0;
  }

}