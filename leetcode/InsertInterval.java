/**
 * Definition for an interval.
 * public class Interval {
 *     int start;
 *     int end;
 *     Interval() { start = 0; end = 0; }
 *     Interval(int s, int e) { start = s; end = e; }
 * }
 */
import java.util.*;
public class InsertInterval {

	public class Interval {
		int start;
		int end;
		Interval() { start = 0; end = 0; }
		Interval(int s, int e) { start = s; end = e; }
	}

	public static void main(String[] args) {

	}

	public List<Interval> insert(List<Interval> intervals, Interval newInterval) {
	    if (intervals.size() == 0) {
            intervals.add(newInterval);
            return intervals;
        }
        List<Interval> result = new ArrayList<Interval>();
        boolean start = false, end = false;
        for (int i = 0; i < intervals.size(); i ++) {
            Interval curr = intervals.get(i);
            // no overlap
            if (curr.end < newInterval.start || curr.start > newInterval.end) {
                if (!end && newInterval.end < curr.start) {
                    result.add(newInterval);
                    end = true;
                }
                result.add(curr);
            }
            else {
                if (!start) {
                    newInterval.start = Math.min(curr.start, newInterval.start);
                    start = true;
                }
                if (!end && newInterval.end <= curr.end) {
                    newInterval.end = Math.max(curr.end, newInterval.end);
                    result.add(newInterval);
                    end = true;
                }
            }
        }
        if (!end) {
            result.add(newInterval);
        }
        return result;
	}

}