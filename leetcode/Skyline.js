var getSkyline = function(buildings) {
  if (buildings.length < 1)
    return buildings;

  function pushUnique(arr, elem, isLast) {
    if (arr.length === 0) {
      arr.push(elem);
      return;
    }
    if (arr[arr.length - 1][0] == elem[0]) {
      if (isLast) {
        arr[arr.length - 1][1] = Math.min(arr[arr.length - 1][1], elem[1]);
      } else {
        arr[arr.length - 1][1] = Math.max(arr[arr.length - 1][1], elem[1]);
      }

      return;
    }
    if (arr[arr.length - 1][1] == elem[1]) {
      return;
    }
    arr.push(elem);
  }

  function divide(start, end) {
    if (start == end) {
      var tmp = [];
      tmp.push([buildings[start][0], buildings[start][2]]);
      tmp.push([buildings[start][1], 0]);
      return tmp;
    }
    var left = divide(start, parseInt((start + end) / 2));
    var right = divide(parseInt((start + end) / 2 + 1), end);
    return merge(left, right);
  }

  function merge(l, r) {
    var i = 0,
      j = 0,
      h1 = 0,
      h2 = 0,
      hmax = 0;
    var result = [];

    while (i < l.length && j < r.length) {
      if (l[i][0] < r[j][0]) {
        h1 = l[i][1]; // the height of the left building
        hmax = Math.max(h1, h2);
        pushUnique(result, [l[i][0], hmax], false);
        i++;
      } else {
        h2 = r[j][1]; // the height of the right building
        hmax = Math.max(h1, h2);
        pushUnique(result, [r[j][0], hmax], false);
        j++;
      }
    }
    while (i < l.length) {
      if (i == l.length - 1)
        pushUnique(result, l[i], true);
      else
        pushUnique(result, l[i], false);
      i++;
    }
    while (j < r.length) {
      if (j == r.length - 1)
        pushUnique(result, r[j], true);
      else
        pushUnique(result, r[j], false);
      j++;
    }
    return result;
  }

  return divide(0, buildings.length - 1);
};