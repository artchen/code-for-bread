import java.util.*;
public class CountOfSmallerNumbersAfterSelf {

	public static class BSTNode {
		public BSTNode left;
		public BSTNode right;
		public int count; // count number of nodes in left subtree
		public int dup; // count number of occurrance of this node's val
		public int val;
		public BSTNode(int val) {
			this.val = val;
			this.count = 0;
			this.dup = 1;
		}
	}
	public static void main(String[] args) {
		int[] test = {5,2,6,1,0,7,1,2,30,5};
		System.out.println(countSmaller(test));
	}

	public static List<Integer> countSmaller(int[] nums) {
		List<Integer> result = new ArrayList<Integer>();
		if (nums == null || nums.length == 0) return result;
		if (nums.length == 1) {
			result.add(0);
			return result;
		}
		BSTNode root = new BSTNode(nums[nums.length-1]);
		result.add(0);
		for (int i = nums.length-2; i >= 0; i--) {
			result.add(insert(root, nums[i]));
		}
		Collections.reverse((ArrayList<Integer>)result);
		return result;
	}

	public static int insert(BSTNode node, int val) {
		if (node == null) return 0;
		
		// larger goes to right subtree
		if (val > node.val) {
			if (node.right == null) {
				node.right = new BSTNode(val);
				return node.count + node.dup;
			}
			else {
				return node.count + node.dup + insert(node.right, val);
			}
		}
		
		// smaller goes to left subtree
		else if (val < node.val) {
			node.count++;
			if (node.left == null) {
				node.left = new BSTNode(val);
				return 0;
			}
			else {
				return insert(node.left, val);
			}
		}
		
		// equal increment dup
		node.dup++;
		return node.count;
	}

}