import java.util.*;
public class BurstBalloons {
	public static void main(String[] args) {
		int[] test = {};
		System.out.println(maxCoins(test));
	}
  public static int maxCoins(int[] nums) {
  	if (nums == null || nums.length == 0) return 0;
    int n = nums.length;

    int[][] dp = new int[n][n];
    
    // for a span of length = span...
    for (int span = 1; span <= n; span++) {
        
      // ...start from index [left]...
      for (int left = 0; left <= n-span; left++) {
          
        int right = left + span - 1;
        
        // ...and end at index [right]
        for (int i = left; i <= right; i++) {
            
          // if we burst this balloon, we got this many coins
          int coins = nums[i] * get_coins(nums,left-1) * get_coins(nums,right+1);
          
          // calculate cumulated coins
          if (i != left) {
            coins += dp[left][i-1];
          }
          
          // calculate cumulated coins
          if (i != right) {
            coins += dp[i+1][right];
          }
          
          // update max coins for the [left,right] span
          dp[left][right] = dp[left][right] > coins ? dp[left][right] : coins;
        }
      }
    }

    return dp[0][n-1];
  }

  public static int get_coins(int[] nums, int i) {
    return (i < 0 || i >= nums.length) ? 1 : nums[i];
  }
}