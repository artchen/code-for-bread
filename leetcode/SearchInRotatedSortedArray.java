public class SearchInRotatedSortedArray {

    public static void main(String[] args) {
        int[] test = {4,5,6,7,0,1,2};
        System.out.println(search(test, 5));
    }

    public static int search(int[] nums, int target) {
        if (nums.length == 0) return -1;
        if (nums.length == 1) return target == nums[0] ? 0 : -1;
        int pivot = findPivot(nums, 0, nums.length-1);
        if (pivot == 0) return binarySearch(nums,0,nums.length-1,target);
        else if (target >= nums[0]) return binarySearch(nums,0,pivot-1,target);
        return binarySearch(nums,pivot,nums.length-1,target);
    }
    
    public static int findPivot(int[] nums, int start, int end) {
        while (start <= end) {
            int mid = (start+end)/2;
            if (nums[start] > nums[end]) {
                if (nums[mid] < nums[start]) {
                    end = mid;
                }
                else {
                    start = mid+1;
                }
            }
            else return start;
        }
        return start;
    }
    
    public static int binarySearch(int[] nums, int start, int end, int target) {
        while (start <= end) {
			int mid = (start + end) / 2;
			if (target == nums[mid]) {
				return mid;
			}
			else if (target > nums[mid]) {
				start = mid + 1;
			}
			else {
				end = mid - 1;
			}
		}
		return -1;
    }
}