public class RomantoInteger {
    public static void main(String[] args) {
        String test = "MMXIV";
        System.out.println( romanToInt(test) );
    }
    public static int romanToInt(String s) {
        String current = null;
        String prev = null;
        int current_int = 0;
        int prev_int = 0;
        int result = 0;
        
        for (int i=0; i<s.length(); i++) {
            prev = current;
            current = s.substring(i, i+1);
            current_int = lettertoInteger(current);
            prev_int = lettertoInteger(prev);

            if (prev_int < current_int) {
                result += current_int - 2*prev_int;
            }
            else {
                result += current_int;
            }
            
            
        }

        return result;
        
    }

    public static int lettertoInteger(String s) {
        int result = 0;
        if (s == null) return result;
        if ( s.equals("I") ) {
            result = 1;
        }
        else if ( s.equals("V") ) {
            result = 5;
        }
        else if ( s.equals("X") ) {
            result = 10;
        }
        else if ( s.equals("L") ) {
            result = 50;
        }
        else if ( s.equals("C") ) {
            result = 100;
        }
        else if ( s.equals("D") ) {
            result = 500;
        }
        else if ( s.equals("M") ) {
            result = 1000;
        }
        return result;
    }
}