/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class InvertBinaryTree {

	public static void main(String[] args) {
		TreeNode root = new TreeNode(4);
		root.left = new TreeNode(2);
		root.right = new TreeNode(7);
		root.left.left = new TreeNode(1);
		root.right.left = new TreeNode(6);
		invertTree(root);
		System.out.println(root.left.right.val);
	}

  public static TreeNode invertTree(TreeNode root) {
  	if (root == null) return null;
  	TreeNode left = invertTree(root.left);
  	TreeNode right = invertTree(root.right);
  	root.left = right;
  	root.right = left;
  	return root;
  }

}