public class LongestValidParentheses {

	public static void main(String[] args) {
		System.out.println(longestValidParentheses(")(()()))()()"));
		System.out.println(longestValidParentheses("(((()()())))"));
	}

	public static int longestValidParentheses(String s) {
		int n = s.length(), max = 0;
		int[] dp = new int[n+1]; // previous continuous valid parentheses length
		for (int i = 1; i < n+1; i++) {
			// s[i-1] is current char, s[i-2] is prev char
			// dp[i] is current dp, dp[i-1] is prev dp
			int tmp = (i-2) - dp[i-1];
			// current char is (
			if (s.charAt(i-1) == '(') {
				dp[i] = 0;
			}
			// current char is )
			else {
				// first char 
				if (tmp < 0) dp[i] = 0;
				// former wrapper is )
				else if (s.charAt(tmp) == ')') dp[i] = 0;
				// good wrappers
				else {
					dp[i] = dp[i-2] > dp[i-1] ? 2+dp[i-2] : 2+dp[i-1];
					max = max > dp[i] ? max : dp[i];
				}
			}
			
		}
		return max;
	}

}