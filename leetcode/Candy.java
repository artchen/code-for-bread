public class Candy {

	public static void main(String[] args) {
		int[] ratings = {5,3,1};
		System.out.println(candy(ratings));
	}

	public static int candy(int[] ratings) {
		if (ratings.length < 2) return ratings.length;
		int[] candies = new int[ratings.length];
		candies[0] = 1;
		int sum = 1;
		for (int i = 1; i < ratings.length; i++) {
			if (ratings[i] > ratings[i-1])
				candies[i] = candies[i-1]+1;
			else
				candies[i] = 1;
			sum += candies[i];
		}
		for (int i = ratings.length-2; i >= 0; i--) {
			if (ratings[i] > ratings[i+1] && candies[i] <= candies[i+1]) {
				sum += candies[i+1] - candies[i] + 1;
				candies[i] = candies[i+1] + 1;
			}
		}
		return sum;
	}

}