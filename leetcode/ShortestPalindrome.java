import java.util.*;
public class ShortestPalindrome {

	public static void main(String[] args) {
		List<String> results = new ArrayList<String>();
		results.add(shortestPalindrome("abc"));
		results.add(shortestPalindrome("aabc"));
		results.add(shortestPalindrome("aaabccc"));
		results.add(shortestPalindrome("aaabaaa"));
		results.add(shortestPalindrome("aaabaaa"));
		results.add(shortestPalindrome("aaadcaaacdaaa"));
		for (int i = 0; i < results.size(); i++)
			System.out.println(results.get(i));
	}

	// wonderful recursive 4ms solution
	public static String shortestPalindrome(String s) {
		int i = 0, j = s.length()-1;
		for (; j >= 0; j --) {
			if (s.charAt(j) == s.charAt(i)) i++;
		}
		if (i == s.length()) return s;
		String addon = s.substring(i);
		return (new StringBuilder(addon)).reverse().toString() + shortestPalindrome(s.substring(0,i)) + addon;
	}

	// my original slow solution
	public static String shortestPalindrome2(String s) {
    int i = 0, j = 0, pivot = 0, n = s.length();
    char[] c = s.toCharArray();
    boolean odd = (n & 1) > 0;
    if (n == 0) return "";
    if (n == 1) return s;
    pivot = n/2;
    i = pivot - 1;
    j = odd ? pivot + 1 : pivot;
    while (pivot > 0) {
    	while (i >= 0 && j < s.length() && c[i] == c[j]) {
  			i--;
  			j++;
  		}
  		if (i < 0) break; // found the right pivot
  		odd = !odd;
    	if (odd) pivot--;
	    i = pivot - 1;
	    j = odd ? pivot + 1 : pivot;
    }
    if (j == 0 && c[0] == c[1]) j = j+2;
    else if (j == 0 && c[0] != c[1]) j++;
    StringBuilder sb = new StringBuilder();
    int k = n-1;
    while (k >= j) {
    	sb.append(c[k]);
    	k--;
    }
    return sb.append(s).toString();
	}
}