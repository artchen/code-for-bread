/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) {
 *         val = x;
 *         next = null;
 *     }
 * }
 */
public class RemoveDuplicatesFromSortedList  {
    public ListNode deleteDuplicates(ListNode head) {
        ListNode itr = head;

        while (itr != null) {
            if (itr.next != null) {
                if (itr.val == itr.next.val) {
                    itr.next = itr.next.next;
                }
                else {
                  itr = itr.next;  
                }
            }
            else {
                return head;
            }
        }
        return head;
    }
}