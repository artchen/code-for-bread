/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
import java.util.*;
public class RecoverBST {
	
	public static void main(String[] args) {
		TreeNode root = new TreeNode(8);
		root.left = new TreeNode(0);
		
		root.right = new TreeNode(10);
		root.right.left = new TreeNode(7);
		root.right.right = new TreeNode(15);
		root.right.left.left = new TreeNode(6);
		root.right.left.right = new TreeNode(5);
		
		recoverTree(root);
	}

	// Morris Traversal, Real O(1) space solution
	public static void recoverTreeMorris(TreeNode root) {
		if (root == null) return;

		/* In-order Morris Traversal */
		List<TreeNode> result = new ArrayList<TreeNode>();
		TreeNode swap = null;
		TreeNode curr = root, prev = null, checkpt = null;
		while (curr != null) {
			// there is no left subtree
			if (curr.left == null) {
				// check checkpt
				if (checkpt != null && checkpt.val > curr.val) {
					if (result.size() < 1) {
						result.add(checkpt);
						swap = curr;
					}
					else
						result.add(curr);
				}
				checkpt = curr;
				curr = curr.right;
			}
			// there is left subtree
			else {
				prev = curr.left;
				// find the last node of in-order traversal in left subtree
				while (prev.right != null && prev.right != curr) {
					prev = prev.right;
				}
				// case 1: when we traverse down the left subtree
				if (prev.right == null) {
					prev.right = curr;
					curr = curr.left;
				}
				// case 2: when we finished the left subtree, back to the "root" (subtrees' root)
				else {
					prev.right = null;
					// check checkpt
					if (checkpt != null && checkpt.val > curr.val) {
						if (result.size() < 1) {
							result.add(checkpt);
							swap = curr;
						}
						else
							result.add(curr);
					}
					checkpt = curr;
					curr = curr.right;
				}
			}
		}
		if (result.size() == 2) {
			TreeNode node1 = result.get(0);
			TreeNode node2 = result.get(1);
			int val1 = node1.val;
			node1.val = node2.val;
			node2.val = val1;
		}
		else if (result.size() == 1 && swap != null) {
			TreeNode node1 = result.get(0);
			int val1 = node1.val;
			node1.val = swap.val;
			swap.val = val1;
		}
		
	}

	// In-order traversal using a stack
	public static void recoverTree(TreeNode root) {
		if (root == null) return;

		Stack<TreeNode> stack = new Stack<TreeNode>();
		stack.push(root);
		TreeNode curr = root, f1 = null, f2 = null, checkpt = null;
		boolean found = false;
		// standard in-order traversal using a stack
		while (curr != null) {
			stack.push(curr);
			curr = curr.left;
		}	
		while (!stack.empty()) {
			curr = stack.pop();
			if (checkpt != null && checkpt.val > curr.val) {
				if (!found) {
					f1 = checkpt;
				}
				else f2 = curr;
			}
			checkpt = curr;
			// if has right branch, traverse right subtree
			if (curr.right != null) {
				curr = curr.right;
				while (curr != null) {
					stack.push(curr);
					curr = curr.left;
				}
			}
		}
		if (f1 != null) System.out.println(f1.val);
		if (f2 != null) System.out.println(f2.val);
		// do swap
		if (f1 != null && f2 != null) {
			int val1 = f1.val;
			f1.val = f2.val;
			f2.val = val1;
		}
		else {

		}
	}

}