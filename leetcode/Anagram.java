import java.util.*;
public class Anagram {

	public static void main(String[] args) {
		String s = "anagram", t = "nagaram";
		System.out.println(isAnagram(s,t));
	}

	/**
	 * assume only lowercase English alphabets, a faster solution O(N)
	 */
	public static boolean isAnagram(String s, String t) {
		if (s.length() != t.length()) return false;
		int[] alpha = new int[26];
		for (int i = 0; i < s.length(); i++) {
			alpha[(int)(s.charAt(i)-'a')]++;
			alpha[(int)(t.charAt(i)-'a')]--;
		}
		for (int i = 0; i < alpha.length; i++) {
			if (alpha[i] != 0) return false;
		}
		return true;
	}

	/**
	 * slow but universal hashmap solution, works for any unicode value
	 */
	public static boolean isAnagram2(String s, String t) {
		if (s.length() != t.length()) return false;
		HashMap<Character,Integer> ht = new HashMap<Character,Integer>();
		for (int i = 0; i < s.length(); i++) {
			char sc = s.charAt(i);
			char tc = t.charAt(i);
			if ( !ht.containsKey(sc) )
				ht.put(sc,1);
			else
				ht.put(sc, ht.get(sc)+1);
			if ( !ht.containsKey(tc) ) 
				ht.put(tc,-1);
			else
				ht.put(tc, ht.get(tc)-1);
		}
		for (Map.Entry<Character,Integer> entry : ht.entrySet()) {
			Integer val = entry.getValue();
			if (val != 0) return false;
		}
		return true;
	}

}