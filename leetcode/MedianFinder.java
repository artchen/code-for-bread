import java.util.*;
public class MedianFinder {

	private PriorityQueue<Integer> minHeap = new PriorityQueue<Integer>(20, Collections.reverseOrder());
	private PriorityQueue<Integer> maxHeap = new PriorityQueue<Integer>();
	int bound = Integer.MAX_VALUE;

	public MedianFinder() {

	}

	// Adds a number into the data structure.
	public void addNum(int num) {
		if (num > bound)
		    maxHeap.add(num);
		else
		    minHeap.add(num);
		if (minHeap.size() - maxHeap.size() > 1)
		    maxHeap.add(minHeap.poll());
		else if (minHeap.size() - maxHeap.size() < -1)
		    minHeap.add(maxHeap.poll());
		bound = maxHeap.size() == 0 ? bound : maxHeap.peek();
	}

	// Returns the median of current data stream
	public double findMedian() {
		if (minHeap.size() == maxHeap.size()) return ((double)minHeap.peek() + (double)maxHeap.peek()) / 2;
		else if (minHeap.size() > maxHeap.size()) return (double)minHeap.peek();
		return (double)maxHeap.peek();
	}

};
