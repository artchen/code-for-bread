
import java.util.*;

public class LongestSubstringWithoutRepeatingCharacters {
	
	public static void main (String[] args) {
		System.out.println( lengthOfLongestSubstring("au") );
		System.out.println( lengthOfLongestSubstring("abba") );
		System.out.println( lengthOfLongestSubstring("tmmzuxt") );
	}

	// better solution: one iteration
	public static int lengthOfLongestSubstring(String s) {
		if (s == null || s.length() < 1) return 0;
    if (s.length() == 1) return 1;

    int[] map = new int[256];
    for (int i = 0; i < map.length; i++) {
    	map[i] = -1;
    }
    int len = 1, start = 0;
    for (int i = 0; i < s.length(); i++) {
    	char c = s.charAt(i);
    	// only update start if there a duplicate is in (start,i)
    	if (map[c] >= start) {
				start = map[c]+1; 
    	}
    	map[c] = i;
    	len = len > i-start+1 ? len : i-start+1;
    }
    return len;
	}

	// my solution: O(N) time, O(256) space, two iterations in total
	public static int lengthOfLongestSubstring2(String s) {
		if (s == null || s.length() < 1) return 0;
    if (s.length() == 1) return 1;
    
    boolean[] map = new boolean[256];
    int max = 1, i = 0, j = 1;
    map[s.charAt(0)] = true;
    while (i < s.length() && j < s.length()) {
      char c = s.charAt(j);
      if (map[c]) {
        while (s.charAt(i) != c) {
          map[s.charAt(i)] = false;
          i++;
        }
        i++;
      }
      else {
        map[c] = true;
      }
      j++;
      max = max > j-i ? max : j-i;
    }
    return max;
	}


}