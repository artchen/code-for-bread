/**
 * Definition for binary tree
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class SameTree {
    public boolean isSameTree(TreeNode p, TreeNode q) {
        boolean isLeftSame = false;
        boolean isRightSame = false;
        /* Both null counts as equal */
        if (p == null && q == null) return true;
        /* Only one null counts as unequal */
        if ( (p != null && q == null) || (p == null && q != null) ) return false;
        /* Both are not null, compare value and continue traversing */
        if (p.val == q.val) {
            if (p.left != null && q.left != null) {
                isLeftSame = isSameTree(p.left, q.left);
            }
            else if (p.left == null && q.left == null) {
                isLeftSame = true;
            }
            else {
                isLeftSame = false;
            }
            if (p.right != null && q.right != null) {
                isRightSame = isSameTree(p.right, q.right);
            }
            else if (p.right == null && q.right == null) {
                isRightSame = true;
            }
            else {
                isRightSame = false;
            }
        }
        return (isLeftSame && isRightSame);
    }
}