import java.util.*;
public class SlidingWindowMaximum {

	public static void main(String[] args) {
		int[] nums = {1,3,-1,-3,4,3,6,7};
		System.out.println(Arrays.toString(maxSlidingWindow(nums,3)));
	}

  public static int[] maxSlidingWindow(int[] nums, int k) {
  	int n = nums.length;
  	if (n == 0) return new int[0];
    int[] result = new int[n-k+1];
    LinkedList<Integer> q = new LinkedList<Integer>();
    int i = 0;
    for (; i < k; i++) {
      while (q.size() != 0 && nums[i] > nums[q.peekLast()]) {
        q.pollLast();
      }
      q.addLast(i);
    }
    result[0] = nums[q.peekFirst()];
    for (; i < n; i++) {
      int wi = i-k+1;
      // head is out
      if (q.peekFirst() < wi) q.pollFirst();
      while (q.size() != 0 && nums[i] > nums[q.peekLast()]) {
        q.pollLast();
      }
      q.addLast(i);
      result[wi] = nums[q.peekFirst()];
    }
    return result;
  }

}