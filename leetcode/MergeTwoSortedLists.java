/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) {
 *         val = x;
 *         next = null;
 *     }
 * }
 */
public class MergeTwoSortedLists {
    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        if (l1 == null && l2 == null) return null;
        if (l1 == null && l2 != null) return l2;
        if (l1 != null && l2 == null) return l1;
        ListNode head = null;
        
        if (l1.val <= l2.val) {
            head = l1;
            l1 = l1.next;
        }
        else {
            head = l2;
            l2 = l2.next;
        }
        
        ListNode itr = head;

        while (l1 != null) {
            if (l2 != null) {
                if (l1.val <= l2.val) {
                    itr.next = l1;
                    itr = itr.next;
                    l1 = l1.next;
                }
                else {
                    itr.next = l2;
                    itr = itr.next;
                    l2 = l2.next;
                }
            }
        }
        
        while(l1 != null) {
            itr.next = l1;
            itr = itr.next;
            l1 = l1.next;
        }
        
        while(l2 != null) {
            itr.next = l2;
            itr = itr.next;
            l2 = l2.next;
        }
        
        return head;
    }
}