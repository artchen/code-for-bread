import java.util.*;
public class WordLadder {

	public static class Node {
		public String word;
		public int count;
		public Node(String w, int c) {
			this.word = w;
			this.count = c;
		}
	}
	
	public static void main(String[] args) {
		HashSet<String> wordList = new HashSet<String>();
		String[] list = {"si","go","se","cm","so","ph","mt","db","mb","sb","kr","ln","tm","le","av","sm","ar","ci","ca","br","ti","ba","to","ra","fa","yo","ow","sn","ya","cr","po","fe","ho","ma","re","or","rn","au","ur","rh","sr","tc","lt","lo","as","fr","nb","yb","if","pb","ge","th","pm","rb","sh","co","ga","li","ha","hz","no","bi","di","hi","qa","pi","os","uh","wm","an","me","mo","na","la","st","er","sc","ne","mn","mi","am","ex","pt","io","be","fm","ta","tb","ni","mr","pa","he","lr","sq","ye"};

		for (String s : list) {
			wordList.add(s);
		}
		System.out.println(ladderLength("qa", "qa", wordList));
	}


	public static int ladderLength(String beginWord, String endWord, Set<String> wordList) {
		wordList.add(endWord);
		ArrayDeque<Node> q = new ArrayDeque<Node>();
		q.add(new Node(beginWord, 1));
		//int min = Integer.MAX_VALUE;
		while (q.size() != 0) {
			String s = q.peek().word;
			int count = q.peek().count;
			// if found, record current count
			if (s.equals(endWord)) {
				//min = min > count ? count : min;
				return count;
			}
			q.pop();
			char[] ca = s.toCharArray();
			for (int i = 0; i < ca.length; i++) {
				char tmp = ca[i];
				for (int j = 0; j < 26; j++) {
					char cur = (char)('a'+j);
					if ( cur == ca[i]) continue;
					ca[i] = cur;
					String ss = new String(ca);
					if (wordList.contains(ss)) {
						q.add(new Node(ss,count+1));
						wordList.remove(ss);
					}
				}
				ca[i] = tmp;
			}
		}
  	return 0;//min == Integer.MAX_VALUE ? 0 : min;
  }
}