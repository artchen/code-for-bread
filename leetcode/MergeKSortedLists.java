/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
public class MergeKSortedLists {
	
	public static void main(String[] args) {

	}

	public static ListNode mergeKLists(ListNode[] lists) {
		if (lists.length == 0) return null;
    return split(lists,0,lists.length-1);
	}

	public static ListNode split(ListNode[] lists, int start, int end) {
		if (start == end) return lists[start];
    ListNode left = split(lists,start,(end+start)/2);
    ListNode right = split(lists,(end+start)/2+1,end);
    return merge(left,right);
	}

	public static ListNode merge(ListNode r1, ListNode r2) {
		ListNode root = new ListNode(0);
    ListNode itr = root;
    while (r1 != null && r2 != null) {
      if (r1.val < r2.val) {
        itr.next = r1;
        r1 = r1.next;
      }
      else {
        itr.next = r2;
        r2 = r2.next;
      }
      itr = itr.next;
    }
    while (r1 != null) {
      itr.next = r1;
      r1 = r1.next;
      itr = itr.next;
    }
    while (r2 != null) {
      itr.next = r2;
      r2 = r2.next;
      itr = itr.next;
    }
    return root.next;
	}

}