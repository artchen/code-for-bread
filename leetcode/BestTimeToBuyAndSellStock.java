public class BestTimeToBuyAndSellStock {
    public static void main(String[] args){
        int[] test_array = {4, 5, 2, 8, 3, 10, 1};
        System.out.println(maxProfit(test_array));
    }
    public static int maxProfit(int[] prices) {
        int buy = -1;
        int sell = -1;
        int max = 0;
        for (int i = 0; i < prices.length; i++) {
            if (i == 0) {
                buy = i;
            }
            else {
                if (prices[i] > prices[buy] && prices[i] - prices[buy] > max) {
                    sell = i;
                    max = prices[i] - prices[buy];
                }
                if (prices[i] < prices[buy]) {
                    buy = i;
                }
            }
        }
        return max;
    }
}