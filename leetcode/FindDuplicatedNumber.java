public class FindDuplicatedNumber {
	public static void main(String[] args) {
		int[] test = {5,9,7,1,3,5,8,2,4};
		System.out.println(findDuplicate(test));
	}

	// O(n) two pointer solution
	public static int findDuplicate(int[] nums) {
		return 0;
	}

	// O(nlogn) binary search solution
	public static int findDuplicate2(int[] nums) {
		int low = 1, high = nums.length-1;
		int mid;
		while (low < high) {
			mid = (low+high)/2;
			int count = 0;
			for (int num : nums) {
				if (num <= mid) count++;
			}
			if (count > mid) high = mid;
			else low = mid + 1;
		}
		return low;
	}

}