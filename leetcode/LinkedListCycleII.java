/**
 * Definition for singly-linked list.
 * class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) {
 *         val = x;
 *         next = null;
 *     }
 * }
 */
public class LinkedListCycleII {
    public static void main(String[] args) {
        ListNode head = new ListNode(1);
        ListNode itr = head;
        ListNode start_cycle = head;
        for (int i = 1; i <= 10; i++) {
            itr.next = new ListNode(i);
            itr = itr.next;
            if (i == 4) {
                start_cycle = itr;
            }
        }
        itr.next = start_cycle;
        System.out.println(detectCycleBetter(head).val);
    }

    /* better solution */

    public static ListNode detectCycleBetter(ListNode head) {
        if (head == null) return null;
        ListNode p1 = head;
        ListNode p2 = head;
        boolean found = false;
        
        while (p1 != null && p2 != null) {
            p1 = p1.next;
            if (p2.next == null) return null;
            p2 = p2.next.next;
            if (p2 == p1) {
                found = true;
                break;
            }
        }
        
        if (found) {
            p1 = head;
            while(p1 != p2) {
                p1 = p1.next;
                p2 = p2.next;
            }
            return p1;
        }
        
        return null;
    }

    /* my solution */

    public static ListNode detectCycle(ListNode head) {
        if (head == null) return null;
        ListNode p1 = head;
        ListNode p2 = head;
        ListNode p3 = head;
        ListNode flag = null;
        int loop_length = 0;
        int diff = 0;
        boolean init = true;
        
        while (p1 != null && p2 != null &&  p3 != null) {
            if (flag == null) {
                p2 = (p3 != null) ? p3.next : null;
                p3 = (p2 != null) ? p2.next : null;
                /* meet in the loop */
                if (p1 == p2 || p1 == p3) {
                    flag = p1;
                }
            }
            else {
                loop_length++;
                if (p1 == flag) {
                    break;
                }
            }
            p1 = p1.next;
        }
        
        /* if found loop */
        if (flag != null) {
            p1 = flag;
            p2 = head;

            /* walking towards each other */
            while ( (p1 != flag && p2 != flag) || init) {
                init = false;
                if (p1 == p2) {
                    return p1;
                }
                p1 = p1.next;
                p2 = p2.next;
            }

            /* p1 hits flag first */
            if (p1 == flag) {
                while (p2 != flag) {
                    p2 = p2.next;
                    diff++;
                }
                p1 = flag;
                p2 = head;
                for (int i = 0; i < diff; i++) {
                    p2 = p2.next;
                }
                while (p1 != p2) {
                    p2 = p2.next;
                    p1 = p1.next;
                }
                return p1;
            }
            /* p2 hits flag first */
            else if (p2 == flag) {
                while (p1 != flag) {
                    p1 = p1.next;
                    diff++;
                }
                p1 = flag;
                p2 = head;
                for (int i = 0; i < diff; i++) {
                    p1 = p1.next;
                }
                while (p1 != p2) {
                    p2 = p2.next;
                    p1 = p1.next;
                }
                return p1;
            }
        }

        return null; // found no loop
    }
}