public class ValidNumber {

	public static void main(String[] args) {
		String test = "1.e1";
		System.out.println("[" + test + "]: " + isNumber(test));
	}

	public static boolean isNumber(String s) {
		String t = s.trim();
    if (t.length() == 0) return false;
    int n = t.length();
    boolean e = false; // e presented
    boolean d = false; // decimal dot presented
    boolean valid_dot = false; // dot has previous char
    int minus = 0; // # of -
    int plus = 0; // # of +
    char c = ' ';
    char p = ' ';
    for (int i = 0; i < t.length(); i++) {
        c = t.charAt(i);
        if (!validChar(c)) return false;
        if (c == '.') {
            if (e||d||n==1||(i==n-1&&(p=='+'||p=='-')) ) return false;
            if (i != 0) valid_dot = true;
            d = true;
        }
        if (c == 'e') {
            if (e||i==0||i==n-1||p=='-') return false;
            if (p=='.'&&!valid_dot) return false;
            e = true;
        }
        if (c == '-') {
            if ((i!=0&&p!='e')||minus>1||i==n-1) return false;
            minus++;
        }
        if (c == '+') {
            if ((i!=0&&p!='e')||plus>1||i==n-1) return false;
            plus++;
        }
        p = c;
    }
    return true;
	}

 	public static boolean validChar(char c) {
    if (c != '-' && c != 'e' && c != '.' && (c-'0' < 0 || c-'9' > 0) ) return false;
    return true;
  }


}