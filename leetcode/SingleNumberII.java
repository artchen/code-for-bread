public class SingleNumberII {
    public static void main(String[] args){
        int[] test = {-19,-46,-19,-46,-9,-9,-19,17,17,17,-13,-13,-9,-13,-46,-28};
        //singleNumberII(test);
        System.out.println( singleNumberII(test) );
    }

    public static int singleNumberII(int[] A) {
        // Initialize result
        int result = 0;
     
        int x, sum;
     
        // Iterate through every bit
        for (int i = 0; i < 32; i++)
        {
          // Find sum of set bits at ith position in all
          // array elements
          sum = 0;
          x = (int)(1 << i);
          for (int j=0; j< A.length; j++ )
          {
              if ( ( A[j] & x ) != 0)
                sum++;
          }
     
          // The bits with sum not multiple of 3, are the
          // bits of element with single occurrence.
          if (sum % 3 != 0)
            result |= x;
        }
     
        return result;
    }

}