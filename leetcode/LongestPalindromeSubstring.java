public class LongestPalindromeSubstring {

	public static int len = 0;
	public static int start = 0;
	
	public static void main(String[] args) {
		System.out.println( longestPalindrome("abcdcbef") );
		System.out.println( longestPalindrome("fffffffffffffffffffffffffffffffffffffffffgggggggggggggggggggggggg") );
	}

	public static String longestPalindrome(String s) {
		if (s == null || s.length() == 1) return s;
		for (int i = 0; i < s.length(); i++) {
			findPalindrome(s,i,i); // extend from symmetrical axis to find palindrome
			findPalindrome(s,i,i+1);
		}
		return s.substring(start,start+len);
	}

	public static void findPalindrome(String s, int i, int j) {
		while (i >= 0 && j <= s.length()-1 && s.charAt(i)==s.charAt(j)) {
			i--;
			j++;
		}
		i++;
		j--;
		if (len < j-i+1) {
			len = j-i+1;
			start = i;
		}
	}

}