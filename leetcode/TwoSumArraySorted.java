/*
	Given a **sorted** array and a target sum, return the indexes of two elements 
	from the array that has sum equals target.
*/
	import java.util.*;
public class TwoSumArraySorted {
	
	public static void main (String[] args) {
		int[] test = {2,3,5,8,11,40};
		System.out.println(Arrays.toString(find(10, test)) );
	}

	// O(N) time, O(1) space, two pointer
	public static int[] find (int target, int[] nums) {
		int i = 0, j = nums.length - 1;
		while (i < j) {
			int sum = nums[i] + nums[j];
			if (sum < target) {
				i++;
			}
			else if (sum > target) {
				j--;
			}
			else {
				return new int[] {i,j};
			}
		}
		return null; // not found
	}

}