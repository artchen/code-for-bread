public class SearchInsertPosition {

    public static void main(String[] args) {
        int[] test = {1,2,4,5,6,8,9,11,14,16,19,21};
        System.out.println( searchInsert(test, 6)+ "/" + (test.length+1) );
        System.out.println( searchInsert(test, 0) + "/" + (test.length+1) );
        System.out.println( searchInsert(test, 22) + "/" + (test.length+1)  );
    }

    public static int searchInsert(int[] A, int target) {
        int pos = 0;
        for (int i=0; i< A.length; i++) {
            if (target <= A[i]) {
                pos = i;
                return pos;
            }
            else {
                pos++;
            }
        }
        return pos;
    }
}