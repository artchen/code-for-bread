public class ShortestPath {
	public static void main(String[] args){
		int[][] grid = {
							{1,2},
							{1,1}
						}; 
		System.out.println( minPathSum(grid) );

	}
    public static int minPathSum(int[][] grid) {
        if (grid == null) return 0;
        int[][] dist = new int[grid.length][grid[0].length];
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                dist[i][j] = -1;
            }
        }
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                if (i != 0 && j != 0) {
                    dist[i][j] = ( ( dist[i-1][j] <= dist[i][j-1] ) ? dist[i-1][j] : dist[i][j-1] ) + grid[i][j];
                }
                else if (i != 0 && j == 0) {
                    dist[i][j] = grid[i][j] + dist[i-1][j];
                }
                else if (i == 0 && j != 0) {
                    dist[i][j] = grid[i][j] + dist[i][j-1];
                }
                else {
                    dist[i][j] = grid[i][j];
                }
            }
        }
		for (int i = 0; i < dist.length; i++) {
            for (int j = 0; j < dist[0].length; j++) {
                System.out.print(dist[i][j]);
            }
            System.out.println();
        }

        return dist[grid.length-1][grid[0].length-1];
    }
}