/*
 * Reverse digits of an integer.
 * Example1: x = 123, return 321
 * Example2: x = -123, return -321
 */
public class ReverseInteger {
    public int reverse(int x) {
        String str = Integer.toString(x);
        String rev = "";
        int result = 0;
        if (x < 0) {
            rev = rev + str.charAt(0);
            for ( int i = str.length() - 1 ; i > 0 ; i-- ) {
                rev = rev + str.charAt(i);
            }
            result = Integer.parseInt(rev);
        }
        else {
            for ( int i = str.length() - 1 ; i >= 0 ; i-- ) {
                rev = rev + str.charAt(i);
            }
            result = Integer.parseInt(rev);
        }
        return result;
    }
}