import java.util.Arrays;
public class RemoveElement {
	public static void main(String[] args) {
		int[] test = {1,4,5,6,7,8,9,0,35,3,2,4,2,42,3,4,4,2,3,1};
		System.out.println( test.length );

		System.out.println( removeElement(test, 0) );
	}
    public static int removeElement(int[] A, int elem) {
        
        int count = 0;

        for (int i=0; i<A.length; i++) {
        	if (A[i] == elem) {
        		++count;
        	}
        	else if (count > 0){
        	    A[i - count] = A[i];
        	}
        }

        return A.length - count;

    }
}