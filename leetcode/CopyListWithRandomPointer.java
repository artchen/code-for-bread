import java.util.*;
public class CopyListWithRandomPointer {
    
    public static class RandomListNode {
        public int label;
        public RandomListNode next, random;
        public RandomListNode(int x) { this.label = x; }
    }

    public static void main(String[] args) {
        RandomListNode head = new RandomListNode(-1);
        head.next = new RandomListNode(-2);
        head.random = head.next;
        head.next.random = head;
        RandomListNode itr0 = head;
        RandomListNode itr = copyRandomList(head);
        while (itr != null && itr0 != null) {
            System.out.print("new " + itr.label + " original " + itr0.label + " ");
            if (itr.random == null) System.out.print("new random " + itr.random + " ");
            else System.out.print("new random " + itr.random.label + " " + "[" + itr.random + "] ");
            if (itr0.random == null) System.out.println("original random " + itr0.random);
            else System.out.println("original random " + itr0.random.label + "[" + itr0.random + "]");
            itr = itr.next;
            itr0 = itr0.next;
        }
    }

    // O(3N) time, O(1) space
    public static RandomListNode copyRandomList(RandomListNode head) {
        if (head == null) return head;
        if (head.next == null) {
            RandomListNode newHead = new RandomListNode(head.label);
            newHead.random = head.random;
            return newHead;
        }
         RandomListNode itr = head;
         // 1st iteration: construct zig-zag linked-list
         while (itr != null) {
             RandomListNode next = itr.next;
             itr.next = new RandomListNode(itr.label);
             itr.next.next = next;
             itr = itr.next.next;
         }
         // 2nd iteration: copy random pointer
         itr = head;
         while (itr != null && itr.next != null) {
             itr.next.random = (itr.random == null) ? null : itr.random.next;
             itr = itr.next.next;
         }
         // 3rd iteration: recover 2 linked-list from the zig-zag linked-list
         itr = head;
         RandomListNode newHead = head.next;
         while (itr != null && itr.next != null) {
             RandomListNode newNode = itr.next;
             itr.next = newNode.next;
             newNode.next = newNode.next == null ? null : newNode.next.next;
             itr = itr.next;
         }
         return newHead;
    }
}