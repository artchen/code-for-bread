public class EditDistance {

	public static void main(String[] args) {
		System.out.println(minDistance("dist", "vocation"));
	}

  public static int minDistance(String word1, String word2) {
  	int n = word1.length();
  	int m = word2.length();

  	int[][] result = new int[n+1][m+1];

  	for (int i = 0; i <= n; i ++) {
  		result[i][0] = i;
  	}
  	for (int j = 0; j <= m; j ++) {
  		result[0][j] = j;
  	}

  	for (int i = 0; i < n; i++) {
  		char c1 = word1.charAt(i);
  		for (int j = 0; j < m; j++) {
  			char c2 = word2.charAt(j);
  			if (c1 == c2) {
  				result[i+1][j+1] = result[i][j];
  			}
  			else {
  				int rep = result[i][j] + 1;
  				int ins = result[i][j+1] + 1;
  				int del = result[i+1][j] + 1;
  				int min = Math.min(Math.min(rep,ins),del);
  				result[i+1][j+1] = min;
  			}
  		}
  	}

  	return result[n][m];
  }
  
}