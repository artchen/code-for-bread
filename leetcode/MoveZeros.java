/**
 * Given an array nums, write a function to move all 0's to the end of it while 
 * maintaining the relative order of the non-zero elements.
 * For example, given nums = [0, 1, 0, 3, 12], 
 * after calling your function, nums should be [1, 3, 12, 0, 0].
 * 
 * 1. You must do this in-place without making a copy of the array.
 * 2. Minimize the total number of operations.
 *
 */
import java.util.*;
public class MoveZeros {

	public static void main(String[] args) {
		int[] test = {0,1,0,3,12};
		moveZeroes2(test);
		System.out.println(Arrays.toString(test));
	}
  
  /**
   * My original solution
   */
  public static void moveZeroes(int[] nums) {
     int z = -1;
     int i = 0;
     while (i < nums.length) {
     	if (nums[i] == 0 && z == -1) {
     		z = i;
     	}
     	else if (z >= 0 && z < nums.length) {
     		nums[z] = nums[i];
     		nums[i] = 0;
     		while (z < nums.length) {
     			if (nums[z] == 0) break;
     			z++;
     		}
     	}
     	i++;
     }
  }

  /**
   * A more standard O(N) solution
   */
  public static void moveZeroes2(int[] nums) {
  	if (nums == null || nums.length == 0) return;
  	int pos = 0;
  	for (int num : nums) {
  		if (num != 0) {
  			nums[pos] = num;
  			pos++;
  		}
  	}
  	while (pos < nums.length) {
  		nums[pos] = 0;
  		pos++;
  	}
  }

}