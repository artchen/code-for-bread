public class ClimbingStairs {

    public static void main(String[] args) {
        System.out.println( climbStairsPass(4) );
        System.out.println( climbStairsNoPass(4) );
        System.out.println( climbStairsNoPassRec(4) );
    }

    /* no passing finish line: loop version */
    public static int climbStairsNoPass(int n) {
        int[] count = {0, 1, 2};
        if (n < 3) {
            return count[n];
        }
        for (int i = 3; i <= n; i ++) {
            count[0] = count[1] + count[2];
            count[1] = count[2];
            count[2] = count[0]; 
        }
        return count[0];
    }

    /* no passing finish line: recursive version */
    /* This is slow! */
    public static int climbStairsNoPassRec(int n) {
        if (n < 1) return 0;
        if (n == 1) return 1;
        if (n == 2) return 2;
        return climbStairsNoPassRec(n-1) + climbStairsNoPassRec(n-2);
    }

    /* allow passing finish line */
    public static int climbStairsPass(int n) {
        if (n < 1) return 0;
        if (n == 1) return 2;
        return climbStairsPass(n-1) + n - 1;
    }
}