import java.util.*;
public class NQueensSolver {

	public static int count = 0;
	public static List<List<String>> result = new ArrayList<List<String>>();
	public static List<String> board = new ArrayList<String>();

	public static void main (String[] args) {
		
	}

  /**
   * Check position safety
   */
	public static boolean checkSafe(int row, int col) {
		int i = 0, j = 0, n = board.size();
		// check upper rows
		for (i = row; i >= 0; i--) {
			if (board.get(i).charAt(col) == 'Q')
				return false;
		}
		// check upper left diagonal
		for (i = row, j = col; i >= 0 && j >= 0; i--, j--) {
			if (board.get(i).charAt(j) == 'Q')
				return false;
		}
		// check upper right diagonal
		for (i = row, j = col; i >= 0 && j < n; i--, j++) {
			if (board.get(i).charAt(j) == 'Q')
				return false;
		}
		return true;
	}

	/**
   * Recursive solve
   */
	public static boolean solve(int row) {
		int j = 0, n = board.size();
		// if row == n, this is a solution
		if (row == n) {
			result.add(new ArrayList<String>(board)); // make a copy
			count++;
			return false;
		}
		// else keep recurring
		for (j = 0; j < n; j++) {
			if (checkSafe(row, j)) {
				place(board,row,j,"Q");
				solve(row+1);
				place(board,row,j,".");
			}
			place(board,row,j,".");
		}
		return false;
	}

	/**
   * Solve N-queens top-level
   */
  public static List<List<String>> solveNQueens(int n) {
  	initBoard(n);
    solve(0);
   	return result;
  }

  /**
   * Place a piece at (row,col)
   */
  public static void place(List<String> board, int row, int col, String piece) {
  	board.set(row, board.get(row).substring(0,col)+""+piece+board.get(row).substring(col+1));
  }

  /**
   * Initialize a chess board
   */
  public static void initBoard(int n) {
  	String row = "";
  	for (int j = 0; j < n; j++) {
			row = row + ".";
		}
  	for (int i = 0; i < n; i++) {	
  		board.add(row);
  	}
  }

  /**
   * Print a chess board
   */
  public static void print(List<String> board) {
  	for (int i = 0; i < board.size(); i ++) {
  		System.out.println(board.get(i));
  	}
  }

}