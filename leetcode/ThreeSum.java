/*
	Find all triplets that sums up to 0 in an array of integers.
*/
import java.util.*;
public class ThreeSum {
	
	public static void main (String[] args) {
		int[][] test = {
			{0,-1,1},
			{0,-1,2},
			{0,2,4,-1,3,-2,-3,1},
			{-2,0,0,2,2}
		};
		for (int i = 0; i < test.length; i++)
			print( threeSum(test[i]) );
	}

	public static void print(List<List<Integer>> list) {
		for (List<Integer> elem : list) {
			System.out.println(elem);
		}
	}

	public static List<List<Integer>> threeSum(int[] nums) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		if (nums == null || nums.length < 3) return result;
		Arrays.sort(nums);
		for (int i = 0; i < nums.length; i++) {
			if (i == 0 || i > 0 && nums[i] != nums[i-1]) {
				int start = i+1, end = nums.length-1;
				while (start < end) {
					if (nums[start] + nums[end] + nums[i] == 0) {
						List<Integer> ans = new ArrayList<Integer>();
						ans.add(nums[i]);
						ans.add(nums[start]);
						ans.add(nums[end]);
						result.add(ans);
						start++;
						end--;
						while (nums[start] == nums[start-1] && start < end) start++;
						while (nums[end] == nums[end+1] && start < end) end--;
					}
					else if (nums[start] + nums[end] + nums[i] > 0) {
						end--;
					}
					else {
						start++;
					}
				}
			}
		}
		return result;
	}

}