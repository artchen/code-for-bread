import java.util.*;
public class JumpGameII {

    public static void main(String[] args) {
        int[] nums = {5,6,4,4,6,9,4,4,7,4,4,8,2,6,8,1,5,9,6,5,2,7,9,7,9,6,9,4,1,6,8,8,4,4,2,0,3,8,5};
        System.out.println(jump(nums));
    }

    public static int jump(int[] nums) {
        int n = nums.length;
        if (n < 2) return 0;
        if (n == 2) return 1;
        int farthest = 0, range = 0,  step = 0;
        for (int i = 0;  i < nums.length; i++) {
            if (i > range) {
                range = farthest;
                step++;
            }
            farthest = farthest > i+nums[i] ? farthest : i+nums[i];
            if (farthest >= nums.length-1) {
                step++;
                break;
            }
        }
        return step;
    }

    // working solution, but time limit exceeded
    public static int jump2(int[] nums) {
        int n = nums.length;
        if (n < 2) return 0;
        if (n == 2) return 1;
        int[] min_step = new int[n];
        for (int i = nums.length-1; i >= 0; i--) {
            int allow = nums[i];
            int need = n-1-i;
            if (need <= allow) min_step[i] = 1;
            else {
                int min = Integer.MAX_VALUE;
                for (int j = allow; j > 0; j--) {
                    int tmp = min_step[i+j] < Integer.MAX_VALUE ? min_step[i+j]+1 : Integer.MAX_VALUE; 
                    min = Math.min(min,tmp);
                }
                min_step[i] = min;
            }
        }
        return min_step[0];
    }

}