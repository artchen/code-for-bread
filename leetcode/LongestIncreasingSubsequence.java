import java.util.*;

public class LongestIncreasingSubsequence {
	
	public static void main (String[] args) {
		int[] input = {10,9,3,4,2,3,4,4,5,6};
		System.out.println(lengthOfLIS(input));
	}

	public static int lengthOfLIS (int[] nums) {
		if (nums == null || nums.length == 0) return 0;
		if (nums.length == 1) return 1;
		int len = 1;
		int[] tmp = new int[nums.length];
		tmp[0] = nums[0];
		for (int i = 1; i < nums.length; i++) {
			if (nums[i] < tmp[0]) {
				tmp[0] = nums[i];
			}
			else if (nums[i] > tmp[len-1]) {
				tmp[len] = nums[i];
				len++; // len always points the next blank space
			}
			else {
				int pos = binInsert(tmp,0,len-1,nums[i]);
				tmp[pos] = nums[i];
			}
		}
		return len;
	}

	public static int binInsert(int[] tmp, int start, int end, int target) {
		while (start < end) {
			int mid = (start+end)/2;
			if (target > tmp[mid]) {
				start = mid+1;
			}
			else if (target < tmp[mid]) {
				end = mid;
			}
			else {
				return mid;
			}
		}
		return start;
	}

}