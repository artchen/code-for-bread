public class ReverseWords {
	public static void main(String[] args) {
		String str = "We are   fakes";
        String space = " 1";

        System.out.println(reverseWords(str) + "/");
        System.out.println(reverseWordsII(str) + "/");
        System.out.println( "/" + reverseWordsII(space) + "/");
	}

    /* Method 1: use trim() */
    public static String reverseWords(String s) {
        if (s == null || s.equals("")) return "";
        String str = s.trim();
        if (str.equals("")) return "";
        String[] str_arr = str.split(" ");
        String result = "";
        for (int i = str_arr.length - 1; i >= 0; i--) {
            if ( !str_arr[i].trim().equals("") ) {
                result += str_arr[i].trim() + " ";
            }
            
        }
        return result.trim();
    }

    /* Method 2: traverse w/o trim() */
    public static String reverseWordsII(String s) {
        if (s == null || s.equals("") ) return "";
        s += " ";
        String[] str_arr = s.split("");
        int start = -1;
        int end = -1;
        String result = "";

        for (int i = 0; i<str_arr.length; i++) {

            if (start == -1 && !str_arr[i].equals(" ") ) {
                start = i;
            }
            if ( (start != -1) && (end == -1) && str_arr[i].equals(" ") ) {
                end = i;
                result = " " + s.substring(start, end) + result;
                start = -1;
                end = -1;
            }
        }

        return result.trim();
    }
}