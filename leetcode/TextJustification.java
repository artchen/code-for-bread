import java.util.*;
public class TextJustification {

	public static void main(String[] args) {
		String[] test1 = {"This", "is", "an", "example", "of", "text", "justification."};
		int len1 = 16;
		String[] test2 = {"abc", ""};
		int len2 = 3;
		String[] test3 = {"Listen","to","many,","speak","to","a","few."};
		int len3 = 6;
		String[] test4 = {"a","b","c","d","e"};
		int len4 = 3;
		String[] test5 = {"What","must","be","shall","be."};
		int len5 = 12;
		print(fullJustify(test1,len1));
		print(fullJustify(test2,len2));
		print(fullJustify(test3,len3));
		print(fullJustify(test4,len4));
		print(fullJustify(test5,len5));
	}

	public static void print(List<String> list) {
		for (int i = 0; i < list.size(); i++)
			System.out.println("|" + list.get(i) + "|");
		System.out.println();
	}

	public static List<String> fullJustify(String[] words, int maxWidth) {
		List<String> result = new ArrayList<String>();
		int start = 0, end = -1, charCount = 0, wordLen = 0, wordCount = 0, newLen = 0;
		String buf = "";
		for (int i = 0; i < words.length; i++) {
			wordLen = words[i].length();
			newLen = wordCount + wordLen + charCount;

			if (maxWidth < newLen) {
				buf = buildLine(words,maxWidth,start,end,charCount,false);
				result.add(buf);
				charCount = wordLen;
				wordCount = 1;
				start = i;
				end = i;
				// if it is last line last word
				if (i == words.length-1) {
					buf = buildLine(words,maxWidth,start,end,charCount,true);
					result.add(buf);
				}
			}
			else if (maxWidth == newLen) {
				end++;
				buf = buildLine(words,maxWidth,start,end,charCount+wordLen,false);
				result.add(buf);
				charCount = 0;
				wordCount = 0;
				start = i+1;
			}
			else {
				// add current word
				charCount += wordLen;
				wordCount ++;
				end++;
				// if it is last line last word
				if (i == words.length-1) {
					buf = buildLine(words,maxWidth,start,end,charCount,true);
					result.add(buf);
				}
			}

		}
		return result;
	}

	public static String buildLine(String[] words, int maxWidth, int start, int end, int charCount, boolean last) {
		String gap = "";
		String buf = "";
		int wordCount = end-start+1;
		int spaces = maxWidth - charCount;

		if (start == end) {
			for (int j = 0; j < maxWidth-words[end].length(); j++) {
				gap = gap + " ";
			}
			buf = words[end] + gap + "";
		}
		else if (last) {
			for (int i = 0; i < maxWidth - charCount - (end-start); i++ )
				gap = gap + " ";
			for (int j = start; j <= end; j++) {
				buf = buf + words[j];
				if (j != end) {
					buf = buf + " ";
				}
				else {
					buf = buf + gap;
				}
			}
		}
		else {
			int rem = spaces % (wordCount-1);
			int div = spaces / (wordCount-1);
			for (int i = 0; i < div; i++) {
				gap = gap + " ";
			}
			for (int j = start; j <= end; j++) {
				buf = buf + words[j];
				if (j != end) {
					buf = buf + gap;
				}
				if (rem > 0) {
					buf = buf + " ";
					rem--;
				} 
			}
		}

		return buf;
	}

}