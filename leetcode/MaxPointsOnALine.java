/**
 * Definition for a point.
 * class Point {
 *     int x;
 *     int y;
 *     Point() { x = 0; y = 0; }
 *     Point(int a, int b) { x = a; y = b; }
 * }
 */
import java.util.*;
import java.math.*;
public class MaxPointsOnALine {

	public static class Point {
		int x;
		int y;
		Point() { x = 0; y = 0; }
		Point(int a, int b) { x = a; y = b; }
	}

	// y=kx+b
	public static class Line {
		public double k;
		public double b;
		public boolean vertical;
		public Line(double b) {
			b = b;
			vertical = true;
		}
		public Line(double k, double b, boolean v) {
			k = k;
			b = b;
			vertical = false;
		}
	}

	public static void main(String[] args) {
		Point a = new Point(3,10);
		Point b = new Point(0,2);
		Point c = new Point(2,2);
		Point d = new Point(1,2);
		Point[] test = {a,b,b,a};
		System.out.println(maxPoints(test));
	}

  public static int maxPoints(Point[] points) {
  	if (points.length == 0) return 0;
  	if (points.length == 1) return 1;
    HashMap<String,HashSet<Integer>> ht = new HashMap<String,HashSet<Integer>>();
    HashSet<Integer> tmp;
    String key;
    for (int i = 0; i < points.length; i++) {
    	for (int j = i+1; j < points.length; j++) {
    		key = buildKey(points[i], points[j]);
    		if (ht.containsKey(key)) {
    			tmp = ht.get(key);
    			tmp.add(i);
    			tmp.add(j);
    		}
    		else {
    			tmp = new HashSet<Integer>();
    			tmp.add(i);
    			tmp.add(j);
    			ht.put(key, tmp);
    		}
    	}
    }
    int largest = 0;
    for (Map.Entry<String,HashSet<Integer>> entry : ht.entrySet()) {
			tmp = entry.getValue();
			largest = Math.max(largest, tmp.size());
			System.out.println(entry.getKey());
		}
    return largest;
  }

  public static String buildKey(Point a, Point b) {
  	String key;
  	if (a.x == b.x) {
  		// vertical line
  		key = "" + a.x;
  	}
  	else {
  		double k = ((double)b.y-(double)a.y)/((double)b.x-(double)a.x);
  		double offset = (double)a.y - (double)k * (double)a.x;
  		BigDecimal k1 = new BigDecimal(k).setScale(6, BigDecimal.ROUND_HALF_UP);
  		BigDecimal offset1 = new BigDecimal(offset).setScale(6, BigDecimal.ROUND_HALF_UP);
  		
  		//key = k + "," + offset;
  		key = k1.toString() + "," + offset1.toString();
  	}
  	System.out.println("a=(" + a.x + "," + a.y + "), b=(" + b.x + "," + b.y + "), " + key);
  	return key;
  }

}