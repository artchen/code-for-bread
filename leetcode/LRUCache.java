import java.util.*;
public class LRUCache {
    
    public class Node {
        public Node prev;
        public Node next;
        public int value;
        public int key;
        public Node(int key, int val) {
            prev = null;
            next = null;
            this.value = val;
            this.key = key;
        }
    }
    
    private int capacity;
    private Node head;
    private Node tail;
    private HashMap<Integer,Node> cache;
    
    public LRUCache(int capacity) {
        this.capacity = capacity;
        cache = new HashMap<Integer,Node>();
    }

    public void print() {
        Node itr = head;
        while (itr != null) {
            Node prev = itr.prev;
            Node next = itr.next;
            System.out.print("key=" + itr.key + ", value=" + itr.value);
            if (prev != null)
                System.out.print(", prev=" + prev.key);
            else
                System.out.print(", prev=null");
            if (next != null)
                System.out.print(", next=" + next.key);
            else
                System.out.print(", next=null");
            System.out.println();
            itr = itr.next;
        }
    }
    
    public int get(int key) {
        if (!cache.containsKey(key)) return -1;
        
        // rearrance doubly linked list order, put accessed node to tail
        Node current = cache.get(key);
        Node prev = current.prev;
        Node next = current.next;
        
        // current is tail
        if (current == tail) {
            // no need to rearrange
        }
        // current is not tail, but current is head
        else if (current == head) {
            head = current.next; // here head is guaranteed  not null
            head.prev = null;
            current.prev = this.tail;
            current.next = null;
            this.tail.next = current;
            this.tail = current;
        }
        // current is in the middle (not head, not tail)
        // this should guarantee prev and next not null
        else {
            prev.next = next;
            next.prev = prev;
            current.prev = this.tail;
            current.next = null;
            this.tail.next = current;
            this.tail = current;
        }
        return cache.get(key).value;
    }
    
    public void set(int key, int value) {
        // key exists, modify value, rearrange doubly linked list order
        if (cache.containsKey(key)) {
            Node current = cache.get(key);
            Node prev = current.prev;
            Node next = current.next;
            // current is tail
            if (current == tail) {
                // no need to rearrange
            }
            // current is not tail, current is head
            else if (current == head) {
                head = current.next;
                head.prev = null;
                current.prev = this.tail;
                current.next = null;
                this.tail.next = current;
                this.tail = current;
            }
            // current is in the middle (not head, not tail)
            else {
                prev.next = next;
                next.prev = prev;
                current.prev = this.tail;
                current.next = null;
                this.tail.next = current;
                this.tail = current;
            }
            current.value = value;
        }
        // key not exist, cache full, replace
        else if (cache.size() == this.capacity) {
            cache.remove(head.key);
            Node nextHead = this.head.next;
            Node newNode = new Node(key,value);
            
            if (nextHead != null) {
                nextHead.prev = null;
                head = nextHead;
                newNode.prev = this.tail;
                this.tail.next = newNode;
                this.tail = newNode;
            }
            else {
                // nextHead is null means head==tail, only 1 item in linked list
                this.head = newNode;
                this.tail = newNode;
            }
            cache.put(key,newNode);
        }
        // key not exist, cache not full, just push
        else {
            Node newNode = new Node(key,value);
            // if empty cache, set newNode as head and tail
            if (this.head == null || this.tail == null) {
                this.head = newNode;
                this.tail = newNode;
            }
            // else just append to tail
            else {
                newNode.prev = this.tail;
                this.tail.next = newNode;
                this.tail = newNode;
            }
            cache.put(key,newNode);
        }
    }
}
