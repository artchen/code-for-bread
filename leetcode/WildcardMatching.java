import java.util.*;
public class WildcardMatching {

	public static void main(String[] args) {
		String s1 = "abb";
		String p1 = "**??";
		System.out.println(isMatch(s1, p1));
	}

	public static boolean isMatch(String s, String p) {
		boolean star = false;
		int i = 0, j = 0, si = -1, sj = 0;
		while (j < s.length()) {
			if (i < p.length() && (p.charAt(i) == '?' || p.charAt(i) == s.charAt(j))  ) {
				i++;
				j++;
			}
			else if (i < p.length() && p.charAt(i) == '*') {
				si = i;
				sj = j;
				i++;
			}
			else if (si != -1){
				i = si + 1;
				sj++;
				j = sj;
			}
			else return false;
		}
		while (i < p.length()) {
			if (p.charAt(i) != '*') return false;
			i++;
		}
		
		return true;
	}

}