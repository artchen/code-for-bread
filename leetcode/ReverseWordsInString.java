import java.util.*;
public class ReverseWordsInString {
	
	public static void main(String[] args) {
		System.out.println("|" + reverseWords("  Today is a build day    ") + "|");
		System.out.println("|" + reverseWords("    ") + "|");
		System.out.println("|" + reverseWords("   a ") + "|");
		System.out.println("|" + reverseWords("a") + "|");
		System.out.println("|" + reverseWords("abc deb") + "|");
		System.out.println("|" + reverseWords("a debasdf ' asd>   sdf") + "|");
		System.out.println("|" + reverseWords("abdwe debasdf ' asd>   sdf") + "|");
		System.out.println("|" + reverseWords("ab debasdf ' asd>   sdf") + "|");
	}

	// official book solution 10ms
	public static String reverseWords(String s) {
		StringBuilder sb = new StringBuilder();
		int j = s.length();
		for (int i = s.length()-1; i >= 0; i--) {
			if (s.charAt(i) == ' ') {
				j = i;
			}
			else if (i == 0 || s.charAt(i-1) == ' ') {
				if (sb.length() != 0) {
					sb.append(' ');
				}
				sb.append(s.substring(i,j));
			}
		}
		return sb.toString();
	} 

	// my solution: two pointer 6ms
	public static String reverseWords5(String s) {
		int i = s.length()-1, j = s.length()-1;
		StringBuilder sb = new StringBuilder();
		while (i >= 0 && j >= 0) {
			if (s.charAt(j) == ' ') {
				if (i == 0 && s.charAt(i) != ' ') sb.append(s.substring(i,j));
        j--;
      }
      else if (i == 0) {
      	if (s.charAt(i) == ' ') sb.append(s.substring(i+1,j+1));
      	else sb.append(s.substring(i,j+1));
      }
      else if (s.charAt(i) == ' ') {
      	sb.append(s.substring(i+1,j+1) + " ");
        j = i;
      }
      i--;
		}
		if (sb.length() > 0 && sb.charAt(sb.length()-1) == ' ') {
			sb.deleteCharAt(sb.length()-1);
		}
		return sb.toString();
	}

	// community solution: use split() and other utilities 11ms
	public static String reverseWords4(String s) {
    String[] words = s.trim().split(" +");
    Collections.reverse(Arrays.asList(words));
    return String.join(" ", words);
	}

	// my solution: use split() 11ms
	public static String reverseWords3(String s) {
		String[] arr = s.split("[ ]+");
		StringBuilder sb = new StringBuilder();
		for (int i = arr.length - 1; i >= 0; i--) {
			sb.append(arr[i]);
			sb.append(' ');
		}
		return sb.toString().trim();
	}

	// my solution: naive solution 18ms
	public static String reverseWords2(String s) {
    StringBuilder sb = new StringBuilder();
    ArrayList<String> arr = new ArrayList<String>();
    int start = 0, end = 0;
    String str = s.trim();
    while (end <= str.length() && start < str.length()) {
        if (str.charAt(start) == ' ') {
          start++;
        }
        else if (end == str.length()) {
        	arr.add(str.substring(start));
        }
        else if (str.charAt(end) == ' ') {
        	arr.add(str.substring(start,end));
          start = end+1;
          end = end++;
        }
        end++;
    }
    for (int i = arr.size()-1; i >= 0; i--) {
    	sb.append(arr.get(i) + " ");
    }
    return sb.toString().trim();
	}

}