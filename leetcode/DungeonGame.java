import java.util.*;
public class DungeonGame {

	public static void main(String[] args) {
		int[][] dungeon = {
			{-2,-3,3},
			{-5,-10,1},
			{10,30,-5}
		};
		System.out.println(calculateMinimumHP(dungeon));
	}

	public static int calculateMinimumHP(int[][] dungeon) {
		int m = dungeon.length;
		int n = dungeon[0].length;
		int delta = 0;
		int right = 0;
		int down = 0;
		if (m == 0 || n == 0) return 1;

		// dp[][] stores the minimum hp needed to get to the destination from current position
		// we traverse backwards from destination to origin
		// delta.hp = dungeon[i][j], the change of hp at current position
		int[][] dp = new int[m][n];

		// to survive at the destination position,
		// hp needed = 1 - delta.hp > 0 ? 1 - delta.hp : 1
		// hp needed = max(1-delta.hp, 1);
		delta = 1-dungeon[m-1][n-1];
		dp[m-1][n-1] = delta > 0 ? delta : 1;

		// last column (rightmost)
		// hp needed = dp[prev] - delta.hp > 0 ? dp[prev] - delta.hp : 1
		// hp needed = max(dp[perv]-delta.hp, 1)
		for (int i = m - 2; i >= 0; i--) {
			delta = dp[i+1][n-1]-dungeon[i][n-1];
			dp[i][n-1] = delta > 0 ? delta : 1;
		}

		// last row (bottom)
		// same situation as the last column
		for (int j = n - 2; j >= 0; j--) {
			delta = dp[m-1][j+1]-dungeon[m-1][j];
			dp[m-1][j] = delta > 0 ? delta : 1;
		}

		// the rest
		// hp needed (right) = max(dp[perv.right]-delta.hp, 1)
  	// hp needed (down) = max(dp[perv.down]-delta.hp, 1)
  	// hp needed = min(right,down)
		for (int i = m - 2; i >= 0; i--) {
			for (int j = n - 2; j >= 0; j--) {
				right = dp[i][j+1]-dungeon[i][j];
				right = right > 0 ? right : 1;
				down = dp[i+1][j]-dungeon[i][j];
				down = down > 0 ? down : 1;
				dp[i][j] = right < down ? right : down;
			}
		}
		return dp[0][0];
	}

}