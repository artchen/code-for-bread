import java.util.*;
public class PascalsTriangleII {
	public static void main(String[] args) {
		ArrayList<Long> result = (ArrayList<Long>)getRow(2);
		for (int i = 0; i < result.size(); i++) {
			System.out.print(result.get(i) + " ");
		}
		System.out.println();
	}
	public static List<Long> getRow(int rowIndex) {
		List<Long> result = new ArrayList<Long>();
		long temp = 1;
		for (int i = 0; i < rowIndex + 1; i++) {
			result.add((long)1);
		}
        for (int i = 1; i < rowIndex; i++) {
        	temp = result.get(i-1) * (rowIndex - i + 1) / i;
            result.set(i, temp);
            result.set(rowIndex-i, temp);
            temp = 1;
        }
        return result;
	}
}