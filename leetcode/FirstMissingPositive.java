public class FirstMissingPositive {

	public static void main(String[] args) {
		int[] test = {3,4,-1,1,1};
		System.out.println(firstMissingPositive(test));
	}

  public static int firstMissingPositive(int[] nums) {
  	for (int i = 0; i < nums.length; i++) {
  		// keep swaping until every in-bound positive number is in place
  		while (nums[i] != i+1) {
  			if (nums[i] <= 0 || nums[i] >= nums.length) break; // skip negative or out-of-bound numbers
  			if (nums[i] == nums[nums[i]-1]) break; // skip duplicate numbers
  			// swap nums[i] with nums[k-1]
  			int k = nums[i];
  			nums[i] = nums[k-1];
  			nums[k-1] = k;
  		}
  	}
  	for (int i = 0; i < nums.length; i++) {
  		if (nums[i] != i+1) {
  			return i+1;
  		}
  	}
  	return nums.length+1;
  }

}