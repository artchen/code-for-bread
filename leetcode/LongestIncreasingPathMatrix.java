public class LongestIncreasingPathMatrix {
	
	public static void main (String[] args) {

	}

	public static int longestIncreasingPath (int[][] matrix) {
		if (matrix == null || matrix.length == 0 || matrix[0].length == 0) return 0;
		int max = 0;
		int[][] dp = new int[matrix.length][matrix[0].length]; // save the max decreasing path length up to (i,j)

		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				int tmp = dfs(matrix,dp,i,j);
				max = max > tmp ? max : tmp;
			}
		}
		return max;
	}

	public static int dfs (int[][] matrix, int[][] dp, int i, int j) {
		if (dp[i][j] != 0) return dp[i][j];
		int m = matrix.length, n = matrix[0].length;
		int x = matrix[i][j];
		int tmp = 1, max = 1;
		
		if (i > 0 && matrix[i-1][j] > x) {
			tmp = dfs(matrix,dp,i-1,j)+1;
			if (tmp > max) max = tmp;
		}
		if (j > 0 && matrix[i][j-1] > x) {
			tmp = dfs(matrix,dp,i,j-1)+1;
			if (tmp > max) max = tmp;
		}
		if (i < m-1 && matrix[i+1][j] > x) {
			tmp = dfs(matrix,dp,i+1,j)+1;
			if (tmp > max) max = tmp;
		}
		if (j < n-1 && matrix[i][j+1] > x) {
			tmp = dfs(matrix,dp,i,j+1)+1;
			if (tmp > max) max = tmp;
		}

		dp[i][j] = max;
		return max;
	}

}