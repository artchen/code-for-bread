public class SetMatrixZeroes {
    public static void main(String[] args) {
        int[][] test_array = {
            {0,0,0,5},
            {4,3,1,4},
            {0,1,1,4},
            {1,2,1,3},
            {0,0,1,1}
        };
        int[][] test_array_2 = {
            {1, 1, 1},
            {0, 1, 2}
        };
        setZeroes(test_array);
        for(int i = 0; i < test_array.length; i++) {
            for (int j = 0; j < test_array[0].length; j++) {
                System.out.print(test_array[i][j] + " ");
            }
            System.out.println();
        }
    }
    public static void setZeroes(int[][] matrix) {

        boolean left_border_zero = false;
        boolean top_border_zero = false;

        /* Find zeroes and log them in the first slot in row or column */
        for(int i = 0; i < matrix.length; i++) {
            for(int j = 0; j < matrix[0].length; j++) {
                if ( matrix[i][j] == 0 ) {
                    if ( i != 0 && j != 0 ) {
                        matrix[0][j] = 0;
                        matrix[i][0] = 0;
                    }
                    if (i == 0) {
                        top_border_zero = true;
                    }
                    if (j == 0) {
                        left_border_zero = true;
                    }
                }
            }
        }
 
        /* Flood rows except top border */
        for(int i = 0; i < matrix.length; i++) {
            if (matrix[i][0] == 0 && i != 0) {
                for (int j = 0; j < matrix[0].length; j++) {
                    matrix[i][j] = 0;
                }
            }
        }

        /* Flood columns except left border */
        for(int i = 0; i < matrix[0].length; i++) {
            if (matrix[0][i] == 0 && i != 0) {
                for (int j = 0; j < matrix.length; j++) {
                    matrix[j][i] = 0;
                }
            }
        }

        /* Flood left border */
        if (left_border_zero) {
            for (int j = 0; j < matrix.length; j++ ) {
                matrix[j][0] = 0;
            }
        }

         /* Flood top border */
        if (top_border_zero) {
            for (int j = 0; j < matrix[0].length; j++ ) {
                matrix[0][j] = 0;
            }
        }

    }

}