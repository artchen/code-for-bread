/*
  Given a string S and a string T, find the minimum window in S which will contain all the characters in T in complexity O(n).

  For example,
  S = "ADOBECODEBANC"
  T = "ABC"
  Minimum window is "BANC".

  Note:
  If there is no such window in S that covers all characters in T, return the empty string "".
  If there are multiple such windows, you are guaranteed that there will always be only one unique minimum window in S.
*/

public class MinimumWindowSubstring {
  
  public static void main (String[] args) {
    String s = "ADOBECODEBANC";
    String t = "ABC";
    System.out.println( minWindow(s,t) );
  }
  
  public static String minWindow (String s, String t) {
    char[] sArray = s.toCharArray(), tArray = t.toCharArray();
    if (sArray.length < tArray.length) return "";
    int[] sMap = new int[256];
    int[] tMap = new int[256];
    
    for (char c : tArray) tMap[c]++;
    
    int left = 0, right = 0; // moving window pointer
    int start = 0, end = sArray.length; // saved window pointer
    int cnt = 0; // count of valid characters
    boolean valid = false;
    
    while (left < sArray.length && right < sArray.length) {
      char l = sArray[left];
      char r = sArray[right];
      
      // if current char is in t
      if (tMap[r] > 0) {
        // only update cnt if we haven't found enough r character
        if (sMap[r] < tMap[r]) cnt++;
        sMap[r]++;
      }
      
      // if a valid window is found
      if (cnt == tArray.length) {
        valid = true;
        while (sMap[l] == 0 || sMap[l] > tMap[l]) {
          if (sMap[l] > tMap[l]) sMap[l]--;
          left++;
          l = sArray[left];
        }
        if (right-left+1 < end-start) {
          start = left;
          end = right+1;
        }
      }
      right ++;
    }
    return valid ? s.substring(start,end) : "";
  }
}