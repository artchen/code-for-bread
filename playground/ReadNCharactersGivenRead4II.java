public class ReadNCharactersGivenRead4II {
  private char[] buffer = new char[4]; // stores the bytes already read
  private int offset = 0; // points to the byte offset after the last read
  private int bufSize = 0; // remaining characters after last read
  
  public int read4(char[] buffer) {
    return 4;
  }
  
  /**
   * @param buf Destination buffer
   * @param n   Maximum number of characters to read
   * @return    The number of characters read
   */
  public int read(char[] buf, int n) {
    int readBytes = 0;
    boolean eof = false;
    while (!eof && readBytes < n) {
      int sz = (bufSize>0) ? bufSize : read4(buffer);
      if (bufSize == 0 && sz < 4) eof = true;
      int bytes = n-readBytes < sz ? n-readBytes : sz;
      System.arraycopy(buffer,offset,buf,readBytes,bytes);
      offset = (offset+bytes) % 4;
      bufSize = sz - bytes;
      readBytes += bytes;
    }
    return readBytes;
  }
}