import java.util.*;
public class QuickSort {

     public static void main(String []args){
        int[] test = {100,99,98,97,50,25,12,10,4,3,2,1};
        System.out.println( "Original: " + Arrays.toString(test) );
        System.out.println( "Quick Sort: " + Arrays.toString(quickSort(test)) );
        System.out.println( "Insertion Sort: " + Arrays.toString(insertionSort(test)) );
     }
     
     public static int[] quickSort(int[] nums) {
         if (nums.length < 5) return insertionSort(nums);
         return sort(nums,0,nums.length-1);
     }
     
     private static int[] sort(int[] nums, int start, int end) {
         if (start >= end) return nums;
         int mid = (start+end)/2;
         int pivot = nums[median3(nums,start,mid,end)];
         // partition
         int i = start, j = end;
         while (i < j) {
             while (nums[i] < pivot) i++;
             while (nums[j] > pivot) j--;
             if (i <= j) {
                 swap(nums, i, j);
                 i++;
                 j--;
             }
         } // after the loop, i either == j or == j+1
         sort(nums,start,j);
         sort(nums,i,end);
         return nums;
     }
     
     /**
      * Find the median of three array elements, return the index of median
      */
     private static int median3(int[] nums, int start, int mid, int end) {
         int a = nums[start], b = nums[mid], c = nums[end];
         if (a >= b && a <= c || a >= c && a <= b) return start;
         else if (b >= a && b <= c || b >= c && b <= a) return mid;
         return end;
     }
     
     /**
      * Swap 2 array elements
      */
     private static void swap(int[] nums, int i, int j) {
         int tmp = nums[i];
         nums[i] = nums[j];
         nums[j] = tmp;
     }
     
     
     /**
      * Helper: Insertion sort
      */
     private static int[] insertionSort(int[] nums) {
         for (int i = 1; i < nums.length; i++) {
             int tmp = nums[i], j = i-1;
             for (; j >= 0; j--) {
                 if (nums[j] > nums[j+1]) {
                     nums[j+1] = nums[j];
                 }
             }
             if (j >= 0) nums[j] = tmp;
         }
         return nums;
     }
}
