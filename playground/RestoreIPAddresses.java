import java.util.*;
public class RestoreIPAddresses {
    
    List<String> result;
    List<String> ans;
    
    
    // iterative solution
    public List<String> restoreIpAddresses(String s) {
        result = new ArrayList<String>();
        int len = s.length();
        if (len < 4 || len > 12) return result;
        // idea: [0:i].[i:j].[j:k].[k:]
        for (int i = 1; i <= 3 && i < len-2; i++) {
            for (int j = i+1; j <= i+3 && j < len-1; j++) {
                for (int k = j+1; k <= j+3 && k < len; k++) {
                    String s1 = s.substring(0,i), s2 = s.substring(i,j), s3 = s.substring(j,k), s4 = s.substring(k);
                    if (isValid(s1) && isValid(s2) && isValid(s3) && isValid(s4)) {
                        result.add(s1 + "." + s2 + "." + s3 + "." + s4);
                    }
                }
            }
        }
        return result;
    }
    
    public boolean isValid(String s) {
        int len = s.length();
        if (len > 3 || len>1 && s.charAt(0) == '0' || Integer.parseInt(s) > 255) return false;
        return true;
    }
    
    // my original recursive backtracking solution
    public List<String> restoreIpAddresses2(String s) {
        result = new ArrayList<String>();
        if (s.length() < 4 || s.length() > 12) return result;
        ans = new ArrayList<String>();
        find(s,0);
        return result;
    }
    
    public void find(String s, int start) {
        if (start == s.length()) {
            if (ans.size() == 4) {
                result.add( build(ans) );
            }
            return;
        }
        else if (ans.size() >= 4) return;
        ans.add(s.substring(start,start+1));
        find(s,start+1);
        ans.remove(ans.size()-1);
        
        
        if (start+2 <= s.length()) {
            int intval2 = Integer.parseInt(s.substring(start,start+2));
            if (intval2 >= 10) {
                ans.add(s.substring(start,start+2));
                find(s,start+2);
                ans.remove(ans.size()-1);
            }
        }
        
        
        if (start+3 <= s.length() ) {
            int intval3 = Integer.parseInt(s.substring(start,start+3));
            if (intval3 <= 255 && intval3 >= 100) {
                ans.add(s.substring(start,start+3));
                find(s,start+3);
                ans.remove(ans.size()-1);
            }
        }
    }
    
    public String build(List<String> s) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.size(); i++) {
            sb.append(s.get(i));
            if (i < s.size()-1) sb.append('.');
        }
        return sb.toString();
    }
}