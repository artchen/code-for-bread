/**
 * Definition for an interval.
 * public class Interval {
 *     int start;
 *     int end;
 *     Interval() { start = 0; end = 0; }
 *     Interval(int s, int e) { start = s; end = e; }
 * }
 */
import java.util.*;
public class MergeIntervals {
  
  public class Interval {
    int start;
    int end;
    Interval() {
      start = 0;
      end = 0;
    }
    Interval(int start, int end) {
      this.start = start;
      this.end = end;
    }
  }
  
  public class IntervalComparator implements Comparator<Interval> {
    public int compare(Interval a, Interval b) {
      if (a.start==b.start) return a.end-b.end;
      return a.start-b.start;
    }
  }
  public List<Interval> merge(List<Interval> intervals) {
    if (intervals.size() < 2) return intervals;
    List<Interval> result = new ArrayList<Interval>();
		Collections.sort(intervals, new IntervalComparator());
		Interval current = null;
    Interval prev = new Interval(intervals.get(0).start, intervals.get(0).end);
    
		for (int i = 1; i < intervals.size(); i++) {
			current = intervals.get(i);
      // current interval does not overlap with prev, push prev to result
			if (prev.end < current.start) {
				result.add(new Interval(prev.start,prev.end));
				prev.start = current.start;
				prev.end = current.end;
			}
      // current interval overlap with prev, update prev's end property
			else if (prev.end < current.end) {
				prev.end = current.end;
			}
      // if at end, push the last interval
			if (i == intervals.size() - 1) {
				result.add(prev);
			}
		}
		return result;
  }
}