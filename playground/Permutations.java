import java.util.*;
public class Permutations {
  
  private static List<List<Integer>> result;
  private static List<Integer> ans;
  private static boolean[] used;
  
  public static void main(String[] args) {
    int[] nums = {1,2,3,4};
    print(permute(nums));
  }
  
  private static void print(List<List<Integer>> list) {
    for (int i = 0; i < list.size(); i++) {
      System.out.println(list.get(i));
    }  
  }
  
  private static List<List<Integer>> permute(int[] nums) {
    result = new ArrayList<List<Integer>>();
    ans = new LinkedList<Integer>();
    used = new boolean[nums.length];
    recursive(nums);
    return result;
  }
  
  private static void recursive(int[] nums) {
    if (ans.size() == nums.length) {
      result.add( new LinkedList<Integer>(ans) );
      return;
    }
    for (int i = 0; i < nums.length; i++) {
      if (used[i] == false) {
        ans.add(nums[i]);
        used[i] = true;
        recursive(nums);
        ans.remove(ans.size()-1);
        used[i] = false;
      }
    }
  }
  
}