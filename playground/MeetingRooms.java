/*
LeetCode: Meeting Rooms
Given an array of meeting time intervals consisting of start and end times [[s1,e1],[s2,e2],...] (si < ei), determine if a person could attend all meetings.

For example,

Given [[0, 30],[5, 10],[15, 20]],
return false.
*/

import java.util.*;
public class MeetingRooms {

  static class Interval {
    int start;
    int end;
    Interval(int start, int end) {
      this.start = start;
      this.end = end;
    }
  }
  
  static class IntervalComparator implements Comparator<Interval> {
    @Override
    public int compare(Interval a, Interval b) {
      if (a.start == b.start) return a.end-b.end;
      return a.start-b.start;
    }
  }

  public boolean canAttendMeetings(Interval[] intervals) {
    Arrays.sort(intervals, new IntervalComparator());

    for (int i = 1; i < intervals.length; i++) {
      Interval i1 = intervals[i - 1];
      Interval i2 = intervals[i];
      if (i1.end > i2.start) {
        return false;
      }
    }

    return true;
  }
}
