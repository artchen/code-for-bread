import java.util.*;
public class Subsets {
  
  public static void main (String[] args) {
    int[] nums = {1,2,3};
    print( subsets(nums) );
  }
  
  public static List<List<Integer>> subsets(int[] nums) {
    List<List<Integer>> result = new ArrayList<List<Integer>>();
    result.add(new ArrayList<Integer>()); // [] empty set counts as a subset
    Arrays.sort(nums); // sort to ensure non-descending order
    for (int i = 0; i < nums.length; i++) {
      List<List<Integer>> tmp = new ArrayList<List<Integer>>();
      for (int j = 0; j < result.size(); j++) {
        // new subsets are build upon existing subsets
        // example: 
        // [] -> [2]
        // [1] -> [1,2]
        List<Integer> newSet = new ArrayList<Integer>(result.get(j));
        newSet.add(nums[i]);
        // we need tmp to hold the new subsets of this layer
        // cannot directly add to result
        tmp.add(newSet);
      }
      for (List<Integer> newSet : tmp)
        result.add(newSet);
    }
    return result;
  }
  
  public static void print(List<List<Integer>> list) {
    for (List<Integer> l : list) System.out.println(l);
  }
  
}