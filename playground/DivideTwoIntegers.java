import java.util.*;
public class DivideTwoIntegers {
  
  public static void main (String[] args) {
    
  }
  
  public int divide (int dividend, int divisor) {
    long result = recursive(dividend, divisor);
    return result > Integer.MAX_VALUE ? Integer.MAX_VALUE : (int)result;
  }
  
  public long recursive(long dividend, long divisor) {
    int count = 0;
    boolean neg = dividend < 0 != divisor < 0;
    // convert to positive division
    if (dividend < 0) dividend = -dividend;
    if (divisor < 0) divisor = -divisor;
    if (divisor > dividend) return 0;
    long sum = divisor, divide = 1;
    while (dividend-sum >= sum) {
      sum += sum;
      divide += divide;
    }
    if (neg) return -(divide + recursive(dividend-sum, divisor));
    return divide+recursive(dividend-sum,divisor);
  }

}