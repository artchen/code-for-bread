import java.util.*;
public class Combinations {
    
  private static List<List<Integer>> result;
  private static List<Integer> ans;
  
  public static void main(String[] args) {
    
  }
  
  public static List<List<Integer>> combine(int n, int k) {
      result = new ArrayList<List<Integer>>();
      ans = new ArrayList<Integer>();
      combination(n,k,1);
      return result;
  }
  
  public static void combination(int n, int k, int start) {
      if (ans.size() == k) {
          result.add( new ArrayList<Integer>(ans) );
          return;
      }
      for (int i = start; i <= n; i++) {
          ans.add(i);
          combination(n,k,i+1);
          ans.remove(ans.size()-1);
      }
  }
}