import java.util.*;

public class QuickSelect {
  
  public static void main (String[] args) {
    int[] test = {1,6,2,7,0,5,3,4,8,10};
    int[] test2 = {6,5,4,3,2,1};
    int[] test3 = {1,2,3,4,5,6};
    System.out.println( findKthLargest(test,1) );
    System.out.println( findKthLargest(test2,2) );
    System.out.println( findKthLargest(test3,3) );
  }
  
  public static int findKthLargest(int[] nums, int target) {
    return quickSelect(nums,nums.length-target);
  }
  
  /*
    select the kth smallest item in an unsorted list
  */
  public static int quickSelect(int[] data, int target) {
    return quickSelectInRange(data,target,0,data.length-1);
  }
  
  public static int quickSelectInRange(int[] data, int target, int first, int last) {
    if (first >= last) 
      return data[first];
    // choice of pivot
    int pivotIndex = (int) (Math.random() * (last-first+1)) + first; 
    // move pivot to first
    swap(data,pivotIndex,first);
    // partition to get new pivot position
    pivotIndex = partition(data,first,last);
    int len = pivotIndex - first;
    if (target < len) 
      return quickSelectInRange(data,target,first,pivotIndex-1);
    else if (target > len) 
      return quickSelectInRange(data,target-len-1,pivotIndex+1,last);
    
    return data[pivotIndex];
  }
  
  private static int partition(int[] data, int first, int last) {
    // pivot is initially at first position
    int pivot = data[first];
    int left = first, right = last;
    while (left < right) {
      while (data[left] <= pivot && left < right) left++;
      while (data[right] > pivot) right--;
      if (left < right) swap(data,left,right);
    }
    // move pivot to middle
    swap(data,first,right);
    return right;
  }
  
  private static void swap(int[] data, int a, int b) {
    int tmp = data[a];
    data[a] = data[b];
    data[b] = tmp;
  }
  
}