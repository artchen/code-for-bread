# Dropbox Classical Interview Questions

## Leetcode: Combination Sums



## Leetcode: Game of Life

## Leetcode: Letter Combinations of a Phone Number

## Duplicate Files

```
Write a function that returns a list of lists of duplicate files in a directory.
Example input:

  /foo/

Example output:

  [
    ['/foo/bar.png', '/foo/images/foo.png'],
    ['/foo/file.tmp', '/foo/other.tmp', '/foo/temp/baz/that.foo']
  ]
```

1. Find all files using bfs or dfs. 
2. Categorize by size. 
3. Within each bucket with more than 1 files (they are of the same size), use the first 100 bytes of each file as hash key to further categorize the files. If there are duplicates, continue to try to categorize the files using the next 100 bytes until all bytes are used up. Return those remain in the same bucket.

## Buy Soda

```
We need to buy N soda, and we have sizes=`[1,2,3,5,7]` (for example), find all combinations.
Example:
N=3 sizes=[1,2]
return [[1,1,1], [1,2]]
```

## atoi

```
Given a string such as "123" or "67", write a function to output number represented by the string without using casting.
```

Same as Leetcode atoi().

## Boolean Expression eval()

```
Write a function called eval(), which takes a string and returns a boolean. 
This string is allowed 6 different characters: 0, 1, &, |, (, and ). 
Eval should evaluate the string as a boolean expression, where 0 is false, 1 is true, & is an and, and | is an or. 
An example string might look like "(0 | (1 | 0)) & (1 & ((1 | 0) & 0))"  
``` 

## sqrt()

```
Write a function to compute the square root of a number.
```

