/*
	Given a matrix of friend/enemy relationship between a group of people, your goal is to figure out if two individuals have a particular relationship chain ("friend of a friend", "enemy of a friend"). 
	For example, consider when you have the following matrix of relationships, where F means "friend of", and E means "enemy of", and an entry at (x,y) is the relationships between individual x and individual y: 

		0 1 2
	0 - F E 
	1 F - F 
	2 E F - 

	In this example, individual 0 and individual 2 have a relationship chain E (eg. 0 <-> 2), but they also have the relationship chains FF (0 <-> 1 <-> 2) and EFFE (0 <-> 2 <-> 1 <-> 0 <-> 2). 

	These relationships have the following properties:

	1. Being friends and enemies are symmetric, so if person A is friend with person B that implies that person B is also friend with Person A, similarly with enemies.
	2. A person cannot have a relationship to themself and so no relationship chain will include 0 <-> 0.
	3. The only valid relationships are F, E, or -.
	4. Given a matrix of size n * n, the indices for 2 individuals, and a relationship chain as a string, return 1 if this relationship exists between the 2 people and 0 if otherwise.

*/

import java.util.*;

public class Frenemy {
	
	public static void main (String[] args) {
		char[][] matrix = {
			{'-', 'F', 'E'},
			{'F', '-', 'F'},
			{'E', 'F', '-'}
		};
		int x = 0;
		int y = 2;
		String[] rels = {"E", "FF", "EFFE", "F", "EE", "EFFF"};
		for (int i = 0; i < rels.length; i++)
			System.out.println( isFrenemy(matrix, x, y, rels[i]) );
	}

	// dfs
	public static int isFrenemy (char[][] matrix, int x, int y, String relationship) {
		int n = matrix.length;
		int m = relationship.length();
		if (m == 0) return x == y ? 1 : 0;
		char c = relationship.charAt(0);
		for (int i = 0; i < matrix[x].length; i++) {
			if (matrix[x][i] == c && isFrenemy(matrix, i, y, relationship.substring(1))==1 ) {
				return 1;
			} 
		}
		return 0;
	}


	// dp
	public static int isFrenemy2 (char[][] matrix, int x, int y, String relationship) {
		int n = matrix.length;
		int m = relationship.length();
		boolean[][] dp = new boolean[n][m+1];
		// dp[x][y]: person x who has relation r[y] is in chain
		dp[x][0] = true;

		// traverse relation chain RTL
		for (int c = 1; c <= m; c ++) {
			char rel = relationship.charAt(c-1);
			// mark matched relations
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < n; j++) {
					// i != j: skip self to self
					// matrix[i][j] == rel: relation matched
					// dp[j][c-1]: is in chain
					if (i != j && matrix[i][j] == rel && dp[j][c-1]) {
						dp[i][c] = true;
					}
				}
			}
		}
		return dp[y][m] ? 1 : 0;
	}

}