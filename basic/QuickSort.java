import java.util.*;
public class QuickSort {

  public static void main (String[] args) {
    int[] test = {93,8,2,61,94,5,1,7,62,38,3,7,6};
    int[] test_empty = new int[0];
    int[] test_one = {1};
    int[] test_reverse = {100,99,98,97,50,25,12,10,4,3,2,1};
    System.out.println( Arrays.toString(qsort(null)) );
    System.out.println( Arrays.toString(qsort(test_empty)) );
    System.out.println( Arrays.toString(qsort(test_one)) );
    System.out.println( Arrays.toString(qsort(test)) );
    System.out.println( Arrays.toString(qsort(test_reverse)) );
  }
  
  // O(NlogN) time, O(1) space
  public static int[] qsort (int[] list) {
    if (list == null || list.length < 2) return list;
    if (list.length < 5) return isort(list);
    sort(list,0,list.length-1);
    return list;
  }
  
  // recursive in-place quick sort
  public static void sort (int[] list, int start, int end) {
    if (start >= end) return;
    int mid = start+(end-start)/2;
    int pivot = list[median3(list,start,mid,end)];
    
    int i = start, j = end;
    while (i <= j) {
      while (list[i] < pivot) i++;
      while (list[j] > pivot) j--;
      // swap
      if (i <= j) {
        int tmp = list[j];
        list[j] = list[i];
        list[i] = tmp;
        i++;
        j--;
      }
    }
    sort(list,start,j);
    sort(list,i,end);
  }
  
  // find the middle index of 3 array element
  public static int median3 (int[] list, int a, int b, int c) {
    int A = list[a];
    int B = list[b];
    int C = list[c];
    if (A >= B && A <= C || A >= C && A <= B) return a;
    else if (B >= A && B <= C || B >= C && B <= A) return b;
    return c;
  }
  
  // O(N^2) time, O(1) space
  public static int[] isort (int[] list) {
    int tmp, j;
    for (int i = 1; i < list.length; i++) {
      tmp = list[i];
      for (j = i-1; j >= 0 && list[j] > tmp; j--) {
        list[j+1] = list[j];
      }
      list[j+1] = tmp;
    }
    return list;
  }
  
}