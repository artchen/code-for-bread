public class Fibonacci {

	private static int limit = 10;
	private static int[] dp = new int[limit+1];
	
	public static void main(String[] args) {
		System.out.print("Recursive print(): ");
		print_rec(0,1,1);
		System.out.println("Recursive DP sum(): " + sum_rec(0,1,1));

		System.out.print("Iterative print(): ");
		print_itr();
		System.out.println("Iterative sum(): " + sum_itr());

		System.out.println("3rd: " + nth(limit));
		System.out.println("3rd: " + nth_dp(limit));
	}

	public static void print_rec(int prev, int current, int count) {
		if (count == limit) {
			System.out.println(current);
			return;
		}
		System.out.print(current + ", ");
		print_rec(current,prev+current,count+1);
	}

	public static void print_itr() {
		int prev = 0, current = 1, next = prev+current;
		for (int i = 0; i < limit; i++) {
			System.out.print(current);
			if (i < limit-1) System.out.print(", ");
			next = current+prev;
			prev = current;
			current = next;
		}
		System.out.println();
	}

	public static int sum_rec(int prev, int current, int count) {
		if (count == limit) return current;
		return current+sum_rec(current,prev+current,count+1);
	}

	public static int sum_itr() {
		int prev = 0, current = 1, next = prev+current, sum = 0;
		for (int i = 0; i < limit; i++) {
			sum += current;
			next = current+prev;
			prev = current;
			current = next;
		}
		return sum;
	}

	public static int nth(int n) {
		if (n == 1 || n == 2) return 1;
		return nth(n-1) + nth(n-2);
	}

	public static int nth_dp(int n) {
		if (dp[n] != 0) return dp[n];
		dp[n] = nth(n-1) + nth(n-2);
		return dp[n];
	}

}