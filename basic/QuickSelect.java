import java.util.*;

public class QuickSelect {

	public static void main (String[] args) {
		int[] test = {38,27,43,3,39,9,82,10,2,67,32,5};
		System.out.println( Arrays.toString(test) );
		int index = quickSelect(test, 3);
		System.out.println( "The 3rd smallest element is " + test[index]);
		System.out.println( Arrays.toString(test) );
	}

	public static void swap (int[] data, int i, int j) {
		int tmp = data[i];
		data[i] = data[j];
		data[j] = tmp;
	}

	public static int partition (int[] data, int start, int end) {
		int pivot = data[start];
		int left = start, right = end;

		while (left < right) {
			while (data[left] <= pivot && left < right) left++;
			while (data[right] > pivot) right--;
			if (left < right) swap(data, left, right);
		}

		swap(data, start, right);
		return right;
	}

	public static int quickSelectInRange (int[] data, int start, int end, int target) {
		if (start >= end) return start;

		int r = (int) (Math.random() * (end - start + 1)) + start;
		swap(data, start, r);

		int pivot = partition(data,start,end);
		int len = pivot - start;
		if (len > target) return quickSelectInRange(data, start, pivot-1, target);
		if (len < target) return quickSelectInRange(data, pivot+1, end, target);

		return pivot;
	}

	public static int quickSelect(int[] data, int target) {
		return quickSelectInRange(data,0,data.length-1,target);
	}
}