import java.util.*;
public class TreeTraversal {

	public static class TreeNode {
		TreeNode left;
		TreeNode right;
		int val;
		TreeNode(int val) {
			this.val = val;
		}
	}

	public static void main(String[] args) {
		TreeNode root = new TreeNode(1);
		root.left = new TreeNode(2);
		root.right = new TreeNode(3);
		root.left.left = new TreeNode(4);
		root.left.right = new TreeNode(5);
		root.right.left = new TreeNode(6);
		root.right.right = new TreeNode(7);

		System.out.println("Pre-order(recursion):");
		System.out.println(pre_order(root) + "\n");

		System.out.println("Pre-order(iteration+stack):");
		System.out.println(pre_order_itr(root)+ "\n");

		System.out.println("Post-order(recursion):");
		System.out.println(post_order(root)+ "\n");

		System.out.println("Post-order(iteration+stack):");
		System.out.println(post_order_itr(root)+ "\n");

		System.out.println("In-order(recursion):");
		System.out.println(in_order(root)+ "\n");

		System.out.println("In-order(iteration+stack)");
		System.out.println(in_order_itr(root)+ "\n");

		System.out.println("Level-order(recursion):");
		System.out.println(level_order(root)+ "\n");
		System.out.println();

		System.out.println("Level-order(iteration+queue):");
		System.out.println(level_order_itr(root)+ "\n");
		System.out.println();
	}


	/* Pre-order: root-left-right */

	public static List<Integer> pre_order(TreeNode root) {
		List<Integer> result = new ArrayList<Integer>();
		if (root == null) return result;
		pre_order_rec(root, result);
		return result;
	}

	public static void pre_order_rec(TreeNode root, List<Integer> result) {
		if (root == null) return;
		result.add(root.val);
		pre_order_rec(root.left,result);
		pre_order_rec(root.right,result);
	}

	public static List<Integer> pre_order_itr(TreeNode root) {
		List<Integer> result = new ArrayList<Integer>();
		if (root == null) return result;
		TreeNode itr = root;
		Stack<TreeNode> stack = new Stack<TreeNode>();
		stack.push(itr);
		while (!stack.empty()) {
			itr = stack.pop();
			result.add(itr.val);
			if (itr.right != null) stack.push(itr.right);
			if (itr.left != null) stack.push(itr.left);
		}
		return result;
	}

	/* Post-order: left-right-root */

	public static List<Integer> post_order(TreeNode root) {
		List<Integer> result = new ArrayList<Integer>();
		if (root == null) return result;
		post_order_rec(root,result);
		return result;
	}

	public static void post_order_rec (TreeNode root, List<Integer> result) {
		if (root == null) return;
		post_order_rec(root.left,result);
		post_order_rec(root.right,result);
		result.add(root.val);
	}

	public static List<Integer> post_order_itr(TreeNode root) {
		List<Integer> result = new ArrayList<Integer>();
		if (root == null) return result;
		Stack<TreeNode> stack = new Stack<TreeNode>();
		TreeNode current = root;
		TreeNode prev = null;
		stack.push(current);

		while (!stack.empty()) {
			current = stack.peek();

			// current is root, or traversing down via left or right route
			if (prev == null || prev.left == current || prev.right == current) {
				// go to left subtree first
				if (current.left != null) stack.push(current.left);
				// then right subtree
				else if (current.right != null) stack.push(current.right);
				// then myself
				else result.add(stack.pop().val);
			}
			// left subtree visited, back to parent
			else if (current.left == prev) {
				if (current.right != null) stack.push(current.right);
				else result.add(stack.pop().val);
			}
			// right subtree visited, back to parent
			else if (current.right == prev) {
				result.add(stack.pop().val);
			}
			prev = current;
		}
		return result;
	}

	/* In-order: left-root-right */

	public static List<Integer> in_order(TreeNode root) {
		List<Integer> result = new ArrayList<Integer>();
		if (root == null) return result;
		in_order_rec(root,result);
		return result;
	}

	public static void in_order_rec (TreeNode root, List<Integer> result) {
		if (root == null) return;
		in_order_rec(root.left,result);
		result.add(root.val);
		in_order_rec(root.right,result);
	}

	public static List<Integer> in_order_itr(TreeNode root) {
		List<Integer> result = new ArrayList<Integer>();
		if (root == null) return result;
		TreeNode itr = root;
		Stack<TreeNode> stack = new Stack<TreeNode>();
		while (!stack.empty() || itr != null) {
			if (itr == null) {
				itr = stack.pop();
				result.add(itr.val);
				itr = itr.right;
			}
			else {
				stack.push(itr);
				itr = itr.left;
			}
		}
		return result;
	}

	/* Level-order: by level */

	public static List<List<Integer>> level_order(TreeNode root) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		if (root == null) return result;
		int height = get_height(root);
		for (int i = 1; i <= height; i++) {
			ArrayList<Integer> level = new ArrayList<Integer>();
			level_order_rec(root, i, level);
			result.add(level);
		}
		return result;
	}

	public static void level_order_rec(TreeNode root, int lev, List<Integer> level) {
		if (root == null)
      return;
		if (lev == 1)
			level.add(root.val);
		else {
			level_order_rec(root.left, lev-1, level);
			level_order_rec(root.right, lev-1, level);
		}
	}

	private static int get_height(TreeNode root) {
		if (root == null) return 0;
		int left = get_height(root.left);
		int right = get_height(root.right);
		if (left > right) {
			return 1+left;
		}
		return 1+right;
	}

	public static List<List<Integer>> level_order_itr(TreeNode root) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		if (root == null) return result;
		Deque<TreeNode> queue = new ArrayDeque<TreeNode>();
		queue.add(root);
		int height = get_height(root);
		for (int i = 0; i < height; i++) {
			List<Integer> level = new ArrayList<Integer>();
			int count = queue.size();
			while (count > 0) {
				TreeNode itr = queue.poll();
				level.add(itr.val);
				if (itr.left != null)
					queue.add(itr.left);
				if (itr.right != null)
					queue.add(itr.right);
				count--;
			}
			result.add(level);
		}
		return result;
	}

}
