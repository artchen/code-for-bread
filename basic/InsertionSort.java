import java.util.*;
public class InsertionSort {

  public static void main (String[] args) {
    int[] test = {93,8,2,61,94,5,1,7,62,38,3,7,6};
    int[] test_empty = new int[0];
    int[] test_one = {1};
    int[] test_reverse = {100,99,98,97,50,25,12,10,4,3,2,1};
    System.out.println( Arrays.toString(isort(test)) );
    System.out.println( Arrays.toString(isort(test_reverse)) );
  }
  
  // O(N^2) time, O(1) space
  public static int[] isort (int[] list) {
    int tmp, j;
    for (int i = 1; i < list.length; i++) {
      tmp = list[i];
      for (j = i-1; j >= 0 && list[j] > tmp; j--) {
        list[j+1] = list[j];
      }
      list[j+1] = tmp;
    }
    return list;
  }
}