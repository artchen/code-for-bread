import java.util.*;

public class CheckBST {

	static class TreeNode {
		public int val;
		public TreeNode left;
		public TreeNode right;
		public TreeNode(int val) {
			this.left = null;
			this.right = null;
			this.val = val;
		}
	}
	
	public static void main (String[] args) {
		TreeNode root = new TreeNode(10);
		root.left = new TreeNode(7);
		root.right = new TreeNode(15);
		root.left.right = new TreeNode(8);
		root.left.right.left = new TreeNode(2);
		root.left.right.right = new TreeNode(20);

		System.out.println( check(root) );
	}

	public static boolean check (TreeNode root) {
		return checkWithRange(root, Integer.MIN_VALUE, Integer.MAX_VALUE);
	}

	public static boolean checkWithRange (TreeNode root, int min, int max) {
		if (root == null) return true;
		if (root.val >= max || root.val <= min) return false;
		return checkWithRange(root.left, min, root.val) && checkWithRange(root.right, root.val, max);
	}

}