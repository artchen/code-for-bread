import java.util.*;
public class MergeSort {
  
  public static void main (String[] args) {
    int[] test = {93,8,2,61,94,5,1,7,62,38,3,7,6};
    int[] test_empty = new int[0];
    int[] test_one = {1};
    int[] test_reverse = {100,99,98,97,50,25,12,10,4,3,2,1};
    System.out.println( Arrays.toString(msort(null)) );
    System.out.println( Arrays.toString(msort(test_empty)) );
    System.out.println( Arrays.toString(msort(test_one)) );
    System.out.println( Arrays.toString(msort(test)) );
    System.out.println( Arrays.toString(msort(test_reverse)) );
  }

  // O(NlogN) time, O(N) space
  public static int[] msort (int[] list) {
    if (list == null || list.length < 2) return list;
    return split(list,0,list.length-1);
  }
  
  public static int[] split (int[] list, int start, int end) {
    if (start >= end) return new int[] {list[start]};
    int mid = start+(end-start)/2; // prevent overflow
    int[] left = split(list, start, mid);
    int[] right = split(list, mid+1, end);
    return merge(left,right);
  }
  
  public static int[] merge (int[] a, int[] b) {
    int[] res = new int[a.length+b.length];
    int i = 0, j = 0, k = 0;
    while (i < a.length && j < b.length) {
      if (a[i] < b[j]) {
        res[k] = a[i];
        i++;
        k++;
      }
      else {
        res[k] = b[j];
        j++;
        k++;
      }
    }
    while (i < a.length) {
      res[k] = a[i];
      i++;
      k++;
    }
    while (j < b.length) {
      res[k] = b[j];
      j++;
      k++;
    }
    return res;
  }

}