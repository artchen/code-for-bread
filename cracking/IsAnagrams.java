/*
	1.4 
	Write a method to decide if two strings are anagrams or not.
*/
import java.util.*;
public class IsAnagrams {
	
	public static void main (String[] args) {
		String a = "abcdefg";
		String b = "adegbfc";
    if (isAnagrams(a,b)) System.out.println("correct");
    else System.out.println("incorrect");
	}

	public static boolean isAnagrams (String a, String b) {
    if (a.length() != b.length()) return false;
		int[] set = new int[256];
    for (int i = 0; i < a.length(); i++) {
      set[a.charAt(i)]++;
      set[b.charAt(i)]--;
    }
    for (int cnt : set) {
      if (cnt != 0) return false;
    }
		return true;
	}


}