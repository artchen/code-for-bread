/*
	1.1 
	Implement an algorithm to determine if a string has all unique characters.
	Use no additional data structure.
*/

import java.util.*;

public class AllUniqueCharInString {
	
	public static void main (String[] args) {
		System.out.println(allUnique("abc"));
		System.out.println(allUnique("aabc"));
	}

	// O(N) time, O(N) space
	public static boolean allUnique (String str) {
		if (str == null || str.length() <= 1 ) return true;
		int[] chars = new int[256]; // 256 ASCII
		for (int i = 0; i < str.length(); i++) {
			int pos = (int)(str.charAt(i));
			chars[pos]++;
			if (chars[pos] > 1) return false;
		}
		return true;
	}

	// O(N^2) time, no extra space
	public static boolean allUnique2 (String str) {
		if (str == null || str.length() <= 1 ) return true;
		for (int i = 0; i < str.length(); i++) {
			char a = str.charAt(i);
			for (int j = i+1; j < str.length(); j++) {
				if (a == str.charAt(j)) return false;
			}
		}
		return true;
	}

}