/*
	1.2 
	Write a function to reverse a string
*/

import java.util.*;

public class ReverseString {
	
	public static void main (String[] args) {
		String test = "";
		System.out.println(test + ": " + reverseString(test));
		String test2 = "ABC";
		System.out.println(test2 + ": " + reverseString(test2));
		String test3 = "ABCD";
		System.out.println(test3 + ": " + reverseString(test3));
	}

	// O(logN) time, O(N) space, N = str.length()
	public static String reverseString(String str) {
		if (str == null || str.length() <= 1) return str;
		int len = str.length();
		int i = 0, j = len-1;
		char[] s = str.toCharArray();
		while (i < j) {
			char tmp = s[i];
			s[i] = s[j];
			s[j] = tmp;
			i++;
			j--;
		}
		return new String(s);
	}

}