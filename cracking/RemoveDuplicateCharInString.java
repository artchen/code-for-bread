/*
	1.3 
	Design an algorithm adn write code to remove the duplicate characters in a 
	string without using any additional buffer.
*/

import java.util.*;

public class RemoveDuplicateCharInString {
	
	public static void main (String[] args) {
		String[] tests = {
			"",
			"a",
			"aabbccdd",
			"abaaac",
			"aaaaab",
			"aabcaadeaa"
		};
		for (int i = 0; i < tests.length; i++) {
			System.out.println( removeDuplicate(tests[i]) );
		}	
	}

	public static String removeDuplicate(String str) {
		StringBuilder s = new StringBuilder();
		int len = str.length();
		boolean dup = false;
		for (int i = 0; i < len; i++) {
			for (int j = 0; j < s.length(); j++) {
				if (s.charAt(j) == str.charAt(i)) dup = true;
			}
			if (!dup) s.append(str.charAt(i));
			dup = false;
		}
		return s.toString();
	}

}