/*
	1.2 
	Write code to reverse a C-style String
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

int 
main(int argc, char **argv)
{
	if (argc != 2) {
		printf("usage: reverse_string <string to reverse>\n");
		return 0;
	}

	char *s = argv[1];
	char *i = s;
	char *j = s + strlen(s)-1;
	while (i < j) {
		char c = *(i);
		*(i) = *(j);
		*(j) = c;
		i++;
		j--;
	}

	printf("%s\n", s);

	return 0;
}