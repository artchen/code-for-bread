# Javascript Lessons

Understand the basics of Javascript, and prepare for javascript interviews.

## Variable

### Hoisting

All variable declaration are hoisted (lifted and declared) to the top of the function.

## Functions

Functions are first-class objects.

### What is **context**?

**context** is the subject that invokes a function, similar to the "object" in the sense of an OOP language.

In the following example, two objects, man and peter, invokes getName() function, and get different results based on their properties.

```javascript
var man = {
  name: "Anonymous",
  getName: function() {
    console.log(this.name);
  }
};
var peter = {
  name: "Peter"
};
man.getName(); // the context is the person object, output "Anonymous"
man.getName.apply(peter); // the context is the peter object, output "Peter"
```

### What does **this** refer to?

`this` refers to the function context. It is used inside a function A, and it contains the value of the object that invokes function A. `this` is defined at the moment when an object invokes the function. Before that, it is undefined.

Common cases:
* For top-level (global) functions, `this` refers to the `window` object.
* For methods (functions defined as a member property of an object), `this` refers to the owning object.
* For constructors, `this` refers to the object instance.

...and there are some tricky cases.


#### Fix **this** in a method passed as a callback

Use bind().

Create and return a new anonymous function that calls the original function using `apply()`, so that we can force the context to be whatever object we want.

```javascript
  var button = {
    clicked: false,
    click: function() {
      this.clicked = true;
    }
  };
  
  function bind(context,name) {
    return function() {
      return context[name].apply(context,arguments);
    }
  }
  
  // usage: bind(context,"name of function")
  var elem = document.getElementById("test");
  elem.addEventListener("click", bind(button,"click"), false);
  //or use elem.addEventListener("click", button.click.bind(button), false);
```

By default, `addEventListener()` will change the context `this` of `click()` invocation to the DOM element `<button>`, however, we want `this` to be the button object we created.

```javascript
  Function.prototype.bind = function() {
    var fn = this;
    var args = Array.prototype.slice.call(arguments);
    var object = args.shift(); // poll()
    return function() {
      return fn.apply(object, args.concat(Array.prototype.slice.call(arguments)));
    };  
  };
```

#### Fix **this** inside closure

In a inner (anonymous) function like the following example (the second this in forEach() callback), `this` does not refer to the object user. Under strict mode, it is undefined.

We can fix the `this` in the inner function by save a reference in the scope of the member function `handler()`. Thus within the closure of handler(), self will be accessible for inner functions as a reference to the user object.

```javascript 
  var user = {
    data: [{name: "adam"}, {name: "peter"}],
    handler: function() {
      console.log(this); // this refers to user object
      var self = this;
      this.data.forEach(function(person) {
        console.log(this); // this refer to [object Window] or undefined if strict mode is used
        console.log(self);
      });
    }
  }
```

#### Fix **this** when method is assigned to a variable

If a member function is assigned to a global variable, the this refers to undefined under strict mode.

```javascript
  var user = {
    data: ["mom", "dad"],
    showData: function(event) {
      console.log(this); // if invoked by user.showData(), this refers to the user object; if invoked by showUserData(), this is undefined.
      console.log(this.data[0]);
    }
  };
  var showUserData = user.showData;
  showUserData(); // this will throw an undefined exception
```

To fix this, use `bind()`.

```javascript
  var showUserData = user.showData.bind(user);
  showUserdata(); // this works fine
```

### apply() vs. call()

* apply(): `function.apply(context, [arg1, arg2, ...])`
* call(): `function.call(context, arg1, arg2, ...)`

Example:

```javascript
  function example() {
    var result = 0;
    for (var i = 0; i < arguments.length; i++) {
      result += arguments[i];
    }
    this.result = result;
  }
  
  var obj1 = {}, obj2 = {};
  
  example.apply(obj1, [1,2,3]); // obj1.result = 6
  example.apply(obj1, 1,2,3,4); // obj2.result = 10
```

### Forcing the function context in callbacks

Example:

```javascript
  function forEach(list, callback) {
    for (var i = 0; i < list.length; i++) {
      callback.call(list[i],i);
    }
  }
  var weapons = ['shuriken', 'katana', 'nunchuncks'];
  forEach(weapons,function(index) {
    if (this == weapons[index]) console.log("this == weapons[index]");
  });
```


### Inline Functions

Example 1: 

```javascript
  var ninja = {
    chirp: function(n) {
      return n > 1 ? this.chirp(n-1) + "-chirp" : "chirp";
    }
  };
  var samurai = {chirp: ninja.chirp};
  ninja = {};
  samurai.chirp();
```

In Example 1, the last line `samurai.chirp()` will fail because on previous line the ninja object was wiped, so `this.chirp(n-1)` lost its reference.

Example 2:
```javascript
  var ninja = {
    chirp: function signal(n) {
      return n > 1 ? signal(n-1) + "-chirp" : "chirp"; 
    }
  };
  var samurai = {chirp: ninja.chirp};
  ninja = {};
  samurai.chirp();
```

In this example, the last line still works, becuase `samurai.chirp` now refers to an inline function `signal()`. Even though ninja's properties are wiped out, `signal()` still exist and can be referenced.

### Fun facts

apply() and call() can be used to fool array methods to perform operations on arguments.

```javascript
function test() {
  Array.prototype.slice.call(arguments, 1);
}
```

Function's length property gives the expected number of arguments.

```javascript
  function record(name,age) {}
  assert(record.length === 2, "expect 2 arguments");
```

We can use `argument.length` to overload functions.

```javascript
  var ninja = {
    whatever： function() {
      switch (arguments.length) {
        case 0: break;
        case 1: break;
        case 2: break;
        //...
      }
    }
  }
```

A function to add method to an object.

```javascript
  function addMethod(obj, name, fn) {
    var old = obj[name];
    obj[name] = function() {
      if (fn.length == arguments.length) {
        return fn.apply(this, arguments);
      }
      else {
        return old.apply(this, arguments);
      }
    }
  }
```

A function to check whether an object is a function (imperfect).

```javascript
  function isFunction(fn) {
    return Object.prototype.toString.call(fn) === "[object Function]";
  }
```

This function can be invoked by either `sum(2,3)` or `sum(2)(3)`.

```javascript
  function sum(x) {
    if (arguments.length == 2) return arguments[0] + arguments[1];
    return function(y) {return x + y};
  }
```

### Partially applying functions

We can prefill arguments to a function before execution. Effectively, it returns a function with predefined arguments.

```javascript
  Function.prototype.partial = function() {
    var fn = this, args = Array.prototype.slice.call(arguments);
    return function() {
      var arg = 0;
      for (var i = 0; i < args.length && arg < arguments.length; i++) {
        if (args[i] === undefined) {
          args[i] = arguments[arg++];
        }
      }
      return fn.apply(this,args);
    };
  };
```

### Memoization

A technique to cache function execution results.

```javascript
  Function.prototype.memoized = function(key) {
    this._values = this._values || {};
    return this._values[key] !== undefined ? this._values[key] : this._values[key] == this.apply(this, arguments);
  };
  // usage: function_name.memoized(args);
```

A technique to memoize function using closure.

```javascript
  Function.prototype.memoize = function() {
    var fn = this;
    return fn.memoized.apply(fn, arguments);
  };
  
  var isPrime = (function(num) {
    var prime = num != 1;
    for (var i = 2; i < num; i++) {
      if (num % i == 0) {
        prime = false;
        break;
      }
    }
    return prime;
  }).memoize();
  // now functions are called in normal manner.
  isPrime(17); // returns true
```

### Function wrapping

A technique for encapsulating the logic of a function while overwriting it with new or extended functionality in a single step.

```javascript
  function wrap(object, method, wrapper) {
    var fn = object[method]; // save the original version
    return object[method] = function() {
      wrapper.apply(this, [fn.bind(this)].concat(Array.prototype.slice.call(arguments)));
    };
  }
```

### Immediate functions

```javascript
  (function() {
    
  })();
```

## Object-oriented Javascript

### Instantiation and prototypes

```javascript
  function Ninja() {
    this.swung = false;
    this.swingSword = function() {
      return !this.swung;
    };
  }
  Ninja.prototype.swingSword = function() {
    return this.swung;
  };
  var ninja = new Ninja();
  ninja.swingSword(); // we are calling the instance function, not the prototype function. output: true
```

### Object typing via constructors

```javascript
  function Ninja() {}
  var ninja = new Ninja();
  // typeof ninja == "object"
  // ninja instanceof Ninja === true
  // ninja.constructor == Ninja
```

### Inheritance and prototype chain

```javascript
  function Person() {}
  Person.prototype.dance = function() {};
  
  function Ninja() {}
  Ninja.prototype = {dance: Person.prototype.dance};
  
  var ninja = new Ninja();
  // ninja instanceof Ninja === true
  // ninja instanceof Person === true
  // ninja instanceof Object === true
```

Create prototype chain by `SubClass.prototype = new SuperClass()`. SubClass has access to the methods of the SuperClass.

```javascript
  Array.prototype.forEach = function(callback, context) {
    for (int i = 0; i < this.length; i++) {
      callback.call(context || null, this[i], i, this);
    }
  };
  [1,2,3].forEach(function(value,index,array) {
    // do sth.
  });
```

All DOM elements inherit from a class called `HTMLElement`.

## Regular Expression

Check if a string is a palindrome. Ignore punctuation symbols.

```javascript
  // standard
  function isPalindrome(s) {
    var i = 0, j = s.length-1;
    var str = s.toLowerCase();
    while (i < j) {
      if (s.charAt(i) < 'a' || s.charAt(i) > 'z') {
        i++;
        continue;
      }
      if (s.charAt(j) < 'a' || s.charAt(j) > 'z') {
        j--
        continue;
      }
      if (s.charAt(i) !== s.charAt(j)) return false;
      i++;
      j--;
    }
    return true;
  }
  // regex
  function isPalindorme2(str) {
    str = str.replace(/\W/g, '').toLowerCase();
    return (str == str.split('').reverse().join(''));
  }
```

## Timer and Interval

Asynchronuous events such as click handler, timer and interval handlers are queued up because Javascript engines are single-threaded by nature.

If a timer is blocked from immediately executing, it will be delayed until the next available time of execution. The actual waiting time will always be longer than or equal to the specified delay, but never shorter.

Intervals may end up executing back to back with no delay if they get backed up enough.

Multiple instances of the same interval handler will never be queued up.

The browsers will shut down irresponsible javascript code from executing, usually 5 seconds. If the computation is expected to be very time consuming, we can use setTimeout() to divide up big tasks into multiple phases so that they won't be killed by the browser.

### Central Timer Control

Only one timer running per page at a time.
Pause and resume the timers at will.
The process for removing callback function is trivialized.

### Asynchronous testing




