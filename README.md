# Code for Bread

Code not for fun.

## Table of content

* Basic data structure and algorithm implementation
* Leetcode solutions 
* Cracking the code interview solutions
* Javascript key concepts
* Real interview questions categorized by company
	- Amazon
	- Facebook
	- Dropbox
	- Pocket Gems
  - Palantir
*
