# Leetcode Brief Hints

* 1 Two Sum 
  - HashMap
  
* 2 Add Two Numbers (represented as linked-list) 
  - be careful for the last "overflowed" carry. 
  - lists may have different length.
  
* 7 Reverse Integer
  - keep dividing 10 to extract digits
  - be careful for integer overflow. For example, `1147483647`, the reverse causes integer overflow

* 8 String to Integer (atoi)
  - leading blank spaces, sign
  - stop processing and return processed value when encountered invaliad characters
  - use long to store temporary result to avoid overflow. check overflow for each iteration
  
* 9 Palindrome Number
  - use the same technique in `7 Reverse Integer`
  - a number is palindrome if it equals its reverse

* 14 Longest Common Prefix
  - find the shortest string
  - go thru the characters in the shortest string, check if all the strings in the array has the same character at the same location, if not, return the current examined substring of the shortest string

* 15 3Sum
  - sort first, to skip duplicates
  - nested loop. fix one number, then use two pointer to find the other 2
  - skip if current==prev 
  
* 16 3Sum Closest
  - similar to 3sum
  - target smaller difference instead of equality
  
* 18 4Sum
  - based on 3Sum
  - use pruning to make algorithm run faster

* 19 Remove Nth Node From End of List
  - let iterator A move n steps forward
  - let iterator A and B move together until A moves out of bound, keep B's previous node as prev, prev is initialized as null
  - if prev is null, remove head; otherwise remove B
  
* 20 Valid Parentheses  
  - stack

* 26 Remove Duplicates from Sorted Array
  - two pointer
  - use i to iterate the array, use j to mark the good region without duplicate
  - if number at i is different from number at j, put number at i on index j and increment j

* 28 Implement strStr() 
  - brute force is easy to implement O(N^2) nested loop
  - better approach uses KMP algorithm

* Divide Two Integers
  - 

* 39 Combination Sum
  - similar to 77 combinations
  - sort first

* 40 Combination Sum II
  - add one conditional to eliminate duplicates

* 43 Multiply Strings
  - 

* 46 Permutations
  - keep a boolean array to mark the used elements
  - keep a temporary list to store the current path, eliminate previous choice when backtracking
  - dfs strategy

* 47 Permutations II
  - almost identical strategy to 46 Permutations. 
  - sort first
  - add one conditional to stop current dfs path if previous number is the same as current

* 56 Merge Intervals
  - 
  
* 61 Rotate List 
  - count total nodes first
  - be careful for cases when k >= count. if k is multiples of count, no rotation is needed
  
* 66 Plus One 
  - be careful that digits could increment by 1 if MSB (at index 0) overflows, aka array expansion needed
  
* 75 Sort Colors
  - Two pass: counting sort
  - One pass: two pointers (two and zero) and one iteration pointer. swap zeroes to front, swap twoes to end, let ones automatically remain in middle. 

* 76 Minimum Window Substring
  - keep track of moving window and saved window
  - use hashmap to store the occurrance of characters in t

* 77 Combinations
  - dfs backtrack

* 78 Subsets
  - sort first
  - use a tmp list of sets, add to final results layer by layer

* 80 Remove Duplicates from Sorted Array II
  - save technique as I, only release constraint to 2

* 82 Remove Duplicates from Sorted List II
  - 
  
* 83 Remove Duplicates from Sorted List  
  - trivial one-pass iteration

* 91 Decode Ways
  - check if the most recent 2 characters form 1X or 21-26
  - check if current character is '0'
  - if prev is not '1' or '2' and current is '0', return 0

* 93 Restore IP Addresses
  - be careful about leading zeroes: "01" or "010" are not valid IP segments
  - recursive backtracking solution is easier to implement
  - iterative idea: `[0:i].[i:j].[j:k].[k:]`

* 102 Binary Tree Level Order Traversal
  - standard queue-based iterative bfs
 
* 108 Convert Sorted Array to Binary Search Tree
  - user middle value as root of the current subtree
  - recursively build left and right subtree

* 109 Convert Sorted List to Binary Search Tree
  - similar idea as 108
  - recursively find middle node as the root of current subtree
  - to find middle node of a linked list, keep 2 iterators, one goes at 2x speed
  
* 111 Minimum Depth of Binary Tree 
  - recursive depth-first traversal and backtrack
  - be careful for cases like `[1,2]`, its min-depth is 2, not 1
  
* 133 Clone Graph
  - dfs or bfs, keep a hashmap to store created node
  - use hashmap to back link created nodes
 
* 143 Reorder List
  - find middle
  - reverse right half
  - reorder by insertion

* 152 Maximum Product Subarray
  - dp: keep track of local maximal, minimal and global maximum
  - `max[i] = Math.max(n*max[i-1], n*min[i-1], n)`, if local max > global max, update global max
  - `min[i] = Math.min(n*max[i-1], n*min[i-1], n)`

* 157 Read N Characters Given Read4
  - be careful that n might be longer than the buffer
  - be careful for the last read that might not by 4-aligned
  
* 158 Read N Characters Given Read4 II - Call multiple times
  - save global variables of offset and buffer

* 160 Intersection of Two Linked Lists
  - common length + a length + b length = common length + b length + a length
  - let two iterators go thru two list, change to another list if run out current list, they will meet at the joint

* 204 Count Primes
  - use an array to mark multiples of visited numbers, better time efficiency
 
* 207 Course Schedule
  - find cycle in graphs (woods)
  - dfs, keep a `visited[]` array
  
* 210 Course Schedule II
  - similar as course scheudle I
  - keep track of path

* 215 Kth Largest Element in an Array
  - quick select
  
* 234 Palindrome Linked List
  - count nodes. reverse half. check palindrome.
  - restore linked list
  
* 235 Lowest Common Ancestor of a Binary Search Tree
  - if both targets are smaller than root, traverse down to left subtree
  - if both targets are larger than root, traverse down to right subtree
  - else return root

* 236 Lowest Common Ancestor of a Binary Tree
  - if current node is p or q or null, return current node
  - traverse down both left and right, save return values
  - if either return values is null, return the other; otherwise return current node (both find, means current node is the LCA)

* 274, 275 H-Index I,II 
  - naive implementation: uses sort then binary search
  - better implementation: uses array of size N as a hashmap
  - for II (sorted), just use binary search

* 278 First Bad Version 
  - standard binary search
  - be careful for overflow. `mid=a+(b-a)/2` is better than `mid=(a+b)/2`
  
* 283 Move Zeroes
  - two pointers, one for iteration, the other points at the leftmost zero, ready for swapping
 
* 289 Game of Life
  - in-place: mark next state with numbers other than 0 or 1, then loop again to modify 
 
* 316 Remove Duplicate Letters 
  - two iterations, 1st one log occurance, 2nd one build result
  - maintain a stack in sync with StringBuilder
  - if prev letters have larger lex-order than current, and they have occurance in the rest of the string, keep popping them from the stack (as well as removing from StringBuilder)
  
* 322 Coin Change
  - dp array `dp[amount+1]` to save the minimum # of coins to use to reach a certain amount
  - try all avaialbe coins, save `dp[amount]` = `dp[amount-coin]+1` if reachable
  - be careful to check `amount>=coin` to prevent index out of bound
  
* 330 Patch Array
  - trick is math?







