/*

Given two arrays/Lists (choose whatever you want to) with sorted and non intersecting intervals. Merge them to get a new sorted non intersecting array/list. 

Eg: 
Given: 
Arr1 = [3-11, 17-25, 58-73]; 
Arr2 = [6-18, 40-47]; 

Wanted: 
Arr3 = [3-25, 40-47, 58-73];

*/

import java.util.*;

public class MergeIntervals {

	static class Interval {
		int lo;
		int hi;
		Interval (int l, int h) {
			lo = l;
			hi = h;
		}
	}
	
	public static void main (String[] args) {
		List<Interval> a = new ArrayList<Interval>();
		a.add(new Interval(3,11));
		a.add(new Interval(17,25));
		a.add(new Interval(58,73));
		List<Interval> b = new ArrayList<Interval>();
		b.add(new Interval(6,18));
		b.add(new Interval(40,47));
		print( merge(a,b) );
	}

	public static void print (List<Interval> list) {
		for (Interval l : list) {
			if (l != null)
				System.out.println(l.lo + "," + l.hi);
		}
	}

	public static List<Interval> merge(List<Interval> a, List<Interval> b) {
		List<Interval> result = new LinkedList<Interval>();
		int i = 0, j = 0;
		Interval current = null, prev = null;
		while (i < a.size() || j < b.size() ) {

			if (i < a.size() && j < b.size()) {
				if (a.get(i).lo < b.get(j).lo) {
					current = a.get(i);
					i++;
				}
				else {
					current = b.get(j);
					j++;
				}
			}
			else {
				if (i < a.size()) {
					current = a.get(i);
					i++;
				}
				else {
					current = b.get(j);
					j++;
				}
			}
			
			if (prev != null) {
				if (current.lo < prev.hi) {
					prev = new Interval(prev.lo, Math.max(current.hi, prev.hi));
				}
				else {
					result.add(prev);
					prev = current;
				}
			}
			else {
				prev = current;
			}
		}
		result.add(prev);
		return result;
	}
}