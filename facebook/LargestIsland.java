/*
	2-d array, only 0 or 1, find largest island of 1
	
	input:
		001000
		111011
		110001
		000110
		011110
		111000
	output:
		 9
*/

public class LargestIsland {

	public static void main (String[] args) {
		char[][] map = {
			{'0','0','1','0','0','0'},
			{'1','1','1','0','1','1'},
			{'1','1','0','0','0','1'},
			{'0','0','0','1','1','0'},
			{'0','1','1','1','1','0'},
			{'1','1','1','0','0','0'}
		};
		System.out.println( find(map) );
	}

	// my solution using depth-first-search
	public static int find (char[][] map) {
		int max = 0;
		for (int i = 0; i < map.length; i++) {
			for (int j = 0; j < map[0].length; j++) {
				if (map[i][j] == '1') {
					int area = dfs(map, i, j);
					if (max < area) max = area;
				}
			}
		}
		return max;
	}

	public static int dfs (char[][] map, int x, int y) {
		int area = 0;
		if (map[x][y] == '1') {
			map[x][y] = '2';
			area = 1;
			if (x > 0) {
				area += dfs(map, x-1, y);
			}
			if (x < map.length-1) {
				area += dfs(map, x+1, y);
			}
			if (y > 0) {
				area += dfs(map, x, y-1);
			}
			if (y < map[0].length-1) {
				area += dfs(map, x, y+1);
			}
		} 
		return area;
	}

}