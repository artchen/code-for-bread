/*
	Given a tree, find the smallest subtree that contains all of the tree's deepest nodes. Example:

				 a
			 / | \
			b  c  d
		 / \	  |
		e   f   g
	 /   / \
	h   i   j
	
	depth of tree: 4
	deepest nodes: [h,i,j]
	least common ancestor of [h,i,j]: b
	return: b

	solution: bfs() find all deepest nodes, then dfs() find lowest common ancester of the deepest nodes
*/
import java.util.*;
public class SmallestSubtreeDeepestNodes {

	static class TreeNode {
		List<TreeNode> children;
		int val;
		TreeNode (int val) {
			this.val = val;
			this.children = new LinkedList<TreeNode>();
		}
	}

	static class LeafNodeInfo {
		TreeNode node;
		int maxDepth;
		LeafNodeInfo (TreeNode node, int maxDepth) {
			this.node = node;
			this.maxDepth = maxDepth;
		}
	}

	private static int maxDepth;
	
	public static void main (String[] args) {
		TreeNode root = new TreeNode(1);

		TreeNode node2 = new TreeNode(2);
		TreeNode node3 = new TreeNode(3);
		TreeNode node4 = new TreeNode(4);

		TreeNode node5 = new TreeNode(5);
		TreeNode node6 = new TreeNode(6);
		TreeNode node7 = new TreeNode(7);

		TreeNode node8 = new TreeNode(8);
		TreeNode node9 = new TreeNode(9);
		TreeNode node10 = new TreeNode(10);

		root.children.add(node2);
		root.children.add(node3);
		root.children.add(node4);
		
		node2.children.add(node5);
		node2.children.add(node6);
		node4.children.add(node7);

		node5.children.add(node8);
		node6.children.add(node9);
		node6.children.add(node10);

		System.out.println( find(root).val );
	}

	public static TreeNode find(TreeNode root) {
		if (root == null || root.children.isEmpty() ) return root;
		return helper(root).node;
	}

	public static LeafNodeInfo helper(TreeNode root) {
		if (root.children.isEmpty() ) return new LeafNodeInfo(root, 1);

		int maxDepth = Integer.MIN_VALUE;
		int size = root.children.size();
		LeafNodeInfo rootInfo = new LeafNodeInfo(root, maxDepth);

		for (TreeNode child : root.children) {
			LeafNodeInfo childInfo = helper(child);
			if (childInfo.maxDepth > maxDepth) {
				maxDepth = childInfo.maxDepth;
				rootInfo.node = (maxDepth == 1) ? root : childInfo.node;
				rootInfo.maxDepth = childInfo.maxDepth + 1;
			}
			else if (childInfo.maxDepth == maxDepth) {
				rootInfo.node = root;
			}
		}
		return rootInfo;
	}

}