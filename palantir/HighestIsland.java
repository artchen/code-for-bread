import java.util.*;
public class HighestIsland {
	
	public static void main (String[] args) {
		int[][] grid = {
			{0,2,1,0,1},
			{2,2,0,1,1},
			{1,0,0,9,1},
			{0,1,1,0,0},
			{1,1,1,1,1}
		};
		System.out.println( highest(grid) );
	}

	public static int highest (int[][] grid) {
		return 0;
	}

	public static int highest2 (int[][] grid) {
		int max = 0, m = grid.length, n = grid[0].length;
		boolean[][] visited = new boolean[m][n];
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				visited[i][j] = false;
			}
		}
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				if (!visited[i][j] && grid[i][j] > 0) {
					int tmp = dfs(grid,i,j,visited);
					if (tmp > max) max = tmp;
				}
			}
		}
		return max;
	}

	public static int dfs (int[][] grid, int i, int j, boolean[][] visited) {
		if (i < 0 || j < 0 || i >= grid.length || j >= grid[0].length || visited[i][j] || grid[i][j] == 0) return 0;
		visited[i][j] = true;
		return grid[i][j] + dfs(grid,i-1,j,visited) + dfs(grid,i+1,j,visited) + dfs(grid,i,j-1,visited) + dfs(grid,i,j+1,visited);
	}

}