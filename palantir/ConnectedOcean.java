/*
	Given a 2-d integer matrix, 0 represents sea, 1 represents land, check whether all the seas are connected. Do it both recursively and iteratively.
*/

import java.util.*;
public class ConnectedOcean {

	static class Pair {
		int i;
		int j;
		Pair(int i, int j) {
			this.i = i;
			this.j = j;
		}
	}
	
	public static void main (String[] args) {
		int[][] map = {
			{1,0,0,0},
			{0,1,0,0},
			{0,0,1,1}
		};
		if (isConnected(map)) System.out.println("connected");
		else System.out.println("disconnected");
	}

	public static boolean isConnected (int[][] grid) {
		boolean found = false;
		Stack<Pair> stack = new Stack<Pair>();
		for (int i = 0; i < grid.length; i++) {
			for (int j = 0; j < grid[0].length; j++) {
				if (!found && grid[i][j] == 0) {
					stack.add(new Pair(i,j));
					found = true;
					while (stack.empty() == false) {
						Pair current = stack.pop();
						if (grid[current.i][current.j] == 0) {
							grid[current.i][current.j] = 2;
							if (current.i > 0) 
								stack.push(new Pair(current.i-1,current.j));
							if (current.i < grid.length-1) 
								stack.push(new Pair(current.i+1,current.j));
							if (current.j > 0) 
								stack.push(new Pair(current.i,current.j-1));
							if (current.j < grid[0].length-1) 
								stack.push(new Pair(current.i,current.j+1));
						}
					}
				}
				else if (found && grid[i][j] == 0) {
					return false;
				}
			}
		}
		for (int i = 0; i < grid.length; i++) {
			for (int j = 0; j < grid[0].length; j++) {
				if (grid[i][j] == 2) grid[i][j] = 1;
			}
		}
		return true;
	}

	// recursive dfs
	public static boolean isConnected2 (int[][] grid) {
		boolean found = false;
		for (int i = 0; i < grid.length; i++) {
			for (int j = 0; j < grid[0].length; j++) {
				if (!found && grid[i][j] == 0) {
					found = true;
					dfs(grid,i,j);
				}
				else if (found && grid[i][j] == 0) {
					return false;
				}
			}
		}
		for (int i = 0; i < grid.length; i++) {
			for (int j = 0; j < grid[0].length; j++) {
				if (grid[i][j] == 2) grid[i][j] = 1;
			}
		}
		return true;
	}

	public static void dfs(int[][] grid, int i, int j) {
		if (i < 0 || j < 0 || i >= grid.length || j >= grid[0].length) return;
		if (grid[i][j] == 0) {
			grid[i][j] = 2;
			dfs(grid,i+1,j);
			dfs(grid,i-1,j);
			dfs(grid,i,j-1);
			dfs(grid,i,j+1);
		}
	}

}