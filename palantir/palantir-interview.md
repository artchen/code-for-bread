# Palantir Interview Questions

## Connected ocean

```
Given a 2-d int matrix, 0 represents sea, 1 represents land, check whether all the seas are connected. Do it both recursively and iteratively.
```
Recursive dfs and iterative dfs with stack.

## Highest island

```
Given a 2-d integer matrix with non-negative numbers, 0 represents water, positive numbers represents islands, find the island with largest sum. Return the largest sum.
Follow up: what if some number is negative?
```

dfs + visited map.

Follow up: dp

## Leetcode: number of islands

Simple dfs.

## Leetcode: unique binary search trees I & II


## Binary tree in-order traversal 

Do this recursively, iteratively with stack, and iteratively in constant space.

Recursive: trivial

Iterative with stack: go all the way down to the left most leaf until current becomes null, push all nodes in the path to stack. Once current becomes null, pop 1 node from the stack and save it in traversal result, then set current to its right. Repeat this procedure until current == null and stack is empty.

Morris traversal: constanct space, only need a current and a prev pointer. The idea is to change the rightmost leaf of the left subtree to have a right pointer back to the root, so that we can backtrack. 

## getMinPowers(sum,power)



## Overlay

Compute overlay of 2 black-white colored grid. Grids are represented by tree of 4-children.


## Leetcode: LRU cache


## Quick select


## Leetcode: clone graph


## Leetcode: contains duplicate I

## Leetcode: contains duplicate II

## Leetcode: contains duplicate III



