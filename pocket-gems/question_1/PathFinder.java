import java.io.*;
import java.util.*;

public class PathFinder {
    public static void main(String[] args)
            throws FileNotFoundException, IOException {
        String filename = "input_1.txt";
        if (args.length > 0) {
        	filename = args[0];
        }
        
        List<String> answer = parseFile(filename);
        System.out.println(answer);
    }
    
    static List<String> parseFile(String filename)
    		throws FileNotFoundException, IOException {
    	/*
    	 *  Don't modify this function
    	 */
        BufferedReader input = new BufferedReader(new FileReader(filename));
        List<String> allLines = new ArrayList<String>();
        String line;
        while ((line = input.readLine()) != null) {
        	allLines.add(line);
        }
        input.close();

        return parseLines(allLines);    	
    }
    
    static List<String> parseLines(List<String> lines) {
    	/*
    	 * 
    	 *  Your code goes here
    	 *  
    	 */
        char start = '0', end = '0';
        HashMap<Character,List<Character>> map = new HashMap<Character,List<Character>>();
        for (int i = 0; i < lines.size(); i++) {
            if (i == 0) {
                String[] tokens = lines.get(i).split(" ");
                if (tokens.length < 2) {
                    System.out.println("Invalid input file format!");
                    return null;
                }
                start = tokens[0].charAt(0);
                end = tokens[1].charAt(0);
            }
            else {
                String[] tokens = lines.get(i).split("[: ]+");
                char first = tokens[0].charAt(0);
                List<Character> toks = strArr2CharList(tokens);
                map.put(first, toks);
            }
        }
    	return findPaths(map,start,end);
    }

    /*
        Helper method. Convert a string array to char list
    */
    static List<Character> strArr2CharList (String[] strs) {
        List<Character> charArray = new LinkedList<Character>();
        for (int i = 1; i < strs.length; i++) {
            charArray.add(strs[i].charAt(0));
        }
        return charArray;
    }


    /*
        Paths finding algorithm
    */
    static List<String> findPaths (HashMap<Character,List<Character>> map, char start, char end) {
        List<String> result = new LinkedList<String>();
        find(map,start,end,result,new StringBuilder(), new HashSet<Character>());
        return result;
    }

    /*
        Recursively find paths
    */
    static void find (HashMap<Character,List<Character>> map, char current, char end, List<String> result, StringBuilder sb, HashSet<Character> visited) {
        
        sb.append(current);
        visited.add(current);
        // if meet destination
        if (current == end) {
            result.add(sb.toString());
        }
        // if not, iterate through adjacency list
        else {
            if (map.containsKey(current)) {
                List<Character> adjList = map.get(current);
                for (char curr : adjList) {
                    if (!visited.contains(curr))
                        find(map,curr,end,result,sb,visited);
                }
            }
        }
        visited.remove(current);
        sb.deleteCharAt(sb.length()-1);
    }
}