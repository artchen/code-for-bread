import java.io.*;
import java.util.*;
import java.text.*;

public class LogParser {
    public static void main(String[] args)
            throws FileNotFoundException, IOException, ParseException {
        String filename = "input_1.txt";
        if (args.length > 0) {
        	filename = args[0];
        }

        String answer = parseFile(filename);
        System.out.println(answer);
    }

    static String parseFile(String filename)
            throws FileNotFoundException, IOException, ParseException {
        /*
    	 *  Don't modify this function
    	 */
        BufferedReader input = new BufferedReader(new FileReader(filename));
        List<String> allLines = new ArrayList<String>();
        String line;
        while ((line = input.readLine()) != null) {
            allLines.add(line);
        }
        input.close();

        return parseLines(allLines.toArray(new String[allLines.size()]));
    }

    static String parseLines(String[] lines) throws ParseException {
        /*
         *
         * Your code goes here
         *
         */
        
        long duration = 0, total = 0;
        String prevStart = "", begin = "";
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy-HH:mm:ss");
        for (String line : lines) {

            String time = line.substring(1,20);
            String status = line.substring(25);

            if (status.equals("START") ) {
                begin = time;
            }
            else if (status.equals("CONNECTED") ) {
                prevStart = time;
            }
            else if (status.equals("DISCONNECTED")) {
                Date start = df.parse(prevStart);
                Date end = df.parse(time);
                long start_milisec = start.getTime();
                long end_milisec = end.getTime();
                duration += end_milisec-start_milisec;
                prevStart = null;
            }
            else if (status.equals("SHUTDOWN")) {
                Date end = df.parse(time);
                long end_milisec = end.getTime();
                if (prevStart != null) {
                    Date start = df.parse(prevStart);
                    long start_milisec = start.getTime();
                    duration += end_milisec-start_milisec;
                }
                Date initial = df.parse(begin);
                long initial_milisec = initial.getTime();
                total = end_milisec-initial_milisec;
            }
        }
        double ratio = (duration*1.0 / total)*100;
        int ratio_int = (int)ratio;
        return ratio_int + "%";
    }

}
