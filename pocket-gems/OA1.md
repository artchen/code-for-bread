# Path Finder (for Java)

## Objective

Write a program that will read an input file and find all the paths from one node to another.

## Details

### Input

Files will have the following format:

* First line: `[Name of start node] [Name of end node]`
* Following lines: `[Name of node] : [List of connected nodes, separated by spaces]`

The file describes a directed graph that may contain cycles. (If `A` connects directly to `B`, that does not necessarily mean that `B` connects directly to `A`)

Each node has a list of neighbors that are reachable from that node. A node may have no neighbors, which is represented by it not having a line in the input file.

### Output

The program should output all the possible paths from the start node to the end node. The paths should not contain cycles. The paths can be output in any order. If there are no possible paths, return an empty list.

E.g. the path "start from node A, travel to node C, travel to node D, end at node F" would be represented by the string `ACDF`.

Modify the parseLines function to return a list of paths, represented by strings.

## Example

Input file:

```
A C
A : B D
B : C D
D : C
```

Reading this file line by line:

* Our paths should be from `A` to `C`
* The node `A` connects to nodes `B` and `C`
* The node `B` connects to node `C`
* The node `D` connects to node `C`
* There is no line for `C`, so it connects to no node

Expected output: a list containing the stirngs `"ABC", "ABDC", "ADC"`, in no particular order.

More sample input can be found in the text files `input_1.txt`, `input_2.txt` and `input_3.txt`
